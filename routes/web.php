<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['installed']], function () {

    Route::group(['middleware' => ['lang']], function () {

        Route::get('/', function () {
            return view('welcome');
        });

        Route::get('getLangs', 'FrontendController@getLangs');
        Route::post('setLang', 'FrontendController@setLang');

        Route::get('/tomos', 'FrontendController@getTomos');
        Route::get('/tomos/{id}', 'FrontendController@getTomo');
        Route::get('/tomos/{id}/capitulos', 'FrontendController@getCapitulos');

        Route::get('/capitulos/{id}/paginas', 'FrontendController@getPaginas');

        Route::get('/capitulos/{id}', 'FrontendController@getCapitulo');

        Route::post('set_favorito', 'FrontendController@setFavorito');
        Route::get('get_favoritos', 'FrontendController@getFavoritos');

        Route::get('getPagina/{paginaId}', 'FrontendController@getPagina');

        Route::get('imprimirPaginas/{id1}/{id2}', 'FrontendController@imprimirCapitulos');

        Route::post('sendEmail', 'FrontendController@sendEmail');

    });

    Auth::routes(['register' => false]);

    Route::group(['middleware' => ['auth']], function () {
        Route::get('admin', 'AdminController@index');
        Route::get('admin/capitulo/{id}/paginas/{langId}', 'AdminController@paginas');
        Route::post('admin/guardar_textos', 'AdminController@guardarTextos');
        Route::get('admin/getText/{id}/{lang}', 'AdminController@getText');

        Route::get('admin/fixNumberErrors/{tomoId}', 'AdminController@fixNumberErrors');
    });

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/export/{id}', 'ExportArrayController@index')->name('export');

});

Route::group(['middleware' => ['not_installed']], function () {
    Route::get('install', 'InstallController@index');
    Route::post('install', 'InstallController@install')->name('install');
});
