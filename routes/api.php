<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['lang']], function () {

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    Route::get('getLangs', 'FrontendController@getLangs');
    Route::post('setLang', 'FrontendController@setLang');

    Route::get('/tomos', 'FrontendController@getTomos');
    Route::get('/tomos/{id}/capitulos', 'FrontendController@getCapitulos');

    Route::get('/capitulos/{id}/paginas', 'FrontendController@getPaginas');

    Route::post('set_favorito', 'FrontendController@setFavorito');
    Route::get('get_favoritos', 'FrontendController@getFavoritos');

    Route::get('getPagina/{paginaId}', 'FrontendController@getPagina');

    Route::get('imprimirPaginas/{id1}/{id2}', 'FrontendController@imprimirPaginas');

});

