
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';

window.Vue.use(VueMaterial);

import VueAwesomeSwiper from 'vue-awesome-swiper';

// require styles
import 'swiper/dist/css/swiper.css';

//Zoom images
import zoom from 'vue-image-zoom';
import 'vue-image-zoom/dist/vue-image-zoom.css';

Vue.use(zoom);

window.Vue.use(VueAwesomeSwiper, /* { default global options } */);

var baseUrl = document.getElementsByName("base_url");
var mainUrl = document.getElementsByName("main_url");

window.myMixin = {
    data: function() {
        return {
            baseUrl : baseUrl[0].attributes.content.nodeValue,
            mainUrl : mainUrl[0].attributes.content.nodeValue,
        }
    },
}

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('frontend-component', require('./components/Frontend/FrontendComponent.vue'));

// const files = require.context('./', true, /\.vue$/i)

// files.keys().map(key => {
//     return Vue.component(_.last(key.split('/')).split('.')[0], files(key))
// })

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});
