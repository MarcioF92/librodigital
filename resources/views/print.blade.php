<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <meta name="base_url" content="{{ url('/') }}">

        <style>
            @page { margin: 1px; }
            body { margin: 1px; }
        </style>

    </head>
    <body>
        <div id="app">
            @foreach($paginasParaImprimir as $pagina)
                <img style="width:99%;height:80%" src="data:image/jpg;base64, {{ $pagina }}" />
            @endforeach
        </div>
    </body>
</html>
