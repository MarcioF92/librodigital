@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Tomos</div>

                    <div class="card-body">
                        @foreach($tomos as $tomo)
                            <h2>{{ $tomo->nombre_espanol }}</h2>
                            @foreach($tomo->capitulos as $capitulo)
                                <li><a href="{{ url('admin/capitulo/'.$capitulo->id.'/paginas/es') }}">{{ $capitulo->titulo_espanol }}</a></li>
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
