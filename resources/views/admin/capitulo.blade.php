@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <p><a href="{{ url('/admin/') }}">Volver</a></p>
                <h1>Capítulo:
                    @if($lang->abreviacion == 'ES')
                        {{$capitulo->titulo_espanol}}
                    @else
                        {{$capitulo->titulo_ingles}}
                    @endif
                </h1>

                @if($lang->abreviacion == 'ES')
                    <a href="{{ url('/admin/capitulo/'.$capitulo->id.'/paginas/en') }}">Inglés</a>
                @else
                    <a href="{{ url('/admin/capitulo/'.$capitulo->id.'/paginas/es') }}">Español</a>
                @endif

                <p><a href="#" class="getAllTexts">Todos los textos</a> </p>

                <form action="{{ url('admin/guardar_textos') }}" method="post">
                    @csrf
                    <input type="hidden" name="capitulo_id" value="{{$capitulo->id}}" />
                    @foreach($capitulo->paginas as $pagina)
                        <div class="row">
                            <div class="col-md-12">
                                <h3>{{ $pagina->titulo }}</h3>
                            </div>
                            <div class="col-md-6">
                                <img src="{{ $pagina->imagen }}" />
                            </div>
                            <div class="col-md-6 form-group">
                                <a href="#" class="getTextButton" data-id="{{$pagina->id}}">Leer imagen</a>
                                <textarea id="text-{{$pagina->id}}" class="form-control" rows="30" name="pagina[{{$pagina->id}}]">
                                    {{$pagina->texto}}
                                </textarea>
                            </div>
                        </div>
                    @endforeach
                    <div class="row">
                        <button type="submit" class="btn btn-dark">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('customJs')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
    <script>
        $(document).ready(function(){

            var allIds = "{!! json_encode($capitulo->paginas()->where('idioma_id', $lang->id)->pluck('id')->all()) !!}";

            function getText(id) {
                console.log(id);
                $.ajax({
                    async: false,
                    method: "GET",
                    url: "{{ url('admin/getText/') }}/" + id + "/{{$lang->abreviacion}}",
                }).done(function( res ) {

                    $("#text-"+id).val(res);

                }).fail(function( res ) {
                    //alert(error_msg);
                });
                return true;
            }
            $(".getTextButton").on('click', function(e){
                e.preventDefault();
                var id = $(this).data('id');
                getText(id);
            });

            $(".getAllTexts").on('click', function(e){
                e.preventDefault();
                var ids = JSON.parse(allIds);
                for(var x = 0; x < ids.length; x++) {
                    getText(ids[x]);
                }
            });
        });
    </script>
@endsection
