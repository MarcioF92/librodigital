<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use thiagoalessio\TesseractOCR\TesseractOCR;


class Pagina extends Model
{
    protected $table = 'paginas';

    protected $guarded = ['id'];

    public function capitulo(){
        return $this->belongsTo('App\Capitulo');
    }

    public function getOCRText($lang)
    {
        if(!empty($this->texto))
            return $this->texto;

        $ln = $lang == 'ES' ? 'spa' : 'eng';

        $originalPath = explode('/', Storage::disk('local')->path(str_replace('/storage/app', '', $this->imagen)));

        $fileName = end($originalPath);
        unset($originalPath[count($originalPath) - 1]);

        $path = '';

        foreach ($originalPath as $op) {
            if(!empty($op))
                $path .= '/'.$op;
        }

        $path .= '/originales/'.$fileName;

        $ocr = new TesseractOCR();
        $ocr->image($path);
        $ocr->lang($ln);
        return $ocr->run();
    }
}
