<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tomo extends Model
{
    protected $table = 'tomos';

    protected $guarded = ['id'];

    public function capitulos(){
        return $this->hasMany('App\Capitulo');
    }
}
