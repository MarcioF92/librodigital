<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Capitulo extends Model
{
    protected $table = 'capitulos';

    protected $guarded = ['id'];

    public function tomo(){
        return $this->belongsTo('App\Tomo');
    }

    public function paginas(){
        return $this->hasMany('App\Pagina');
    }
}