<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'idiomas';

    public $timestamps = false;

    protected $guarded = ['id'];

}