<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPage extends Mailable
{
    use Queueable, SerializesModels;

    protected $fromEmail;
    protected $fromName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fromName, $msg)
    {
        $this->fromName = $fromName;
        $this->msg = $msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('marcio@adue.digital')
            ->view('email', [ 'msg' => $this->msg]);
    }
}
