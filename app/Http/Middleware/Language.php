<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::get('current_lang', false)){
            $lang = \App\Language::where('defecto', 1)->first();
            Session::put('current_lang', $lang);
        }
        return $next($request);
    }
}
