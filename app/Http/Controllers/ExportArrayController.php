<?php

namespace App\Http\Controllers;

use App\Language;
use App\Tomo;
use Illuminate\Http\Request;

class ExportArrayController extends Controller
{
    public function index($tomoId)
    {
        $tomo = Tomo::find($tomoId);
        $array = [
            'nombre_espanol' => $tomo['nombre_espanol'],
            'nombre_ingles' => $tomo['nombre_ingles'],
            'portada' => $tomo['portada'],
            'portada_ingles' => $tomo['portada_ingles'],
            'created_at' => (string) $tomo['created_at'],
            'updated_at' => (string) $tomo['updated_at'],
            'capitulos' => []
        ];

        foreach($tomo->capitulos as $capitulo) {
            $array['capitulos'][] = [
                'imagen' => $capitulo->imagen,
                'imagen_ingles' => $capitulo->imagen_ingles,
                'imagen_titulo' => $capitulo->imagen_titulo,
                'imagen_titulo_ingles' => $capitulo->imagen_titulo_ingles,
                'titulo_espanol' => $capitulo->titulo_espanol,
                'titulo_ingles' => $capitulo->titulo_ingles,
                'texto_espanol' => $capitulo->texto_espanol,
                'texto_ingles' => $capitulo->texto_ingles,
                'orden' => $capitulo->orden,
                'created_at' => $capitulo->created_at,
                'updated_at' => $capitulo->updated_at,
                'paginas' => [
                    'es' => $capitulo->paginas()->where('idioma_id', Language::where('abreviacion', 'ES')->first()->id)->get()->toArray(),
                    'en' => $capitulo->paginas()->where('idioma_id', Language::where('abreviacion', 'EN')->first()->id)->get()->toArray(),
                ]
            ];

        }

        $content = "<?php\n\n";

        $content .= '$tomos = ';

        $content .= var_export($array, true);

        $content .= ';';

        return response($content)
            ->withHeaders([
                'Content-Disposition' => 'attachment; filename=tomos.php',
                'Cache-Control' => 'no-cache',
            ]);

    }
}
