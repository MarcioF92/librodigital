<?php

namespace App\Http\Controllers;

use App\Capitulo;
use App\Language;
use App\Pagina;
use App\Tomo;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Mail\SendPage;
use Illuminate\Support\Facades\Mail;

class FrontendController extends Controller
{

    public function getLangs(){
        return [
            'idiomas' => Language::all(),
            'current' => Session::get('current_lang')
        ];
    }

    public function setLang(Request $request){
        $lang = Language::find($request->input('id'));
        if($lang != null) {
            Session::put('current_lang', $lang);
            return ['current' => Session::get('current_lang', false)];
        }
        return ['success' => false];
    }

    public function getTomos(){
        return Tomo::all();
    }

    public function getTomo($tomoId){
        return Tomo::with('capitulos')
                    ->with(['capitulos.paginas' => function($query) {
                        $query->where('idioma_id', Session::get('current_lang', false)->id);
                    }])
                    ->find($tomoId);
    }

    public function getCapitulos($tomoId){
        return Tomo::find($tomoId)
                    ->capitulos()
                    ->withCount('paginas')
                    ->orderBy('orden')->get();
    }

    public function getCapitulo($capituloId){
        return Capitulo::with('tomo')->find($capituloId);
    }

    public function getPaginas($capituloId){

        $capitulo = Capitulo::with(['paginas' => function($q) {
                // Query the name field in status table
                $q->where('idioma_id', Session::get('current_lang')->id);
            }])
            ->find($capituloId);

        $anteriores = Capitulo::with(['paginas' => function($q) {
                // Query the name field in status table
                $q->where('idioma_id', Session::get('current_lang')->id);
            }])
            ->where('orden', '<', $capitulo->orden)
            ->where('tomo_id', '=', $capitulo->tomo_id)
            ->orderBy('orden')
            ->get();

        $siguientes = Capitulo::with(['paginas' => function($q) {
                // Query the name field in status table
                $q->where('idioma_id', Session::get('current_lang')->id);
            }])
            ->where('orden', '>', $capitulo->orden)
            ->where('tomo_id', '=', $capitulo->tomo_id)
            ->orderBy('orden')
            ->get();

        return [
            'anteriores' => $anteriores,
            'capitulo' => $capitulo,
            'siguientes' => $siguientes,
        ];

    }

    public function getPagina($paginaId){
        return Pagina::find($paginaId);
    }

    public function setFavorito(Request $request){
        $favoritos = Session::get('capitulos_favoritos', []);
        array_push($favoritos, $request->input('pagina_id'));
        Session::forget('capitulos_favoritos');
        Session::put('capitulos_favoritos', $favoritos);
        $favoritosNuevo = Session::get('capitulos_favoritos', []);
        return [
            'success' => true,
        ];
    }

    public function getFavoritos(){
        $favoritos = Session::get('capitulos_favoritos', []);
        return Pagina::whereIn('id', Session::get('capitulos_favoritos', []))->get();
    }

    public function imprimirCapitulos($id1, $id2){
        $paginasParaImprimir = [];
        $paginas = [$id1, $id2];
        foreach ($paginas as $paginaId){

            if(strpos($paginaId, 'portada') !== false){
                $capituloId = explode('-', $paginaId)[1];
                $capitulo = Capitulo::find($capituloId);
                if($capitulo != null){
                    if(Session::get('current_lang')->abreviacion == 'ES'){
                        $paginasParaImprimir[] = base64_encode(file_get_contents(env('MAIN_URL').$capitulo->imagen_titulo));
                    } else {
                        $paginasParaImprimir[] = base64_encode(file_get_contents(env('MAIN_URL').$capitulo->imagen_titulo_ingles));
                    }

                }

            }
            if($paginaId == 'empty'){
                $paginasParaImprimir[] = base64_encode(file_get_contents(env('MAIN_URL') . '/storage/app/public/tomos/capitulo_title_background.jpg'));
            }

            $paginaObj = Pagina::find($paginaId);
            if($paginaObj != null)
                $paginasParaImprimir[] = base64_encode(file_get_contents(env('MAIN_URL').$paginaObj->imagen));
        }

        $pdfObj = App::make('dompdf.wrapper');
        $pdf = $pdfObj->loadView('print', [ 'paginasParaImprimir' => $paginasParaImprimir ])
            ->setPaper('legal');

        return $pdf->download('descarga-'.date('d-m-Y').'.pdf');
    }

    public function sendEmail(Request $request){
        $email = $request->input('email');
        Mail::to($email['email_amigo'], $email['nombre_amigo'])
            ->send(new SendPage($email['nombre'], $email['mensaje']));
        return [
            'success' => true
        ];
    }
}
