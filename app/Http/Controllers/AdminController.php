<?php

namespace App\Http\Controllers;

use App\Capitulo;
use App\Language;
use App\Pagina;
use App\Tomo;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    private const SUMMA_ANDINA_ID = 2;
    private const SUMMA_PATAGONICA_ID = 3;
    private const SUMMA_PAMPEANA_ID = 4;
    private const SUMMA_CHAQUENA_ID = 5;
    private const SUMMA_METROPOLITANA_ID = 6;

    public function index()
    {
        $tomos = Tomo::with('capitulos')
                    ->get();
        return view('admin.index', ['tomos' => $tomos]);
    }
    public function paginas($capituloId, $langId)
    {
        $lang = Language::where('abreviacion', strtoupper($langId))->first();

        $capitulo = Capitulo::with(['paginas' => function($q) use ($lang) {
                return $q->where('idioma_id', $lang->id);
            }])
                ->find($capituloId);

        return view('admin.capitulo', compact('capitulo', 'lang'));
    }

    public function getText($id, $lang)
    {
        $pagina = Pagina::find($id);
        return $pagina->getOCRText($lang);
    }

    public function guardarTextos(Request $request)
    {
        foreach($request->input('pagina') as $id => $texto) {
            $pagina = Pagina::find($id);
            $pagina->texto = !is_null($texto) ? $texto : '';
            $pagina->save();
        }
        return redirect('admin');
    }

    /*public function fixNumberErrors($tomoId)
    {
        switch ($tomoId) {
            case self::SUMMA_ANDINA_ID:
                $this->fixSummaAndina();
                break;

            case self::SUMMA_PATAGONICA_ID:
                $this->fixSummaPatagonica();
                break;

            case self::SUMMA_PAMPEANA_ID:
                $this->fixSummaPampeana();
                break;

            case self::SUMMA_CHAQUENA_ID:
                $this->fixSummaChaquena();
                break;

            case self::SUMMA_METROPOLITANA_ID:
                $this->fixSummaMetropolitana();
                break;
        }
    }*/

    private function fixSummaAndina()
    {
        echo "Arreglando Summa Andina<br>";
        $tomo = Tomo::find(self::SUMMA_ANDINA_ID);

        foreach ($tomo->capitulos as $capitulo) {
            /*
             echo "Arreglando español<br>";
            $paginasEs = $capitulo->paginas()->where('idioma_id', 1)->get();
            foreach ($paginasEs as $pagina) {
                $nuevoOrden = $pagina->orden - 2;
                $pagina->orden = $nuevoOrden;
                $pagina->titulo = 'Página '.$nuevoOrden;
                $pagina->save();
            }*/

            echo "Arreglando inglés<br>";
            $paginasEn = $capitulo->paginas()->where('idioma_id', 2)->get();
            foreach ($paginasEn as $pagina) {
                $nuevoOrden = $pagina->orden + 2;
                $pagina->orden = $nuevoOrden;
                $pagina->titulo = 'Page '.$nuevoOrden;
                $pagina->save();
            }
        }

        echo "Arreglada Summa Andina<br>";
    }

    private function fixSummaPatagonica()
    {
        echo "Arreglando Summa Patagónica<br>";
        $tomo = Tomo::find(self::SUMMA_PATAGONICA_ID);

        foreach ($tomo->capitulos as $capitulo) {
            $paginasEs = $capitulo->paginas()->where('idioma_id', 1)->get();
            foreach ($paginasEs as $pagina) {
                $nuevoOrden = $pagina->orden - 2;
                $pagina->orden = $nuevoOrden;
                $pagina->titulo = 'Página '.$nuevoOrden;
                $pagina->save();
            }

            $paginasEn = $capitulo->paginas()->where('idioma_id', 2)->get();
            foreach ($paginasEn as $pagina) {
                $nuevoOrden = $pagina->orden - 2;
                $pagina->orden = $nuevoOrden;
                $pagina->titulo = 'Page '.$nuevoOrden;
                $pagina->save();
            }
        }

        echo "Arreglada Summa Patagónica<br>";
    }

    private function fixSummaPampeana()
    {
        echo "Arreglando Summa Pampeana<br>";
        $tomo = Tomo::find(self::SUMMA_PAMPEANA_ID);

        foreach ($tomo->capitulos as $capitulo) {
            $paginasEs = $capitulo->paginas()->where('idioma_id', 1)->get();
            foreach ($paginasEs as $pagina) {
                $nuevoOrden = $pagina->orden - 2;
                $pagina->orden = $nuevoOrden;
                $pagina->titulo = 'Página '.$nuevoOrden;
                $pagina->save();
            }

            $paginasEn = $capitulo->paginas()->where('idioma_id', 2)->get();
            foreach ($paginasEn as $pagina) {
                $nuevoOrden = $pagina->orden - 2;
                $pagina->orden = $nuevoOrden;
                $pagina->titulo = 'Page '.$nuevoOrden;
                $pagina->save();
            }
        }

        echo "Arreglada Summa Pampeana<br>";
    }

    private function fixSummaMetropolitana()
    {
        echo "Arreglando Summa Metropolitana<br>";
        $tomo = Tomo::find(self::SUMMA_METROPOLITANA_ID);

        foreach ($tomo->capitulos as $capitulo) {
            $paginasEs = $capitulo->paginas()->where('idioma_id', 1)->get();
            foreach ($paginasEs as $pagina) {
                $nuevoOrden = $pagina->orden - 305;
                $pagina->orden = $nuevoOrden;
                $pagina->titulo = 'Página '.$nuevoOrden;
                $pagina->save();
            }

            $paginasEn = $capitulo->paginas()->where('idioma_id', 2)->get();
            foreach ($paginasEn as $pagina) {
                $nuevoOrden = $pagina->orden - 2;
                $pagina->orden = $nuevoOrden;
                $pagina->titulo = 'Page '.$nuevoOrden;
                $pagina->save();
            }
        }

        echo "Arreglada Summa Metropolitana<br>";
    }

    private function fixSummaChaquena()
    {
        echo "Arreglando Summa Chaqueña<br>";
        $tomo = Tomo::find(self::SUMMA_CHAQUENA_ID);

        foreach ($tomo->capitulos as $capitulo) {
            $paginasEs = $capitulo->paginas()->where('idioma_id', 1)->get();
            foreach ($paginasEs as $pagina) {
                $nuevoOrden = $pagina->orden - 241;
                $pagina->orden = $nuevoOrden;
                $pagina->titulo = 'Página '.$nuevoOrden;
                $pagina->save();
            }

            $paginasEn = $capitulo->paginas()->where('idioma_id', 2)->get();
            foreach ($paginasEn as $pagina) {
                $nuevoOrden = $pagina->orden - 2;
                $pagina->orden = $nuevoOrden;
                $pagina->titulo = 'Page '.$nuevoOrden;
                $pagina->save();
            }
        }

        echo "Arreglada Summa Chaqueña<br>";
    }
}
