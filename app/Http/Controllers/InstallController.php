<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class InstallController extends Controller
{
    public function index()
    {
        return view('install');
    }
    public function install(Request $request)
    {
        try {
            Schema::disableForeignKeyConstraints();

            Artisan::call('migrate:fresh');
            Artisan::call('db:seed');

            if (Storage::disk('local')->has('install'))
                Storage::delete('install');
            Schema::enableForeignKeyConstraints();

            return redirect('/');
        } catch (\Exception $e) {
            $mensaje = $e->getMessage()."\n";
            $mensaje .= $e->getFile()."\n";
            $mensaje .= $e->getLine()."\n";
            $mensaje .= $e->getTraceAsString()."\n";

            echo "<pre>";
            echo $mensaje;
            echo "</pre>";
        }

    }
}
