<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaginasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paginas', function (Blueprint $table) {
            $table->increments('id');
            $table->text('imagen');
            $table->string('titulo', 100);
            $table->text('texto');
            $table->integer('orden');
            $table->unsignedInteger('idioma_id');
            $table->unsignedInteger('capitulo_id');
            $table->timestamps();
        });

        Schema::table('paginas', function (Blueprint $table) {
            $table->foreign('idioma_id')->references('id')->on('idiomas');
            $table->foreign('capitulo_id')->references('id')->on('capitulos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paginas');
    }
}
