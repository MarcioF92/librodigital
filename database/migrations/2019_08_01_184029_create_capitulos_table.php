<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCapitulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capitulos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('imagen');
            $table->text('imagen_ingles');
            $table->text('imagen_titulo');
            $table->text('imagen_titulo_ingles');
            $table->string('titulo_espanol');
            $table->string('titulo_ingles');
            $table->text('texto_espanol');
            $table->text('texto_ingles');
            $table->integer('orden');
            $table->unsignedInteger('tomo_id');
            $table->timestamps();
        });

        Schema::table('capitulos', function (Blueprint $table) {
            $table->foreign('tomo_id')->references('id')->on('tomos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capitulos');
    }
}
