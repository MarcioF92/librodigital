<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTomosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tomos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_espanol', 75);
            $table->string('nombre_ingles', 75);
            $table->string('portada');
            $table->string('portada_ingles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tomos');
    }
}
