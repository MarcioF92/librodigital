<?php

use App\Pagina;
use App\Tomo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class TomoDosSeed extends CustomSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = array('id' => '2','nombre_espanol' => 'SUMMA ANDINA','nombre_ingles' => 'SUMMA ANDINA','portada' => '/storage/app/public/tomos/02_summa_andina/thumbnail.jpg','portada_ingles' => '/storage/app/public/tomos/02_summa_andina/thumbnail_ingles.jpg','created_at' => '2019-08-13 01:06:20','updated_at' => '2019-08-13 01:06:20');

        $tomo2 = Tomo::create($data);

        /* Capítulos Tomo 2 */
        include 'tomo2/capitulos.php';

        if (isset($capitulos)) {
            foreach ($capitulos as &$capitulo) {
                unset($capitulo['color']);
                unset($capitulo['size']);
                unset($capitulo['position_x']);
                unset($capitulo['position_y']);
                unset($capitulo['tomo_id']);
                $tomo2->capitulos()->create($capitulo);
            }
        }

        include 'tomo2/paginas.php';

        if (isset($paginas)) {
            foreach ($paginas as $pagina) {
                Pagina::create($pagina);
            }
        }


        $carpetaTomo = '02_summa_andina';

        $capitulos = [
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/08_apendices/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/08_apendices/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/08_apendices/es/00000000.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/08_apendices/en/00000295.jpg",
                    'titulo_espanol' => "Apéndices",
                    'titulo_ingles' => "Appendices",
                    'texto_espanol' => "Apéndices",
                    'texto_ingles' => "Appendices",
                    'orden' => "9",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "08_apendices",
                'paginas' => [
                    'es' => [
                        'from' => 0,
                        'to' => 97
                    ],
                    'en' => [
                        'from' => 295,
                        'to' => 435
                    ],
                ]
            ],
        ];

        foreach($capitulos as $capitulo) {
            $cap = $tomo2->capitulos()->create($capitulo['capitulo']);
            for ($x = $capitulo['paginas']['es']['from']; $x <= $capitulo['paginas']['es']['to']; $x++) {
                $filename = $this->getFilename($x);
                $cap->paginas()->create([
                    'imagen' => '/storage/app/public/tomos/02_summa_andina/'.$capitulo['carpeta'].'/es/'.$filename.'.jpg',
                    'titulo' => 'Página '.($x+247),
                    'texto' => '',
                    'orden' => ($x+247),
                    'idioma_id' => '1',
                    'created_at' => '2019-01-12 19:01:15',
                    'updated_at' => '2019-01-12 19:01:15',
                ]);
            }
            for ($x = $capitulo['paginas']['en']['from']; $x <= $capitulo['paginas']['en']['to']; $x++) {
                $filename = $this->getFilename($x);
                $cap->paginas()->create([
                    'imagen' => '/storage/app/public/tomos/02_summa_andina/'.$capitulo['carpeta'].'/en/'.$filename.'.jpg',
                    'titulo' => 'Página '.$x,
                    'texto' => '',
                    'orden' => $x,
                    'idioma_id' => '2',
                    'created_at' => '2019-01-12 19:01:15',
                    'updated_at' => '2019-01-12 19:01:15',
                ]);
            }
        }

        /*
        $tomos[] = Tomo::create([
            'nombre_espanol' => 'SUMMA PATAGÓNICA',
            'nombre_ingles' => 'SUMMA PATAGÓNICA',
            'portada' => '/storage/app/public/tomos/03_summa_patagonica/thumbnail.jpg',
            'portada_ingles' => '/storage/app/public/tomos/03_summa_patagonica/thumbnail_ingles.jpg',
        ]);

        $tomos[] = Tomo::create([
            'nombre_espanol' => 'SUMMA PAMPEANA',
            'nombre_ingles' => 'SUMMA PAMPEANA',
            'portada' => '/storage/app/public/tomos/04_summa_pampeana/thumbnail.jpg',
            'portada_ingles' => '/storage/app/public/tomos/04_summa_pampeana/thumbnail_ingles.jpg',
        ]);

        $tomos[] = Tomo::create([
            'nombre_espanol' => 'SUMMA CHAQUEÑA',
            'nombre_ingles' => 'SUMMA CHAQUEÑA',
            'portada' => '/storage/app/public/tomos/05_summa_chaquena/thumbnail.png',
            'portada_ingles' => '/storage/app/public/tomos/05_summa_chaquena/thumbnail_ingles.png',
        ]);
        */
    }
}
