<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.7.9
 */

/**
 * Database `librodigital`
 */

/* `librodigital`.`capitulos` */
$capitulos = array(
  array('id' => '14','imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/00_comienzo/thumbnail.jpg','imagen_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/00_comienzo/thumbnail.jpg','imagen_titulo' => '/storage/app/public/tomos/01_imagen_de_un_pais/00_comienzo/es/00000004.jpg','imagen_titulo_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/00_comienzo/en/00000004.jpg','titulo_espanol' => 'Créditos','titulo_ingles' => 'Credits','texto_espanol' => 'Créditos','texto_ingles' => 'Credits','orden' => '1','tomo_id' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '16','imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/thumbnail.jpg','imagen_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/thumbnail.jpg','imagen_titulo' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000005.jpg','imagen_titulo_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000005.jpg','titulo_espanol' => 'Introducción','titulo_ingles' => 'Introduction','texto_espanol' => 'Introducción','texto_ingles' => 'Introduction','orden' => '2','tomo_id' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '17','imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/thumbnail.jpg','imagen_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/thumbnail.jpg','imagen_titulo' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000032.jpg','imagen_titulo_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000032.jpg','titulo_espanol' => 'Las Regiones','titulo_ingles' => 'The Regions','texto_espanol' => 'Las Regiones','texto_ingles' => 'The Regions','orden' => '3','tomo_id' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '18','imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/thumbnail.jpg','imagen_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/thumbnail.jpg','imagen_titulo' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/es/00000080.jpg','imagen_titulo_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000082.jpg','titulo_espanol' => 'Índice general de la obra','titulo_ingles' => 'Complete table of content','texto_espanol' => 'Índice general de la obra','texto_ingles' => 'Complete table of content','orden' => '4','tomo_id' => '1','created_at' => NULL,'updated_at' => NULL)
);
