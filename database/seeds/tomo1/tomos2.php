<?php

array (
  'nombre_espanol' => 'ARGENTINA IMAGEN DE UN PAIS',
  'nombre_ingles' => 'ARGENTINA IMAGE OF A COUNTRY',
  'portada' => '/storage/app/public/tomos/01_imagen_de_un_pais/thumbnail.png',
  'portada_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/thumbnail_ingles.jpg',
  'created_at' => '2019-08-13 01:06:05',
  'updated_at' => '2019-08-13 01:06:05',
  'capitulos' => 
  array (
    0 => 
    array (
      'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/00_comienzo/thumbnail.jpg',
      'imagen_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/00_comienzo/thumbnail.jpg',
      'imagen_titulo' => '/storage/app/public/tomos/01_imagen_de_un_pais/00_comienzo/es/00000004.jpg',
      'imagen_titulo_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/00_comienzo/en/00000004.jpg',
      'titulo_espanol' => 'Créditos',
      'titulo_ingles' => 'Credits',
      'texto_espanol' => 'Créditos',
      'texto_ingles' => 'Credits',
      'orden' => 1,
      'created_at' => NULL,
      'updated_at' => NULL,
      'paginas' => 
      array (
        'es' => 
        array (
          0 => 
          array (
            'id' => 3,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/00_comienzo/es/00000002.jpg',
            'titulo' => 'Página 2',
            'texto' => 'Diseño Gráfico:
Raúl Bulgheroni

Ilustraciones:
Roberto Tomasello (óleo), A. C. Díaz Domínguez (dibujos).

Fotografías:
Marcelo Beccaceci, Claudio Botti, René Cisneros, Hugo Corbella, Andrés Johnson, Juan Kuriger, Diego Ortiz Mugica, Osvaldo Pons, Roberto Sampaolesi, Atilio Spinello, Archivo Editorial Abril S. A.; Archivo Editorial Atlántida S. A., Archivo General de la Nación, Dirección Nacional de Turismo, Revista Geografía Americana, Revista Plus Ultra.
Derecho de Propiedad Intelectual N°16.757

Primera edición: Fundación Alejandro Angel Bulgheroni Botto. Morgan Internacional (Argentina). 1999.
Edición Digital: 2002








© Raúl Bulgheroni

Hecho el depósito que marca la Ley 11.723 I. S. B. N. 987-98365- 4-5
Todos los derechos reservados.',
            'orden' => 5,
            'idioma_id' => 1,
            'capitulo_id' => 14,
            'created_at' => '2019-01-12 19:01:15',
            'updated_at' => '2019-01-12 19:01:15',
          ),
          1 => 
          array (
            'id' => 4,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/00_comienzo/es/00000003.jpg',
            'titulo' => 'Página 3',
            'texto' => '',
            'orden' => 6,
            'idioma_id' => 1,
            'capitulo_id' => 14,
            'created_at' => '2019-01-12 19:01:15',
            'updated_at' => '2019-01-12 19:01:15',
          ),
        ),
        'en' => 
        array (
          0 => 
          array (
            'id' => 8,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/00_comienzo/en/00000002.jpg',
            'titulo' => 'Page 2',
            'texto' => 'Graphic Design
Raúl Bulgheroni

Illustrations
Roberto Tomasello (oil paintings), A. C. Díaz Domínguez (drawings).

Photo
Marcelo Beccaceci, Claudio Botti, René Cisneros, Hugo Corbella, Andrés Johnson, Juan Kuriger, Diego Ortiz Mugica, Osvaldo Pons, Roberto Sampaolesi, Atilio Spinello, Archives of Editorial Abril S. A., Archives of Editorial Atlántida S. A., Argentine National Archives, National Tourism Directorate, Geografía Americana Magazine, Plus Ultra Magazine.

English Edition:
Translated into English by John E. Landers and Rita P. Landers of the Language Service Bureau, Washington, D.C., U.S.A.

Intellectual Property Rights N° 16.757

First Edition: Fundación Alejandro Angel Bulgheroni Botto. Morgan Internacional (Argentina). 1999.
Digital Edition: 2002

© Raúl Bulgheroni

Filed in accordance with Law 11,723 I. S. B. N. 987-98365- 4-5
All rights reserved.',
            'orden' => 5,
            'idioma_id' => 2,
            'capitulo_id' => 14,
            'created_at' => '2019-01-12 19:01:15',
            'updated_at' => '2019-01-12 19:01:15',
          ),
          1 => 
          array (
            'id' => 9,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/00_comienzo/en/00000003.jpg',
            'titulo' => 'Page 3',
            'texto' => '',
            'orden' => 6,
            'idioma_id' => 2,
            'capitulo_id' => 14,
            'created_at' => '2019-01-12 19:01:15',
            'updated_at' => '2019-01-12 19:01:15',
          ),
        ),
      ),
    ),
    1 => 
    array (
      'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/thumbnail.jpg',
      'imagen_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/thumbnail.jpg',
      'imagen_titulo' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000005.jpg',
      'imagen_titulo_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000005.jpg',
      'titulo_espanol' => 'Introducción',
      'titulo_ingles' => 'Introduction',
      'texto_espanol' => 'Introducción',
      'texto_ingles' => 'Introduction',
      'orden' => 2,
      'created_at' => NULL,
      'updated_at' => NULL,
      'paginas' => 
      array (
        'es' => 
        array (
          0 => 
          array (
            'id' => 12,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000006.jpg',
            'titulo' => 'Página 6',
            'texto' => 'ADVERTENCIA Esta publicación es un intento de brindar un mayor acerca- miento a la realidad que constituye nuestro país, a través del doble camino de la palabra y la imagen. La primera, como fabulosa conquista del género humano que fue estructuran- do fonemas a través de los cuales no solo acuñó un código de intercambio vivencial, sino que además, por su forma es- crita comunicó sus experiencias y fue grabando la historia de los hechos y los pensamientos; la segunda –también re- querimiento ancestral de la especie, desde las prehistóricas artesanales pictografías hasta los técnicos hologramas con- temporáneos– captando mágicamente la hiperrealidad del mundo visible. Dos caminos permanentes y comunicativos para tratar de presentar Argentina y sus habitantes, a nuestros amigos del país y del extranjero. La publicación está compuesta por un volumen introducto- rio a la colección y cinco entregas de dos volúmenes cada una. El primero con un texto o relato acronológico y profusa documentación gráfica que procuran abarcar y vincular la mayor cantidad de factores geográficos, históricos y cultu- rales intervinientes; el segundo volumen está integrado por breves y jerarquizadas comunicaciones de distinguidos especialistas en los temas propios de la región, cada uno de los cuales permitirá una profundización mayor en su campo. Cada libro abarcará una diferente región del país, no ne- cesariamente coincidente con las divisiones políticas en pro- vincias. Primer Volumen:	Introducción a la Argentina Segundo Volumen:	Summa Patagónica, Antártida e Islas Australes Tercer Volumen:	Summa Andina Cuarto Volumen:	Summa Chaqueña Quinto Volumen:	Summa Pampeana Sexto Volumen:	Summa Metropolitana El desarrollo de la zona está enfocada como el juego con que interactúan los polos estructurales: Naturaleza y Hombre, y cuyo resultado es el proceso histórico cultural que nos define y que ofrecemos a consideración de los lectores.',
            'orden' => 4,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          1 => 
          array (
            'id' => 13,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000007.jpg',
            'titulo' => 'Página 7',
            'texto' => 'Alejandro Angel Bulgheroni Botto (óleo de Roberto Tomasello) La Fundación que hoy lleva su nombre, publica en su memoria, esta colección de Argentina, Imagen de un País.',
            'orden' => 5,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          2 => 
          array (
            'id' => 14,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000008.jpg',
            'titulo' => 'Página 8',
            'texto' => 'Mapa de Diego Homen (1558 British Museum). Desde 1554 Homen ha- bía llamado Terra Argentia a lo que es hoy Argentina.',
            'orden' => 6,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          3 => 
          array (
            'id' => 15,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000009.jpg',
            'titulo' => 'Página 9',
            'texto' => 'PRÓLOGO Realizar un libro sobre Argentina lleva implícito dos cues- tionamientos inmediatos: ¿Por qué? ¿Para quién? Cualquier paseante de nuestras ciudades (especialmente de Buenos Aires, donde inexcusablemente en algún momento pasan casi todos los habitantes del país y los extranjeros que lo visitan), encuentra en los escaparates de sus excelentes librerías, una profusión de publicaciones sobre Argentina, que comprenden bellos libros de arte, de fotografías, guías turísticas, descripciones de nuestra naturaleza, serios estu- dios de pensadores sobre nuestra historia o actualidad, li- bros científicos, filosóficos y narrativos, la creación de nues- tros literatos y poetas. En todos y en cada uno de ellos hay una parte profunda y real de nuestra Argentina, pero no es sino a través de una amplia y polifacética información y también a través de un largo tiempo de haber vivido conscientemente en el país, que es posible lograr que los distintos hilos de cada estudio emerjan de su contexto y se vayan combinando, de tal suer- te, que comience a bosquejarse la trama que sustentará la imagen integral de la Argentina. Naturalmente este libro no pretende realizar tal síntesis ci- clópea, pero si aspira a presentar elementos objetivos que sirvan como estímulos para recapacitar sobre el tema. Decimos integral, sin que ello implique hablar de una enti- dad estructurada, concreta y persistente, porque no existe como tal una Argentina. Existen simultáneamente muchas Argentinas, distintas, contrastantes y aún opuestas, pero que sin embargo, llegamos a sentir como misteriosamente ligadas: aspecto o partes de una misma unidad. Esa cualidad de unidad del conjunto, como en la genética de los organismos vivientes, precede a las partes, las que por sí no tienen existencia o significación válida fuera del conjunto. Es un determinado tipo o modalidad de integración de esas partes, lo que dota al conjunto de una cualidad (inherente al mismo) que finalmente define lo que el organismo tiene de único y estructuralmente original. Al igual que en el con- cepto de Gestaldt, un país es un todo que no se resume en la suma de sus partes.',
            'orden' => 7,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          4 => 
          array (
            'id' => 16,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000010.jpg',
            'titulo' => 'Página 10',
            'texto' => 'Como en el reino natural al observar una hoja podemos reconocerla como roble (aunque no existe en todo el ár- bol, ni en todo el robledal, otra absolutamente similar); como en la pintura de un artista (no obstante las varia- ciones temáticas y cromáticas que haya usado), podemos reconocer la personalidad del autor; de la misma manera, la imagen de un país es aprehensible como una unidad resultante, enriquecida de la variedad infinita de sus rea- lidades parciales. En una palabra, se constituye en un or- ganismo viviente en el que, como tal, la vida se manifies- ta por una superación y multiplicación de sus cualidades vitales. Inicialmente todo país surge de una región, una comarca con determinadas características naturales y un grupo humano que ejercitando el primordial instinto de territorialidad, se va apropiando de espacio físico, lo recorre, define, utiliza, modifica de acuerdo a sus necesidades y termina consustanciándose con él, generando lazos e afectivos que conforman la noción de patria. Esas necesidades surgen de un código natural de conducta destinado, inicialmente, a la auto preservación del grupo por sobre la del individuo, a dar continuidad y coherencia a la naciente comunidad. El código natural se transformará, posteriormente, en normas explícitas, en una suerte de éti- ca que irá pautando la idiosincrasia del grupo. Al mismo tiempo, desarrolla hacia el medio y hacia los otros miembros de la comunidad, su capacidad de percibir, como acto sensorial y su capacidad de interpretación y valoración, como acto psicológico, dando paso a una inicial estética de vida y de expresión. Con este enfoque podemos concebir la generación de un país como la conjunción de actos éticos y estéticos de una comunidad asentada en un territorio determinado. Por consiguiente, para compenetrarnos con la esencia real de esa entidad, requiere también de nuestra men- te una actividad ética y estética que nos lleve a meditar, comprender e identificarnos con ella, lo que en el decir de Worringer, sería el acto completo de la contemplación estética. Tenemos por una parte: A) el mundo físico (natural o construido) que podemos asumir como hitos, monumentos o escenarios de las B) constantes asociaciones del hombre (in- dividual o colectivo) enlazadas a través del tiempo por la memoria, como una conciencia continuada. Una interacción recíproca de hombre y naturaleza con, tam- bién, recíprocos y alternados sometimientos que se expresan en conductas o diseños, siempre en procura de encontrar a través de ellos una acentuada armonía; surge inmediatamente el parentesco etimológico de las palabras latinas: mens, memoriae, memento, monumentum (mente, memoria, recordatorio, monumento).',
            'orden' => 8,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          5 => 
          array (
            'id' => 17,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000011.jpg',
            'titulo' => 'Página 11',
            'texto' => 'El monumento tomado con un sentido más amplio que el li- teral físico o cultural, plasma materialmente la continuidad de esa función viva que es la memoria, en todas las dimen- siones del tiempo: el recuerdo del pasado, la conciencia del presente y la previsión del futuro: re- sentimiento	sentimiento	pre- sentimiento (experiencia)	(acción)	(proyección) Estas realizaciones materiales constituyen jalones donde la Humanidad ha dejado impresa la señal de su marcha. A tra- vés de ellos, aprendemos a recrear la historia que, según el juicio de Leibnitz, es la memoria de la especie y de la cual debemos extraer la lección de aquellos momentos que es- tructuraron nuestro presente y que condicionarán nuestro futuro; asimismo, revalorar la resultante final de la conducta y acción de un grupo humano en uso del entorno físico en el cual se estableció y, motivado por los requerimientos de su idiosincrasia, se expresó a través de creaciones físicas y espi- rituales que llamamos cultura. Esa cultura no debe entenderse como proceso acabado, sino como producto sutil y capaz de modificarse constantemen- te con los cambios de las convenciones que prevalecen en el grupo (sociales, económicas, religiosas, etc.). En síntesis, para nuestro cuestionamiento inicial, podemos decir que esta publicación es una tentativa por mostrar un país que busca su identidad política, su desarrollo econó- mico, su calidad y estilo de vida en el caleidoscopio de las muchas Argentinas que conviven en sus diferentes regio- nes, así como en su Capital que se manifiesta como una suerte de sedimentada cristalización de su historia, aspira- ciones y realizaciones. Tentativa, también, por hacer comprender un pueblo espa- ñol por origen, con escasos pero variados aportes indígenas que han ido estructurando su idiosincrasia a través del completo tejido urdido por todas las razas que llegaron y se arrai- garon en el país. País con una situación geográfica aislada en el sur, que le hizo mirar siempre más allá del mar y que ha generado problemas para su inserción en el contexto de América Latina. Buscamos mostrar la calidad del país en documentos e imágenes que capten el instante en su tiempo, de los acontecimientos circunstanciales que nos estructuran y la naturaleza o realidad permanente que nos alberga y nutre: el tiempo y el espacio o donde se genera nuestra identidad provistos por la historia y la geografía; des- mitificar con objetividad los remanidos clichés mentales –que sólo tienen validez turística– y honrar al pasado a través de testimonios, como un punto de partida hacia el futuro.',
            'orden' => 9,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          6 => 
          array (
            'id' => 18,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000012.jpg',
            'titulo' => 'Página 12',
            'texto' => 'Esto conduce a clarificar, sincerar calificaciones que nos da- mos como reales pero que no son más que posibilidades que necesitan ser puestas en movimiento y desarrollarse para que constituyan una realidad. Podremos así, considerar al país y al hombre argentino como una interacción entre el concepto abstracto que de ellos se acuña y la realidad genuina, para ponderar el dinamismo o tensión que requiere nuestro momento, en miras a aproxi- marnos al ideal del concepto abstracto. Finalmente, esta publicación es una intención para en- contrar respuestas a qué es la Argentina hoy y cuáles son sus características permanentes, para tratar de conocer- nos mejor y para que también así lo hagan los amigos del país, a través de informaciones objetivas, como estímulos para que cada lector lo complete, desarrollando sus pro- pias conclusiones. Acaso Paul Valéry no dijo: ¿No será que Dios dejó inconcluso el mundo para que lo concluyeran los hombres?',
            'orden' => 10,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          7 => 
          array (
            'id' => 19,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000013.jpg',
            'titulo' => 'Página 13',
            'texto' => '',
            'orden' => 11,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          8 => 
          array (
            'id' => 20,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000014.jpg',
            'titulo' => 'Página 14',
            'texto' => 'La imagen de la realidad presenta sugestivas resonancias, como lo muestran las fotografías de Osvaldo Pons, tomada a esca- sos milímetros de un cristal trizadoy la de Hugo Corbe- lla, desde gran altu- ra, sobrevolando los hielos continentales en la cabecera del glaciar Viedma.',
            'orden' => 12,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          9 => 
          array (
            'id' => 21,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000015.jpg',
            'titulo' => 'Página 15',
            'texto' => '“La patria fue para mí una presencia constante, un estado de ánimo, una música callada, la herida de una frustración, un rapto de alegría, un entusiasmo que desfallece, una perple- jidad, una esperanza que sobreviene a toda prueba. Es decir, el país tenía para mí, las formas sordas, borrosas y entraña- bles de un sentimiento”. El sentir de Víctor Massuh es una forma también válida de definir la Argentina, si bien es cierto que tal pensamiento adscribe al sentido de patria que como tal, es valido tam- bién para cada habitante del planeta, cuando piensa en su lugar natal. Dice también nuestro poeta: “Patria es la tierra donde se ha nacido Patria es la tierra donde se ha soñado Patria es la tierra donde se ha luchado Patria es la tierra donde se ha sufrido”. Poniendo siempre el acento en esa corriente profunda que subyace en la psiquis del hombre; corriente a veces doloro- sa, a veces resentida, pero siempre cargada de nostalgia y de una cierta irracional afectividad similar a la evocación de nuestra infancia: de la casa paterna donde se opera el mi- lagro de la comprensión de los momentos conflictivos y la añoranza -enriquecida por la distancia en el tiempo- de las situaciones gratas. No importa en qué ignorado pueblito o gran ciudad haya- mos nacido: sus calles, polvorientas o asfaltadas, el olor y el sonido de sus árboles o de la fábrica próxima, el horizonte cerrado por altos edificios o abierto hacia el paisaje, aún en- vían sus mensajes a nuestros sentidos. No importa en qué confortable o común casa haya al- bergado nuestra infancia, siempre permanecerán vi- gentes los oscuros rincones donde guardábamos nues- tros fabulosos tesoros o donde preferíamos permanecer y en los que comenzamos a encontrarnos con nosotros mismos. No importa cuán importantes o ignorados, virtuosos o descarriados hayan llegado a ser nuestros compañeros de niñez y adolescencia; o aquellas personas que de alguna manera ingresaron a nuestra historia individual, sus pre- sencias emergen como la vía por la cual, transponiendo',
            'orden' => 13,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          10 => 
          array (
            'id' => 22,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000016.jpg',
            'titulo' => 'Página 16',
            'texto' => 'el natural y concreto umbral familiar, nos conducen inicial- mente hacia el otro, los demás: la sociedad que comenzó a estructurarnos. De la misma manera que el fuerte vínculo de una auténtica amistad se establece a través de lugares y experiencias com- partidas, así también el continuo y sutil juego de esas fuer- zas interactuantes en una escala social, nos liga a la idea del país provista por el lugar y el tiempo, el ambiente y la histo- ria de sus habitantes. ¿Cómo definir a la Argentina? ¿Cómo caracterizarla? Más que símbolos estructurados, más que históricos clisés reiterativos, más que asociaciones particulares parcialmente justificables (de acuerdo a las experiencias particulares de habitantes y visitantes, los land mark o símbolos invariables deben ex- traerse de imágenes o recuerdos con emoción nacional que involucra lugares, personajes, arte, leyendas, acontecimien- tos, abarcando la totalidad de los mismos, aún aquellos mo- mentos virulentos de crisis o depresión en el proceso históri- co que, aunque los hayamos superado y deseemos olvidarlos, también contribuyen a la formación de una auténtica idea de lo nacional. No hay un sólo símbolo; aún aquellos más dignos y consa- grados no pueden, por sí solos, expresar la esencia de la Argentina y la argentinidad. La mayoría de los símbolos tampoco permanecen fijos en el tiempo. Los ejemplos históricos son de naturaleza variable y signados en su valor de acuerdo con el momento y la circuns- tancia. Las sucesivas generaciones tienen de ellos lecturas di- ferentes, al par que acuñan nuevos hitos de singular fuerza evocativa para su propio presente. Las características esenciales de un país, en cambio, de- rivan de complejos e intangibles rasgos que apelan tan- to al sentimiento como a la mente y que por un proceso interior afloran consustanciados, integrados en símbolos genuinos y atemporales que tienen más relación con una suerte de empatía, de reconocimiento de familiaridad y mutua pertenencia, que con una estructura física o visual concreta. “… Las naciones hijas de la guerra, levantaron por insignias, para anunciarse a los otros pueblos, lobos y águilas carniceras, leones, grifos y leopardos. Pero en la de nuestro escudo, ni hipogrifos fa- bulosos, ni unicornios, ni aves de dos cabezas, ni leones alados que pretenden amedrentar al extranjero. El sol de la civilización que al- boreaba para fecundar la vida nueva; la libertad con el gorro frigio sostenido por manos fraternales como objeto y fin de nuestra vida; una oliva para los hombres de buena voluntad, un laurel para las no- bles virtudes… “Nuestro signo, como Nación reconocida por todos los pueblos de la tierra, ahora y por siempre, es esa Bandera, ya Escudo Nacional del sello de 1813 en la car- ta de ciudadanía del General Arenales',
            'orden' => 14,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          11 => 
          array (
            'id' => 23,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000017.jpg',
            'titulo' => 'Página 17',
            'texto' => 'sea que nuestras huestes trepen los Andes con San Martín, ya sea que surquen am- bos océanos con Brown, ya sea, en fin, que en los tiempos tran- quilos que ella presagió. Se cobije a su sombra la inmigración de nuevos arribantes, trayendo las bellas artes, la industria y el comercio… “… Muchas repúblicas la conocen como salvadora, como auxiliar, como guía en la difícil tarea de emanciparse. Algunas se fundaron a su sombra; otras brotaron de los jirones que en la lid la desgarró. Ningún territorio fue, sin embargo, añadido a su dominio; ningún pueblo quedó absorbido en sus anchos pliegues; ninguna retribu- ción exigida por los grandes sacrificios que nos impuso… “… Esta bandera cumplió ya la promesa que el signo ideográfico de nuestras armas expresa… “… Las fajas celestes y blancas son el símbolo de la soberanía de los reyes españoles sobre los dominios, no de España, sino de la Corona, que se extendían a Flandes, a Nápoles, a las Indias; y de esa banda real hicieron nuestros padres divisa y escarapela, el 25 de Mayo, para mostrar que del pecho de un rey cautivo tomába- mos nuestra propia soberanía como pueblo, que no dependió del Consejo de Castilla, ni de ahí en adelante, dependería del disuelto Consejo de Indias. “El general Belgrano fue el primero en hacer flotar a los vientos la Bandera Real, para coronarnos con nuestras propias manos sobe- ranos de esta tierra, e inscribirnos en el gran libro de las naciones que llenan un destino en la historia de nuestra raza. Por este acto elevamos una estatua en el centro de la plaza de la Revolución de Mayo, al general Porta-Estandarte de la República Argentina…” Domingo Faustino Sarmiento: Discurso de la Bandera en la inau- guración de la esta- tua de Belgrano, es- cultura encargada al taller del francés Carier-Belleuse, donde trabajaba el escultor argentino Manuel de Santa Coloma, quien rea- lizó el caballo (24 de septiembre de 1873).',
            'orden' => 15,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          12 => 
          array (
            'id' => 24,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000018.jpg',
            'titulo' => 'Página 18',
            'texto' => 'Existe una Argentina física: enorme polifacética, contras- tante y desconocida; existe, también una Argentina cir- cunstancial y cotidiana que muchas veces juzgamos desde la perspectiva de nuestra anécdota, con un sentido pesimista y donde todo es adverso: posibilidades de desarrollo, cultura, política. Expresiones cotidianas como: este país, aquí no se puede hacer nada, etc., que impulsan particularmente a grandes a grandes corrientes de nuestra juventud “a cualquier parte, para hacer cualquier cosa” representa una exageración depresiva, una ex- cusa escapista que lleva a dar la espalda a la acción y a transfe- rir las responsabilidades individuales a un abstracto destinata- rio que debió habernos ahorrado el trabajo de nuestra propia realización: los demás, el gobierno, el país. Muchas veces, sin embargo, y debido a esa falta del con- cepto de medida -a esa oscilante proclividad por ensalzar y denostar sin la necesaria serenidad en la evaluación- que caracteriza a los argentinos en sus juicios totalizadores, se acuña también una Argentina ideal o abstracta, triunfalista, superior, única, que si bien posee elementos caracterizado- res válidos, no puede ajustarse exactamente con la realidad territorial e histórica. ¿Cuál es pues la Argentina real? La respuesta no puede ser otra que la interacción de las tres imágenes que, de una ma- nera similar a los antiguos visores estereoscópicos de placas fotográficas, al producir la superposición retinal de las imá- genes, nos ofrecían una presencia tridimensional del objeto fotografiado. LA NATURALEZA La tierra, el clima, el paisaje es uno de los estructurantes básicos de la calidad del país que nos alberga y cuya Constitución lo ofreció generosamente, desde el co- mienzo de nuestra nacionalidad, a to- dos los hombres de buena voluntad, que quieran habitar el suelo argentino. La República es un largo y extenso terri- torio desmembrado de la unidad virrei- nal mayor, creada en 1776 por Carlos II y de la cual en el proceso histórico, se fueron segregando porciones que hoy pertenecen a otros países como Chile y Brasil o constituyen naciones inde- pendientes como Bolivia, Paraguay y Uruguay. Esta extendida faja es casi una suerte de península gigantesca donde el con- tinente americano se avecina al conti- nente antártico, bañados por los dos mayores océanos del planeta y se pro- longa en archipiélagos atlánticos y un sector atlántico. Su posición geográfi- ca lo constituye en un final de itine- rario; no en un país de paso, sino de llegada por lo que para acceder a él debe mediar una expresa voluntad por conocernos. El país –a lo largo de aproximadamente 3.700 Km de norte a sur y un ancho va- riable en dirección este-oeste que lle- ga en su parte superior hasta los 1.400 Km– contiene, simultáneamente, todos los climas y topografías imaginables. En una extensión de aproximadamen- te 4.000.000 de Km2 conviven las cua- tro estaciones, la selva y el desierto, la montaña y la llanura, el mar, la flora, la fauna y los minerales más diversos.',
            'orden' => 16,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          13 => 
          array (
            'id' => 25,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000019.jpg',
            'titulo' => 'Página 19',
            'texto' => 'Aspecto político en las inmediaciones del continente Antártico donde  se observa las zonas que son reclamadas por distintos países, especial- mente Gran Bretaña, que anexa en su llamada “Dependencia de las Falkland” la zona Antártica e Islas donde existen innegables derechos argentinos. Argentina y Chile, aproximadamente a 1.170 Km del círculo ppolar, son las naciones más próximas al Continente Blanco, quedando en 2º lugar Nueva Zelandia ya a 2.170 Km.',
            'orden' => 17,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          14 => 
          array (
            'id' => 26,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000020.jpg',
            'titulo' => 'Página 20',
            'texto' => 'Un oeste vertebrado por la Cordillera de los Andes, gi- gantesca cadena de montañas nevadas, que desde la cumbre más alta de América, el Aconcagua, descien - de en estribaciones menores que penetran el territorio hacia el Oriente, formando –según la latitud– pétreas quebradas de deslumbrantes formas y colores; cerradas selvas subtropicales con floridos gigantes, orquídeas y helechos; cañones resecos y erosionados, escultóricos paisajes cargados de sugestiones míticas; fríos bosques de hayas, araucarias pehuén y arrayanes, enormes par- ques naturales con cascadas, lagos y glaciares de una imponente belleza; valles fértiles, de riquísima produc- ción, a la vera de los ríos que descienden en dirección al mar o regados por las importantes obras con las cuales la ingeniería del hombre fue organizando los torrentosos caudales. El tropical noreste y la Mesopotamia es un enjambre de caudalosos ríos que convergen hacia el Plata en el mayor es- tuario del mundo, liderados por el majestuoso Paraná (vía fluvial de aproximadamente 4.700 Km), que como gigan- tesca y mítica lampalagua baja ondulante, pesado y oscuro. Esta intrincada red en la cual se espeja una vegetación luju- riante y desmedida va cercando subzonas de esteros y baña- dos, de tierras fecundas donde se desarrollan algodonales y arrozales, plantaciones de té, yerba mate, tung, tabaco y frutales, implantos industriales de ricas maderas, parques de palmares y las espectaculares cataratas del río Iguazú, que al volcar sus aguas desde casi 100 metros de altura, en más de 270 saltos, constituye una de las maravillas naturales más importantes del planeta. El agua densa semeja piedras de- rretidas que se transmutan en vaporosas nieblas multicolo- res, para que en ellas dancen innumerables pájaros; verdes piedras sobre suelo rojo que al aproximarse el hombre esta- llan en lagartijas como flechas disparadas a todos los rum- bos; el cielo azul cobalto y el sol redondo y neto que se ocul- ta sin celajes, para dar paso a una noche oscura, poblada de sonidos y presencias, que nos transportan a la fascinación y al misterio descriptos por Horacio Quiroga.',
            'orden' => 18,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          15 => 
          array (
            'id' => 27,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000021.jpg',
            'titulo' => 'Página 21',
            'texto' => 'El centro o la pampa, es a decir de Borges, una entidad que no puede ser contenida en una imagen, sino en una serie de procesos mentales. Su infinitud se hace comprensible únicamente a través de la memoria de una experiencia continua e idénticamente reiterada. Una llanura aplastada y monótona de dimensión inasible, contrapuesta a un cielo abierto e inmenso, casi materialmente presente y abrumador. La región abarca las fértiles planicies en donde se origina la riqueza agrícolo-ga- nadera argentina y va mutando en sus límites con salares y depresiones, pastizales duros, hasta el borde mismo de la aridez del desierto. Como escribía F. Head en sus notas tomadas al azar a través de las pampas (Londres 1826): “La extensión de las pampas es tan prodigiosa que hacia el norte, están bordeadas por bosques de palma- res y hacia el sud por las nieves eternas”. La Patagonia, Antártida, e islas australes constituyen otra región, también de una gran diversificación pero no obstan- te, con el común denominador de constituir el lejano sur, aún para los propios argentinos y en una acepción que exce- de a la mera geografía. Un litoral marítimo, que varía desde amplias y serenas playas a acantilados abruptos, rías y fiordos que maravillaron a los primeros navegantes europeos, las casi desconocidas tierras antárticas; islas calladas y ventosas; mesetas desérticas, testi- gos de un milenario pasado en sus bosques petrificados, o con grandes pastizales en donde se crían innumerables rebaños de ganado ovinos; los bosques, glaciares y lagos cordilleranos, toda la expresión pura de la fuerza de la naturaleza y parti- cularmente la escala, asignan al paisaje una belleza que sólo puede expresarse con el silencio. Resulta pertinente recordar a Humboldt cuando decía: “Al igual que el océano, las estepas em- bargan el espíritu de un sentimiento de lo infinito”. Los yacimientos minerales: petróleo, hierro, carbón, etc., las riquezas de sus mares, praderas y bosques, configuran un verdadero desafío para la iniciativa del Hombre.',
            'orden' => 19,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          16 => 
          array (
            'id' => 28,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000022.jpg',
            'titulo' => 'Página 22',
            'texto' => 'Y arribamos con él, al segundo factor estructurante: el ha- bitante, cuya densidad en el territorio es aproximadamente de sólo 7 personas por Km2. Sarmiento escribía en el Facundo: “La inmensa extensión del país... está enteramente despoblada y ríos navegables posee, que no ha surcado aún el frágil barquichuelo. El mal que aqueja a la República Argentina es la extensión: el desierto la rodea por todas partes, se le insinúa en las entrañas; la soledad y el despoblado sin una habitación humana, son por lo general los límites incuestionables entre una y otra provincia. “Allí la inmensidad por todas partes: inmensa la llanura, inmensos los bosques, inmensos los ríos, el horizonte siempre incierto, siempre confundiéndose con la tierra entre celajes y vapores tenues, que no de- jan en la lejana perspectiva señalar el punto en que el mundo acaba y principia el cielo”. Esta extensión y soledad que puede sonar un tanto extraña a los oídos del abigarrado habitante del Gran Buenos Aires, no representa intrínsecamente un mal, sino por el contrario, un don increíble de la naturaleza que aguarda expectante, que el hombre que la posee, se consustancie con ella y la mande producir, dirigiendo y cuidando su desarrollo en pro- cura del mutuo beneficio. La madre tierra y el hombre padre generando el fruto de la voluntad y del amor. EL HOMBRE Una crítica humorística y ácida circulaba habitualmente en- tre los funcionarios de los grandes organismos internaciona- les: “El gran negocio: comprar argentinos por lo que valen y vender- los por lo que ellos se creen que valen”. ¿Quiénes somos los argentinos? ¿Existe un prototipo como tal? Nuestra estructura étnica tiene básicamente tres raíces: a) La indígena y el mestizaje; b) El criollo de origen español; c) Los innumerables y variados aportes de la inmigración. a) Cuando los españoles llegaron a América encontraron tierras pobladas por diferentes razas y culturas. Lo que actualmente es la República Argentina, por consiguiente, no fue una excepción. Si bien es cierto que no existió aquí, una civilización deslum- brante como la azteca, maya o inca, los habitantes origina- les de nuestro territorio, configuraron diversas culturas de variadas e interesantes manifestaciones. Desde el Paleolítico, según Salvado Canals Frau, intervinie- ron en el poblamiento indígena de Argentina seis diferen- tes tipos raciales: los Huárpidos, Láguidos, Patagónidos, Brasílidos, Andinos y Fueguinos, que comprenden más de 22 familias o tribus diferenciadas. Sin entrar a considerar otras teorías (Ameghino y otros) so- bre la originalidad de hombre americano, los análisis radio- activos del Carbono 14 han permitido datar fehaciente- mente sucesiones estratigráficas de culturas prehistóricas, encontrándose niveles –especialmente en Patagonia– de una antigüedad no menor al décimo milenio anterior a Cristo, y pictografías que coinciden con la técnica y códigos de representación empleados en el Paleolítico superior en Europa y Australia. Uno de los mitos que debe ser descalificado en la mente de la generalidad de los argentinos actuales, es la negación de la presencia aborigen en nuestras raíces. Si bien es cierto que no tuvieron una organización muy fuerte y perdurable y que su aniquilamiento como razas puras fue un proceso progresivo (iniciado con la conquista hispana, acentuado en el período de nuestra independen- cia hasta casi nuestros días, donde han quedado solamente pequeños núcleos aislados en una irremediable etapa de transculturación), no es menos cierto que su presencia, ét- nica y cultural, más o menos pura en el mestizaje, subsiste en grandes sectores del habitante argentino. La pervivencia de esa cultura está más expandida y presente que en los núcleos de reservaciones o en los estudios antropológicos. Se manifiesta en la vida diaria, en la personalidad y modalida- des que caracterizan a un gran',
            'orden' => 20,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          17 => 
          array (
            'id' => 29,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000023.jpg',
            'titulo' => 'Página 23',
            'texto' => 'sector de habitantes de distintas zonas del país, que solemos llamar criollos, por el prurito de no reconocer oficialmente el mestizaje; en sus regímenes alimen- ticios, artesanías, creencias y supersticiones, en escala de valores y patrones de conducta, y fundamentalmente, por esa maravi- llosa concreción del hombre que al par que le permite expresar- se, lo revela en toda su idiosincrasia e historia: el lenguaje. Nuestro hablar cotidiano está lleno de expresiones y giros provenientes de esas culturas. A título de ejemplo: pala- bras como chacarero, que casi asumimos como sinónimo de gringo, es quechua y nos habla ya del nacimiento de la agricultura y fruticultura en la chacra de nuestros indígenas antes del descubrimiento y en el período colonial. Hasta el argentinísimo che por el cual nos reconocen en el resto de América Latina es indígena. Por último, la gran cantidad de negros, que los esclavistas in- gleses autorizados por el rey de España, vendían en el mer- cado de Retiro, tampoco se desvaneció en el aire: zambos y mulatos a través de variadas y múltiples cruzas, permanecen presentes en sectores de nuestra población. b) Nuestra segunda raíz la constituye el criollo “Criollo soy de México, que es nombre que dan las Indias al que en ella nace”. (Tirso de Molina) Así, el hijo de europeo –para nuestra legendaria América, específicamente del español– nacido en el territorio, co- mienza a adquirir un nombre concreto. La influencia telúrica del Nuevo Mundo hace cambiar poco a poco la idiosincrasia del español nacido en él. Ya el geógrafo López de Velazco (entre 1571-1574) señala en ellos “diferencias en la color y el tamaño”; Montalvo en 1579, desde Asunción puntualiza: “una conducta de poco respeto hacia sus padres y mayores, en criollos y mestizos” y Juan de Cárdenas en su Problemas y Secretos Maravillosos de las Indias, al compararlos con el español europeo, califica al criollo como “más delicado, más discreto y pulido”. Criollo tiene la misma raíz que crear-criar en sus acepciones: “dar ser a lo que antes no lo tenía o bien en el sentido de instruir o es- tablecer una nueva dignidad”. Esa nueva dignidad que comienza paulatinamente a manifes- tarse en nuestros habitantes se fue desarrollando a través de una revisión crítica, histórica y contemporánea, y una aper- tura hacia nuevas formas del pensamiento. Al igual que el carácter de la España de la Conquista, oscilante entre la me- dievalidad y el pensamiento moderno renacentista, la raza criolla en formación retuvo principios de orden tradicional, alternados con estructuras del pensamiento moderno. En forma paralela refuerza su fidelidad a la raíz, pero modifica- da por el arraigo a la realidad presente y enriquecida por la incorporación de lo ajeno universal. Salvando la parcialidad del enfoque, no dejan de ser signifi- cativas las observaciones realizadas por dos conocidas perso- nalidades inglesas sobre la hispanidad de la Colonia, donde ya la criollez había desarrollado su carácter, el segundo. “No puedo menos que alabar la paciente virtud de los españoles. Raramente o nunca nos es dado encontrar una nación que haya sufri- do tantas desgracias y miserias… persistiendo empero en su empre- sa con constancia invencible… Tempestades, naufragios, hambre, de- rrotas, alzamientos, sol abrasador, frío, pestes, pobreza extrema… son sus nobles descubridores muchos años trabajaron perdiendo fortuna y vida” (Sir Walter Raleigh, siglo XVII). “Las vastas llanuras de Buenos Aires no están pobladas sino por cris- tianos salvajes conocidos por ‘huachos’ (gauchos) cuyo principal amo- blamiento consiste en cráneos de caballos; cuyo alimento es carne cruda y agua y cuyo pasatiempo favorito es reventar caballos en carre- ras forzadas. Desgraciadamente prefirieron su independencia nacional a nuestros algodones y muselinas” (Sir Walter Scott, siglo XIX).',
            'orden' => 21,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          18 => 
          array (
            'id' => 30,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000024.jpg',
            'titulo' => 'Página 24',
            'texto' => 'En síntesis, más allá de la encomiada y denostada ascenden- cia europea de nuestros habitantes la criollez se presenta como una primicia biológica cultural y social en el contexto de la civilización occidental. Como puntualiza Silvestre Byrón: “Para exasperación del saber científico, ya escarnecido por la cuestión del origen de la población autóctona del continente, un tramado de trazas y civilizaciones encon- tradas pergeñó –hazaña de la sangre y la simiente del mestizaje– un tipo étnico novedoso, variado y singular. El elemento de orden de la criollez no reside en la pureza de la raza, ni en la superioridad de la estirpe, sino en la frescura que la caracteriza; y por lo mismo, en su promesa de ser”. c) Por último, el tercer ingrediente que ha de sazonar y di- versificar el sabor de la argentinidad: el inmigrante. El rei- terado chascarrillo sobre nuestra ascendencia, “los argentinos descienden del barco”, involucra implicancias que van más allá del humor. La sentencia de “gobernar es poblar” de aquella excepcio- nal figura que fue Juan B. Alberdi, enuncia una absoluta realidad en nuestro país, que siempre estuvo abierto a los hombres de cualquier parte del mundo, ya desde 1811, ofre- ciendo seguridad y ayuda a los extranjeros que quisiesen radicarse en él. El proceso inmigratorio incorporó al país hombres que pro- fesaban otras religiones, artesanos, agricultores, comerciantes calificados, hombres de la cultura y de la ciencia y aún pobres ignorantes sin más equipamiento que el tesón y la constancia, ductilidad de adaptación y una indómita vo- luntad puesta al servicio de la empresa, que le permitirá ele- varse por sobre la miseria que abandonaba. Al aliento de la tolerancia de nuestras leyes, que no solo les permiten profesar su fe y difundirla, además de la po- sibilidad de una realización económica (“venir a hacerse la América”), nuestro inmigrantes fueron arraigándose y die- ron al país una fisonomía distinta, plural, culta, emprende- dora, a veces inestable y contestataria, que hizo ingresar más rápidamente a la Argentina al mundo moderno. Juan Vigo dice: “Lo más importante que trajo esa inmigración… fue su mentalidad, espíritu y hábitos, formados en una sociedad cru- damente capitalista como la europea, que le dieron plena y decisiva ventaja con respecto al criollo para progresar, enriquecerse y acaparar tierras”. Para ponderar la situación de la mayoría del campesinado u obrero urbano europeo en aquella época (y que consti- tuirían un significativo porcentaje de los hombres “India” detalle de una acuarela de C. Pelle- grini, contratado como ingeniero por Rivada- via, por Rosas como retratista, autor de grandes obras pictóri- cas argentinas. “La porteña en el templo” (detalle), óleo de Monvoisin, represanta el duelo de Rosa Lastra ante el enjuiciamiento de su padre y hermano en 1839.',
            'orden' => 22,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          19 => 
          array (
            'id' => 31,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000025.jpg',
            'titulo' => 'Página 25',
            'texto' => 'que in- gresaron a nuestro país), únicamente hace falta meditar re- cordando las trágicas descripciones sobre la desprotección y miseria de esas capas sociales y las patéticas y arbitrarias vicisitudes vividas por los personajes que (en sus respectivos países), describen autores como Dickens en sus Cuentos de Navidad u Oliverio Twist en Inglaterra; Friedich Spielhagen en Alemania; Edmundo D´Amici en Italia; Máximo Gorky en Rusia; Lorenzo Pinto en Portugal; Vicente Blasco Ibañez, insobornable testigo de los problemas sociales españoles; Scholem Aleijen con Tieve, El Lechero en sus desventuras patéticas y tragicómicas que puntualizan la lucha contra la pobreza e injusticia en el pueblo judío; Víctor Hugo presen- tando en Los Miserables el calvario de su protagonista por el solo crimen de haber robado, impulsado por el hambre, un pan, en Francia. Una síntesis dolorosa y poética la da la poetisa gallega Rosalía de Castro con: “Le vendieron los bue- yes, / le vendieron las vacas, / el pote de caldo, / la manta de la cama. Vendieron el carro / y las huertas que tenía, / le dejaron solo / con la ropa que vestía. María, soy joven, / no quiero mendigar, / me voy por el mundo / para poderlo ganar. Galicia está pobre / y a La Habana me voy… / Adiós, adiós prendas / de mi corazón.” El controvertido tema de los inmigrantes y colonos no puede enfocarse con un sentido absolutista, es decir adjudicándoles todas las virtudes o todos los defectos de nuestra cultura. Tampoco ellos llegaron siempre a la “tierra de promisión”, an- siada o prometida. Este proceso reconoce dos caras diametralmente diferenciadas: desde 1858 con la fundación de Colonia Esperanza, hasta aproximadamente 1890, período éste donde por lo general los inmigrantes fueron avalados y protegidos desde el comienzo por gobiernos provinciales o la nación misma, y épocas posteriores en que fueron en- gañados y explotados, recibiendo trato poco menos que de esclavos, en manos de inescrupulosas compañías nacionales o extranjeras, por medio de reclutamientos engañosos, espe- culación, incumplimientos y abusivos arriendos de las tierras. Emergen de este sector de nuestra historia figuras como el Barón Hirsh, Nicolás Oroño, Bialet Massé y otros, cada uno de los cuales, testigos o protagonistas de la gesta, han deja- do valiosas constancias sobre el tema. Ya desde 1825, al amparo de la política de Rivadavia, empie- zan a establecerse inmigrantes escoceses y daneses en la provincia de Buenos Aires; Urquiza, de su propio peculio, or- ganiza colonias en Entre Ríos y Santa Fe; en 1865, un grupo de 153 galeses, sufriendo la represión económica y cultural de la burguesía inglesa, abandona su país y se establecen en Chubut, como una verdadera avanzada de civilización an- tes de las luchas de Roca contra los indios (con lo que, por otra parte, los galeses tuvieron un trato pacífico, sin sufrir los nacientes poblados “Negra” (detalle) de pulpería de la ciudad, litografía de Isola; italiano establecido en 1844, que captó aspec- tos costumbristas. “Inmigrante” (detalle” acuarela de Pueyrredón, 1870, inscripta en la fundamental obra de retratos y personajes argentinos.',
            'orden' => 23,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          20 => 
          array (
            'id' => 32,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000026.jpg',
            'titulo' => 'Página 26',
            'texto' => 'ningún tipo de violencia). En el sur, Malvinas y Tierra del Fuego, la tarea evangelizadora de los pastores anglicanos, fue generando desde Ushuaia, asenta- mientos humanos donde colonos ingleses compartieron con nativos y pobladores de otras nacionalidades europeas, los esfuerzos para el desarrollo de esa vasta y aislada región. Por problemas religiosos, raciales, políticos o económicos que sufrían en sus propios países, llegaron a la Argentina, en sucesivas oleadas, durante todo el siglo XIX y comienzo del XX, gente de todas las razas: suizos, alemanes, franceses, polacos, rusos, belgas, judíos, austrohúngaros, sirios, libane- ses (que la jerga popular denominó genéricamente turcos) verdaderos turcos islámicos, armenios, griegos, ucranianos, yugoeslavos, lituanos, bóers, japoneses, etc. Dispersos a lo largo y a lo ancho del país, constituyeron colonias, fundaron pueblos y se insertaron, dándole gran aliento y variedad al desarrollo de nuestra economía y cultura. Párrafo aparte merecen los italianos y españoles; su inciden- cia fue tal, que humorísticamente podía decirse, “Argentina es un país de italianos que habla español” o “la ciudad gallega más grande es Buenos Aires”. Sólo desde 1880 a 1914 llegaron al país más de 2.000.000 de italianos; a fines del siglo XIX, un 20% de la población de Buenos Aires era española. Argentina, especialmente en la segunda mitad de este siglo, también constituyó patria para los latinoamericanos chilenos, uru- guayos, paraguayos, brasileños y de otras nacionalidades que llegaron, con la esperanza de trabajo y paz. En 1895, se realiza el segundo censo nacional que da como resultado, sobre una población total del país de 4.044.911, la relación de 2.950.348 argentinos y 1.094.563 extranjeros, con el significativo dato para la Capital Federal, del predo- minio de un 56% de extranjeros. La influencia de ellos se mostraba en la formación de la opinión pública e hizo reflexionar a Joaquín V. González: “En cierta medi- da (tal presencia), puede ser efecto y causa del progreso verdadero. Cuando este proceso supera las bases de la resistencia de la masa originaria, esa ley de progreso se convierte en un principio de transformación, de cambio y de reemplazo de la sangre vieja por la sangre nueva; …la sabiduría de la ley consiste no en impedir que la masa extranjera se incorpore a la nación, sino fijar la posición media de su incorporación a la masa nacional para que el legado primitivo no se destruya; para que este concepto inicial de patria… no se desvanezca y la célula primitva se conserve”. En síntesis, la coexistencia de estos tipos de factores: tal tipo de naturaleza y tal calidad de hombre, en sus múltiples y re- cíprocas acciones, han diseñado el perfil más aceptable de la Argentina y de los argentinos, por lo menos, en el mo- mento actual. Este perfil no es una imagen estática e inalterable, sino por el contrario, continuo proceso de transformación que va re- ceptando las circunstancias y adaptándose a ellas. Un argentino, en abstracto, pareciera haber retenido selec- tivamente en la estructura de su personalidad, alguna de las múltiples posibles características que provienen de sus tres influencias étnicas: del inmigrante, la búsqueda de seguri- dad, ductilidad, laboriosidad e iniciativa; del criollo, volun- tad de ser, idealismo y generosidad; del indígena y mestizo, fidelidad a las raíces, resignación y astucia. Naturalmente estos factores, en un scaling del positivo al negativo, pueden transformar la búsqueda de seguridad en aburguesamiento; la voluntad de ser en simulación; la resig- nación en apatía; la astucia, en viveza criolla. En esta característica (país superpermeable a las influen- cias, tanto interiores como extranjeras), radica una de nues- tras mayores debilidades, pero al mismo tiempo, es el ger- men de la esperanza, de una posibilidad concreta de rea- lización intensiva y superior. Por otra parte, el carácter del habitante está impregnado por la fuerza que emana del paisaje –dejando en claro que siempre hablamos de generalizaciones en abstracto– no es el mismo argentino el hombre que habita en los encantado- res valles de nuestro noroeste, donde la vida tiene una pre- dominancia serena y recogida, que el argentino que habita nuestra pampa, de una extensión casi metafísica',
            'orden' => 24,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          21 => 
          array (
            'id' => 33,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000027.jpg',
            'titulo' => 'Página 27',
            'texto' => 'en una lla- nura que su habitante sabe que continúa irremediablemen- te igual, más allá del alcance de su vista. El primero, recibe el mensaje de los lugares donde más viva- mente se conserva la tradición indígena e hispánica, tierra con sabor a memorias y a leyendas, donde nacen cantos y versos que forman el aporte cultural más valioso de nues- tro folklore; en el cobijo de los valles, su expresión es dulce, cortés y respetuosa. El segundo, se vuelve introvertido y si- lencioso, parco, rudo pero noble, fuerte y profundo; dueño orgulloso y consciente de la desprotegida libertad que le brindan los rumbos solitarios. Aislados se sienten también los habitantes de Buenos Aires y con una conciencia supranacional de su desconexión con el resto del mundo; este fenómeno en el escenario de la gran ciudad con sus urgencias, competitividad y estímulos, le hace desarrollar un fuerte individualismo y la mezcla siem- pre extraña de inseguridad interior envasada en autosufi- ciencia exterior. Muchas veces, la avidez de prestigio por la apariencia (vo- luntad de ser en su versión negativa) y la carencia de arraigo a valores más vitales (degradación de fidelidad a las raíces), alimenta sentimentalismos exagerados que se expresan en una auténtica y válida creación porteña (el tango quejum- broso) o en entronizaciones circunstanciales y espúreas de ídolos transitorios y sin raíz. En forma simplista, Argentina se divide en dos países anta- gónicos: la Capital y el Interior. Por lo general, en otros países, la natural división política se distingue como Capital y Provincias o Distrito Federal y Estados; la palabra Interior lleva implícitas connotaciones tanto positivas como negativas que comienzan a dar carac- terísticas de un tipo de argentino; entre sus acepciones para Interior, el diccionario no da: “dícese de la habitación o cuarto que no tiene vista a la calle” y “que sólo se siente en el alma”. Juan de Garay en 1580, al fundar por segunda vez, lo que llegaría a ser una de las ciudades más importantes del he- misferio sur, decía: “Es necesario abrir una puerta hacia la tierra” y efectivamente, Buenos Aires, sobre el estuario del Plata, se convirtió en una inmensa entrada, pero que en lugar de dar generoso paso hacia el resto del territorio, cumpliendo el egoísta destino de gran metrópoli (síntesis de la concen- tración real del poder económico y político, potenciado por el mítico y siempre atrayente efluvio de la idea abstracta de poder, que en los viejos imperios tuvieron los monarcas y sus cortes), lo absorbió, transformando el gran ingreso en principio y fin, en sí mismo; a tal punto, que para la ima- gen mental de gran número de extranjeros, Buenos Aires y Argentina parecieran haberse convertido en sinónimos. Esta dicotomía se puntualiza claramente en las manifestacio- nes de destacadas figuras de nuestra historia, por ejemplo, en oportunidad de nuestra Organización Nacional, cuando se buscaba la unificación institucional del país: “La geografía, la historia, los pactos, vinculan a Buenos Aires con el resto de la Nación. Ni ella puede existir sin sus hermanas, ni sus her- manas sin ella”. J. J. Urquiza “Yo (Buenos Aires), soy la República del Río de la Plata… porque soy con mejor derecho quien representa la Nación Argentina”. B. Mitre O bien con motivo del proyecto de capitalización de Buenos Aires: “Buenos Aires capital es una imposición de la historia… La capital en Buenos Aires es el voto nacional porque es la voz misma de la tradi- ción, la realización bajo formas legales del rasgo más característico de nuestra historia”. N. Avellaneda “Por pretender federalizar a Buenos Aires, Rivadavia llevó al país a la tiranía; después… Buenos Aires Capital será el emporio de los hom- bres distinguidos de todas las provincias, pues aquí vendrán todos los que valgan, pero al venir privarán a sus respectivas localidades de su eficaz cooperación. Y muchos de los que vengan, al vivir del calor oficial se ',
            'orden' => 25,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          22 => 
          array (
            'id' => 34,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000028.jpg',
            'titulo' => 'Página 28',
            'texto' => 'corromperán porque no todos los espíritus tienen un alto temple. Aquí estará todo el brillo, toda la riqueza, todo el talento y después… ¿qué quedará en el resto de la República? Quedará la pobreza, la ignorancia, la oscuridad e irritantes distinciones”. L. N. Alem La variedad física de lo que genéricamente llamamos inte- rior, se traduce necesariamente en una variedad de regio- nalismos (más profunda que la división política de provin- cias y caracterizada por la toponimia, las tonadas, las cos- tumbres y las expresiones que traducen claramente idio- sincrasias diferenciadas), los que obviamente, no tienen es- tricta cohesión entre sí. Cada grupo humano desarrolla un fuerte sentido de territo- rialidad ligado a su entorno que, generalmente, prevalece sobre el sentido de argentinidad, el cual experimenta como si le hubiese sido arrebatado. Nuevamente el humor popular expresa: “Dios está en todas par- tes pero atiende en Buenos Aires” o bien, remedando la denuncia que ya en su época hiciera Sarmiento, en el sentido de que la República Argentina acababa en Arroyo del Medio, se la pun- tualiza en: “La Argentina termina en la avenida General Paz”. Si bien aún no existen planes políticos concretos para un de- sarrollo del país en su totalidad, estas tensiones comienzan tímidamente a ceder. Los regionalismos se debilitan parcial- mente en función de la incrementación de las comunicacio- nes, del turismo, y del deporte; grandes corrientes migrato- rias internas van modificando las etnias previas más cerradas y caracterizadoras y en general, el argentino ha comenzado a tener una más fuerte y auténtica conciencia territorial. En síntesis, Argentina resulta un país singularmente atípico. Si bien se genera en la esfera cultural española con los apor- tes del indígena y el mestizaje, al igual que otros pueblos del continente, la intensidad y diversidad de a inmigración re- cibida, con la consecuente y asombrosa fusión posterior de razas, constituyó como entidad nacional un país que no se ajusta estrictamente al concepto latinoamericano como tal. La mezcla de nacionalidades y razas es un proceso común y universal y se ha dado, por lo general, en muchos siglos de historia. Lo que constituye una primicia y originalidad en el proceso, estriba en la gran variedad, simultaneidad y corto tiempo en que este fenómeno de fusión étnico se produjo (y continúan produciéndose) dando como resulta- do, individuos que fundamentalmente se sienten y asumen como argentinos, sin sufrir ningún tipo de discriminación o segregación, motivadas en la extracción social, en la raza, la religión o en su credo político para el desarrollo y ejercicio de la vida social, en sus derechos, sus posibilidades de acceso a las posiciones más aceptables de la vida pública y repre- sentativa. Las naturales aspiraciones de la juventud por la aventura y el conocer mundo, las ambiciones a privilegios y prestigio de una numerosa y fuerte clase media (otra de las caracte- rísticas diferenciales argentinas), el snobismo y el esplín de una clase económicamente alta, hicieron del argentino, un prototipo clásico de “viajero al exterior” para conquistar Europa –y luego poder hablar de ello– para adquirir obje- tos novedosos, para romper, en el anonimato, esquemas de conductas impostadas a las que oficialmente suscriben. Naturalmente, por sobre estas motivaciones espúreas una gran cantidad de jóvenes y adultos de diferente condición social, viaja con afán de estudio, esperanza de perfeccio- namiento, ilusión de conocer lo diferente, con el deseo de la vivencia estética de contemplar las maravillas de la na- turaleza o las fabulosas creaciones del hombre en el arte y en la técnica, en síntesis: con el espíritu de una auténtica peregrinación. El primer tipo, es meramente un turista y caracteriza a un ar- gentino arrogante, sobrador, engolosinado en su propia y engañosa autosuficiencia; el segundo, es un viajero real y su contacto con lo diferente ha contribuido a la estructura- ción de la rapidez mental, la avidez por la actualización y un sentido global para la comprensión de los problemas y las circunstancias, que también califican al argentino. Curiosamente, el mayor descubrimiento que éste suele ha- cer en el extranjero, especialmente el último tipo de viajero (dado que en los primeros se exalta un chauvinismo',
            'orden' => 26,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:16',
            'updated_at' => '2019-01-12 19:01:16',
          ),
          23 => 
          array (
            'id' => 35,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000029.jpg',
            'titulo' => 'Página 29',
            'texto' => 'pueril e irracional), es la revalorización y meditación sobre el propio país con el necesario corolario de la añoranza. El exilio, vo- luntario u obligatorio, resulta muy duro para un argentino. Medios de comunicación masiva, lecturas, ensoñaciones es- capistas, mitologías, han conducido fácilmente a una euro- peización mental de nuestros habitantes; una admiración desmedida por lo extranjero, lo civilizado, sin discernimien- to preciso entre lo auténtico y lo mediocre (“es importado, es bueno”, parece ser un axioma irrebatible). Nuestra inseguridad o temor al ridículo nos lleva, con bastante frecuencia, a no prestar atención o alentar expresio- nes nativas verdaderamente originales. Sin embargo, cuan- do (por razones reales o inauténticas), alguien obtiene un reconocimiento público europeo, por la misma manifesta- ción que ignoráramos, comienza a actuar el mandato de la ciega veneración hacia lo exterior, y el individuo puede re- gresar, seguro de encontrar puertas abiertas hacia todas las posibilidades. Ya desde el año 1846, Esteban Echeverría criticaba la frivo- lidad con que miramos las cosas “porque rara vez nos deja- mos impresionar por ellas, de modo que se graben de un modo indeleble en la memoria”, olvidando así, su origen y valor en la estructura de nuestra identidad. “Así se explica como, desde el principio de la Revolución, andamos como mulas de atahona girando en un círculo vicioso y nunca salimos del atolladero.  “Así hemos gastado nuestra energía en ensayos de todo género para volver a ensayar todo lo olvidado.  “Así nunca salimos de Cristo (*) en materia alguna, porque nunca atesoramos lo aprendido. “Otras causas además, obstan y dañan mucho nuestra educabilidad: Una, es esa candorosa y febril impaciencia con que nos imagina- mos llegar como de un salto y sin trabajo ni rodeos al fin que nos proponemos; otra, la versatilidad de nuevo carácter que nos lleva siempre a buscar lo nuevo y extasiarnos en su admiración, olvidan- do lo conocido. “La Europa, sin querer, fomenta y extravía a menudo esta últi- ma disposición, excelente para la educabilidad cuando es bien dirigida… Contribuye muchas veces a que no se tome arrai- go la buena semilla y a la confusión de ideas; porque hace vacilar la fe en las verdades reconocidas, inocula la duda y mantiene en estéril y perpetua agitación a los espíritus inquietos”. Estos pensamientos escritos hace 140 años, mantienen una asombrosa vigencia en el presente. Nos concentra en el pun- to de absorber con inteligencia los aportes externos, pero sin dejarnos alucinar, en tal medida que olvidemos nuestra propia identidad. Esta es aún producto en formación y no podrá lograrse sin reales esfuerzos y especialmente, con un proceso de introspección que descubra la bifurcación en donde perdimos la ruta, que atesore nuestros aciertos y nos haga recapacitar en los errores cometidos, no para llorar por ellos, sino para aprender de ellos, integrados a la cultura universal, para no caer en un nacionalismo aberrante, pero tamizados por un sentimiento de amor y revalorización de nuestras propias raíces. Tal disposición de ánimo permitirá revertir situaciones con- tradictorias e inexplicables, como la falta de una política in- tegral consistente y continua para el desarrollo de distintos polos de riqueza potencial, que permiten afianzar el progre- so, resolver la dicotomía cultural y la calidad de vida de los argentinos. Continúa aún la fisonomía del desierto. El país es todavía una suerte de mundo periférico a los nudos de radicación concentrada. Amplias zonas de pasiva continuidad, versus limitados focos de dinamismo gradualmente crecientes. No es ajena a esta situación la discontinuidad de la estruc- tura política. En el lapso 1930/1983 (durante el cual hubie- sen correspondido 9 presidentes constitucionales) el país tuvo 28 administradores diferentes. La psiquis del habi- tante ha enfermado: esperanzas y frustraciones reiteradas condujeron al descreimiento y escepticismo, al aislamiento, al desarraigo. (*) Estar muy en los principios de algún arte o una ciencia.',
            'orden' => 27,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          24 => 
          array (
            'id' => 36,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000030.jpg',
            'titulo' => 'Página 30',
            'texto' => 'Estos pensamientos escritos hace 140 años, mantienen una asombrosa vigencia en el presente. Nos concentra en el pun- to de absorber con inteligencia los aportes externos, pero sin dejarnos alucinar, en tal medida que olvidemos nuestra propia identidad. Esta es aún producto en formación y no podrá lograrse sin reales esfuerzos y especialmente, con un proceso de introspección que descubra la bifurcación en donde perdimos la ruta, que atesore nuestros aciertos y nos haga recapacitar en los errores cometidos, no para llorar por ellos, sino para aprender de ellos, integrados a la cultura universal, para no caer en un nacionalismo aberrante, pero tamizados por un sentimiento de amor y revalorización de nuestras propias raíces. Tal disposición de ánimo permitirá revertir situaciones con- tradictorias e inexplicables, como la falta de una política in- tegral consistente y continua para el desarrollo de distintos polos de riqueza potencial, que permiten afianzar el progre- so, resolver la dicotomía cultural y la calidad de vida de los argentinos. Continúa aún la fisonomía del desierto. El país es todavía una suerte de mundo periférico a los nudos de radicación concentrada. Amplias zonas de pasiva continuidad, versus limitados focos de dinamismo gradualmente crecientes. No es ajena a esta situación la discontinuidad de la estruc- tura política. En el lapso 1930/1983 (durante el cual hubie- sen correspondido 9 presidentes constitucionales) el país tuvo 28 administradores diferentes. La psiquis del habi- tante ha enfermado: esperanzas y frustraciones reiteradas condujeron al descreimiento y escepticismo, al aislamiento, al desarraigo. Impacientes, críticos e intolerantes, siempre en busca de la rá- pida aparición del hombre providencial, el país navegó, salvo breves excepciones, entre populismos seducidos por dema- gogos y estructuras autoritarias, arbitrarias y represivas. Estas observaciones no son pesimistas, sino el enunciado de síntomas que nos impulsan a un análisis objetivo y des- prejuiciado para recobrar los ideales y los principios de ge- nerosidad, humanidad y cultura, que históricamente nos constituyeron. Mirar por sobre la tormenta; Sarmiento, en momentos de terri- bles crisis y anarquía, seguía realizando y proyectando para la Argentina que intuía y esperaba (quien no alcanza a ver el hori- zonte es porque observa desde el fondo de un pozo). Todo país que no tiene fe en su identidad, equivoca el rum- bo; nuestro pueblo –aún en forma inconsciente– busca esa conquista que le permita sobreponerse a sus propias dificul- tades; ese fervor aglutinante, esa unidad que hermana, se alimenta y potencia en el vigor que surge de la conciencia de actuar con propósitos compartidos. La Plaza de Mayo, tradicional escenario de manifestaciones po- pulares, ejemplifica esta situación: con diferencia de una sema- na, aloja una muchedumbre airada y amenazante, que protesta por requerimientos económicos, o se exalta dispuesta al sacrificio, ante el mismo mandatario de la semana anterior que (en forma válida o no), toca el botón de la nacionalidad y la soberanía. Tensión es un continuo factor en la vida íntima del argentino ac- tual: negativo, en principio, pero sumamente positivo por el di- namismo que genera. Si sabemos utilizarlo, podemos redefinir el problema, actualizando nuestra imagen y conducta, a fin de ha- cerla acordar con nuestra realidad presente y comprendiendo el pasado con el sentido de evitar la reiteración de sus errores, pero sin destruirlo en su totalidad. Existe una urgencia por reforzar el concepto territorial y hu- mano. De igual manera, existe la necesidad de un persistente, sacrificado y duro esfuerzo para dejar un camino apto para que la nueva generación pueda llegar, a través de sus senti- mientos y acciones, a configurar el concepto integrado de pa- tria argentina.  El futuro es hoy',
            'orden' => 28,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          25 => 
          array (
            'id' => 37,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/es/00000031.jpg',
            'titulo' => 'Página 31',
            'texto' => '“Campo Sembrado”, acrílico sobre tela  de Claudio Riquelme (1933),  Zurbarán Gale- ría de Arte.  Región Pampeana “Viejo Mercado del Norte”, gouache de Leonie Matthis (1883/ 1952),  Zurbarán Gale- ría de Arte.  Región Serrano-Andina “El Palmar”, Colón, Entre Ríos, óleo sobre tela de Fernando Ro- mero Carranza (1935), Zurbarán Galería de Arte.  Región Chaqueña “Serenidad en el Lago”, óleo de Angel Domingo Vena (1888/ 1983),  Zurbarán Gale- ría de Arte.  Región Patagónica',
            'orden' => 29,
            'idioma_id' => 1,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
        ),
        'en' => 
        array (
          0 => 
          array (
            'id' => 39,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000006.jpg',
            'titulo' => 'Page 6',
            'texto' => 'NOTE This series is an attempt to provide you with a better under- standing of the reality of our country, Argentina, through language and images. Language, that fabulous achievement of humankind, which went on creating phonemes, not only generating a code for exchanging information in the course of daily life but, in written form, making it possible to com- municate experiences and gradually record the history of thoughts and events. Images are also one of the ancestral necessities of the species; from prehistoric pictographs to contemporary holograms, images have magically captured the “hyper-reality” of the visible world. These two ancient means of communication will serve to present Argentina and its people to its friends at home and abroad. The series consists of an introduction and five books, each of which is composed of two volumes. The first volume of each book attempts to discuss and tie together as many related geographical, historical, and cultural factors as possible by means of a nonchronological text or account and abundant graphical material. The second volume consists of an orga- nized collection of short pieces by distinguished experts in subjects that pertain to the region, thus providing greater depth in each of these fields.
Each book covers a particular region of the country, which does not necessarily coincide with the country’s political di- vision into provinces.

Book One:	Introduction to Argentina Book Two:	Summa Patagónica, Antarctica
and Southern Islands Book Three:	Summa Andina
Book Four:	Summa Chaqueña Book Five:	Summa Pampeana Book Six:	Summa Metropolitana
Each region is discussed in terms of the interaction between Nature and Humankind, which results in the historical and cultural process presented for your consideration.',
            'orden' => 4,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          1 => 
          array (
            'id' => 40,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000007.jpg',
            'titulo' => 'Page 7',
            'texto' => 'Alejandro Angel Bulgheroni Botto (oil painting by Roberto Tomasello). The foundation that bears his name publishes this series – Argenti- na, Image of a Country – in his memory.',
            'orden' => 5,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          2 => 
          array (
            'id' => 41,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000008.jpg',
            'titulo' => 'Page 8',
            'texto' => 'Map by Diego Homen (1558 British Museum); as early as 1554, Homen had referred to present- day Argentina as Terra Argentia.',
            'orden' => 6,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          3 => 
          array (
            'id' => 42,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000009.jpg',
            'titulo' => 'Page 9',
            'texto' => 'PREFACE     Writing a book about Argentina immediately elicits two questions: “Why?” and “For whom?” Visitors to our cities (particularly to Buenos Aires, which is invariably visited at some time by all Argentines and travel- ers to this country) notice a profusion of books on Argentina in the display windows of each city’s excellent bookstores, including beautiful books of art and photography, travel guides, descriptions of our natural environment, serious studies by thinkers on Argentine history or current events, scientific and philosophical texts, and literary works from the minds of our writers and poets. All in all, and each on its own, these books reveal a true and profound part of Argentina. However, the only way to interweave the numerous threads of each study, enabling them to emerge from their individual contexts and combine in such a way that they begin to form an all- embracing picture of Argentina, is to take in a broad, multifaceted range of information and to live consciously in the country for an extended period of time. Of course, this book does not attempt to carry out such an encyclopedic synthesis, but it does attempt to present cer- tain objective elements that might encourage further re- flection on the topic. I used the word all-embracing without reference to a struc- tured, concrete, and permanent entity, because there is no one Argentina. Many different, contrasting, and even con- flicting Argentinas exist simultaneously; nevertheless, we come to sense that they are all mysteriously connected, as aspects or parts of the same unit. This unity of the whole, as with the genetics of living organ- isms, comes before the parts, which do not exist or have meaning outside of the whole. The particular way in which these parts are integrated is what lends the whole the (inherent) quality that ultimately determines what is unique and structurally original about it. In accordance with the concept of Gestalt, a country is more than just the sum of its parts. Just as in nature we can look at a leaf and recognize it as an oak leaf (even though another leaf exactly like it does not exist on the tree or in the entire grove), and just as we can recognize an artist’s personality',
            'orden' => 7,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          4 => 
          array (
            'id' => 43,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000010.jpg',
            'titulo' => 'Page 10',
            'texto' => 'in his or her paintings (despite variations in subjects and colors), the image of a country can be understood as a resulting unity, enriched by the infinite variety of its partial realities. In a word, it takes the form of a living organism in which life is manifested by the transcendence and multiplication of its vital qualities. Initially, all countries arise from a region, an area with par- ticular natural characteristics and a group of people which, exercising its primordial territorial instinct, gradually takes possession of the physical space, traveling across it, defining it, using it, and changing it in accordance with the group’s needs, until finally the group becomes a part of the land it- self, creating the ties and emotions that make up the notion of nationhood. These necessary activities are the result of a natural code of behavior whose primary aim is to ensure the survival of the group over that of the individual, and to give continu- ity and coherence to the emerging community. The natural code is later transformed into explicit rules, a type of ethical system that will shape the particular character of the group over time. Meanwhile, the group develops its ability to take in its en- vironment and other members of the community using its senses and psychological powers of interpretation and eval- uation, creating its initial aesthetic with respect to life and expression. From this perspective, we can conceive of the birth of a country as a conjunction of ethical and aesthetic activities on the part of a community situated in a particular territory. Consequently, in order for us to be able to grasp the true es- sence of this entity, our own minds must perform an ethical and aesthetic activity aimed at meditating on, understand- ing, and identifying with this entity, a process which, in the words of Worringer, constitutes the complete act of aesthet- ic contemplation. We can conceive of (A) the physical world (natural or man- made) as the landmarks, monuments, or scenes of (B) the ongoing associations of humankind (individual or collective), which are interwoven over the course of time by memory, thereby creating a continual consciousness. In other words, there is a mutual interaction between hu- mankind and nature, producing further interactions ex- pressed in the form of behavior or designs that are always directed toward achieving perfect harmony. This process underlies the etymology of the Latin words mens, memori- ae, memento, and monumentum (mind, memory, reminder, and monument).',
            'orden' => 8,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          5 => 
          array (
            'id' => 44,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000011.jpg',
            'titulo' => 'Page 11',
            'texto' => 'Monument is to be understood in a broader sense than its literal physical or cultural meaning; it is the material em- bodiment of the living function known as memory in all di- mensions of time – memory of the past, consciousness of the present, and foresight into the future: re- sentiment	sentiment	pre- sentiment (experience)	(action)	(projection) These physical creations constitute landmarks on which hu- mankind has stamped the sign of its passage. Through these landmarks, we learn to recreate the history that, according to Leibnitz, is the memory of the species, from which we are to glean the lessons of the moments that formed the present and will determine the future. In addition, we are led to reconsider the final outcome of the behavior and activities of a human group as it interacted with the physical environ- ment in which it established itself and in which – motivated by the demands of its particular character – it expressed it- self through the physical and spiritual creations we term cul- ture. Culture here should not be understood as a finished prod- uct, but rather as the result of a subtle process capable of continual change in response to changes in the conventions of the group (social, economic, religious, etc.). In sum, the response to the question posed earlier is that this series is an attempt to present a country seeking its politi- cal identity, economic development, quality of life, and life- style in the kaleidoscope of the many Argentinas that exist side by side in its various regions and the capital – a country manifested as something akin to the crystallized sedimenta- tion of its history, aspirations, and achievements. The series is also an attempt in that it seeks to understand a people of Spanish origin, with limited but diverse indig- enous elements, that gradually formed its own particular character by weaving together a single tapestry made up of the contributions of each of the peoples that arrived and settled in the country, a country geographically isolated in the south, which made it set its sights beyond the sea and which has generated problems for its inclusion in the Latin America setting. I have sought to reveal the character of the country through documents and images that capture moments in time, the circumstantial events that shape us, and the natural envi- ronment or permanent reality that provides us with shelter and sustenance: time and space, where our identity is cre- ated by history and geography. I have also sought to objec- tively demystify outdated clichés – which are useful only in tourism – and to honor the past through testimonials as a point of departure for the future.',
            'orden' => 9,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          6 => 
          array (
            'id' => 45,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000012.jpg',
            'titulo' => 'Page 12',
            'texto' => 'This leads us to clarify and reconsider opinions that we think are true, but which are actually just possibilities that must be tried out and developed before they can be accepted as reality. In this way, we can view the country and the people of Argentina as the interaction between the abstract concept they create and reality itself, which enables us to contem- plate the dynamism or tension of our own times in the hope of bringing the true essence of the abstract concept within our grasp. Finally, this series is an attempt to answer the questions: “What is Argentina today?” and “What are its permanent characteristics?” In seeking the answers, we Argentines and those interested in Argentina are afforded an opportunity to arrive at a more profound understanding of ourselves; and, in considering objective information that serves as a stimulus to thought, each reader will reach his or her own conclusions. Was it not Paul Valéry who said: “Could it be that God left the world unfinished so that men might finish it?”',
            'orden' => 10,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          7 => 
          array (
            'id' => 46,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000013.jpg',
            'titulo' => 'Page 13',
            'texto' => '',
            'orden' => 11,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          8 => 
          array (
            'id' => 47,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000014.jpg',
            'titulo' => 'Page 14',
            'texto' => 'The image of reality contains suggestive connotations, as  can be seen in these photographs by Osvaldo Pons, taken only a few millimeters from shattered glass and in these by Hugo Corbella, taken from high altitude while flying over the icy expanse of the Viedma glacier.',
            'orden' => 12,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          9 => 
          array (
            'id' => 48,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000015.jpg',
            'titulo' => 'Page 15',
            'texto' => '“For me, the country is a constant presence, a mood, soft music, the prick of frustration, a rapture of joy, an enthusiasm that fades, a per- plexity, a hope that never dies. In other words, for me the country pos- sesses all of the silent, blurred, and mysterious characteristics of an emotion.” These sentiments of Victor Massuh are also a valid way to define Argentina, even though they refer to the sense of homeland that everyone on the planet experiences when thinking about their birthplace. This Argentine poet also wrote: Oneʼs homeland is the land where one was born Oneʼs homeland is the land where one has dreamed Oneʼs homeland is the land where one has fought Oneʼs homeland is the land where one has suffered. The emphasis is always on this deep current in the human psyche. The current is sometimes painful, sometimes re- sentful, but always charged with nostalgia and a certain irrational affectivity similar to that associated with our childhoods: the paternal home, where we encounter the miraculous comprehension of moments of conflict and yearning – heightened by the passage of time – for pleas- ant situations. It matters not whether we were born in a forgotten vil- lage or a great city, whether its streets were dusty or paved, whether we were surrounded by the smells and sounds of trees or of a nearby factory, whether the horizon was blocked by tall buildings or open to the countryside; all of these places send messages to our senses. It matters not which house, comfortable or common, pro- vided us with shelter in our younger days; the dark cor- ners in which we kept our wondrous treasures or the places where we preferred to be, where we began to get to know ourselves, will always be there. It matters not how important or obscure, virtuous or dis- solute the companions of our childhood and adolescent years have become, or who happened to enter our particu- lar lives; the presence of these people is the means',
            'orden' => 13,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          10 => 
          array (
            'id' => 49,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000016.jpg',
            'titulo' => 'Page 16',
            'texto' => 'through which we reach beyond the natural and concrete bounds of the family, taking our first steps toward our fellow human beings: the society that began to shape us. Just as the strong bonds of a true friendship are formed by sharing places and experiences, the ongoing and subtle in- teraction of these forces on a social scale is what connects us to the notion of country created by place and time, by the environment and history of its inhabitants. How can Argentina be defined? How can it be described? The landmarks or essential characteristics of Argentina can- not be limited to structured symbols, repeated historical cli- chés, and personal associations that are valid only in part (consistent with the particular experiences of inhabitants or visitors). Instead, they must be derived from images or memories imbued with national emotion that involve a pan- oply of places, personages, art, legends, and events, even turbulent times of crisis or depression that, in spite of the fact that we have overcome them and wish to forget them, have also contributed to the formation of an authentic na- tional idea. There is no one symbol; not even the most highly regarded and revered of them can express the essence of Argentina and argentinidad, or “Argentineness,” by itself. Indeed, few symbols remain the same over time. The histori- cal examples are variable in nature, and their values have changed in conjunction with changing times and circum- stances. Successive generations have interpreted these sym- bols differently while creating new historical landmarks with extraordinary evocative power for their own times as well. In contrast, a country’s essential characteristics derive from complex and intangible characteristics that appeal to both the emotions and the mind and are fused together through an internal process, giving rise to true and timeless symbols that have more to do with empathy, the recognition of fam- ily and mutual belonging, than with a particular object or image.  One Argentina is physical: enormous, multifaceted, full of contrasts, and unknown; another is seen in the context. 

National Insignia of the 1813 stamp on the certificate of
citizenship of General
Arenales.

“…The nations of the world, children of war, raise their insignias to announce themselves to other peoples, wolves and birds of prey, lions, griffons, and leopards. But the symbol emblazoned on our escutcheon is neither a fabulous hippogriff nor a unicorn, neither a two-headed bird nor a rampant lion poised to devour the world. The sun of civili- zation, dawning to bring forth new life; freedom, with Phrygian cap help aloft by brotherly hands, as the aim and purpose of our life; an olive for men of goodwill, laurels for the noble virtues….
“Our symbol, as a Nation recognized by all of the peoples of the earth, now and forever, is this Flag, in days when our',
            'orden' => 14,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          11 => 
          array (
            'id' => 50,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000017.jpg',
            'titulo' => 'Page 17',
            'texto' => 'forces crossed the Andes with San Martín, in days when they plied the seas with Brown, and, finally, in the days of tranquility that it “Many republics see this Flag as a deliverer, as a helper, and as a guide on the difficult road to emancipation. Some of these republics were founded in its shadow; others blossomed from the shreds that were torn from it in battle. Not a single territory, however, was added to its dominion; not a single people was absorbed in its broad folds; not a bit of recompense was demanded for the great sacrifices it imposed on us “This Flag has already fulfilled the promise expressed by our bla- zon…. “Its blue and white stripes symbolize the sovereignty of the kings of Spain over the dominions – not of Spain – but of the Empire, which extended from Flanders to Naples to the Indies. To this royal banner, our forefathers added a rosette-shaped crest on May 25, demonstrat- ing that from the breast of a captive king, we were claiming our sover- eignty as a nation, a nation that was no longer subject to the Council of Castille nor, from that time forward, to the Council of the Indies. “General Belgrano was the first to fly the royal banner, and thus, by our own hands, we crowned ourselves rulers of this land and entered Argentina into the great book of nations chronicling the fulfillment of the destiny of our race. Now, with this ceremony, we erect a statue in the center of the Plaza of the Revolution of May in honor of General Belgrano, the standard-bearer of the Republic of Argentina….”  Discourse on the Flag by Domingo Faustino Sarmiento on September 24, 1873, during the inauguration of the statue of Belgrano, which was sculpted in the workshop of the French sculp- tor, Carier-Belleu- se; the Argentine sculptor Manuel de Santa Coloma, who fashioned the horse, also worked there',
            'orden' => 15,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          12 => 
          array (
            'id' => 51,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000018.jpg',
            'titulo' => 'Page 18',
            'texto' => 'of specific circumstances and daily life, an Argentina that often finds its way into anecdotes in which we express pessimism and a feeling that everything is headed in the wrong direc- tion – prospects for development, culture, politics. Commonplace sayings such as “In this country, nothing can be done,” which drive large numbers of our young people to go “anywhere, to do anything,” represent a tendency toward de- pressive exaggeration, an escapist excuse that leads one to turn one’s back on action and to transfer individual respon- sibilities to an abstract entity that is supposed to save us from the work we should be doing ourselves: society, the government, the country. Often, however – due to this lack of a sense of moderation, this tendency to alternate between praising and denounc- ing without the calmness needed for deliberation that char- acterizes the sweeping judgements of Argentines – an ideal or abstract Argentina is also created, triumphalist, superior, and unique, which, although valid to some degree, is not in perfect accord with the country’s territorial and historical reality. What then is the real Argentina? The only possible answer is the one given by the interaction of three images, which – like the old stereoscopic slide viewers that superimposed images on the retina – provide us with a three-dimensional impression of the object that was photographed. NATURE The natural environment – the earth, the climate, the landscape – is one the basic structural factors that determine the character of the country in which we live, a country whose Constitution, from the first days of our independence, generously offered land to anyone of good will who wished to settle on Argentine soil. The Republic of Argentina is a vast re- gion that once formed part of the vice- royalty of River Plate, created in 1776 by Charles III of Spain. Over the course of history, other portions of the viceroyal- ty became parts of Chile and Brazil, and formed independent countries such as Bolivia, Paraguay, and Uruguay. This elongated strip resembles a huge pen- insula where South America approaches Antarctica, bathed on either side by the world’s two largest oceans and extend- ing into an Atlantic archipelago and an Atlantic region. Its geographical location has made Argentina a final destination. It is not a country that is crossed on the way to somewhere else, and so visitors to the country must have a specific desire to be- come acquainted with us. Argentina extends approximately 3,700 kilometers from north to south, and its variable breadth from east to west reach- es a maximum of 1,400 km in the north. The country contains every type of cli- mate and topography imaginable. In this area of approximately 4,000,000 square kilometers, the four seasons exist side by side; the jungle and the desert, the moun- tains and the plains, the sea, and a huge variety of animals, plants, and minerals.',
            'orden' => 16,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          13 => 
          array (
            'id' => 52,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000019.jpg',
            'titulo' => 'Page 19',
            'texto' => 'Political situation with regard to Antarctica, showing the areas that have been claimed by various countries, particularly Great Britain, which, as part of its so-called “Dependency of the Falklands,” annexed the Antarctic zone and islands with respect to which Argentina has undeniable rights. Argentina and Chile, located approximately 1170 km from the polar circle, are the closest countries to the White Continent, with New Zealand, at 2170 km, in second place.',
            'orden' => 17,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          14 => 
          array (
            'id' => 53,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000020.jpg',
            'titulo' => 'Page 20',
            'texto' => 'The west is dominated by the Andes, a gigantic range of snow-covered mountains that rises to the highest peak of the Western Hemisphere, Aconcagua, and descends toward the east in the form of low-lying foothills. Depending on the latitude, the Andes include stony ravines with dazzling rock formations and colors; enclosed subtropical jungles re- plete with flowers, orchids, and ferns; dry, eroded canyons that form a sculpted landscape full of mythic allusions; cold beech, Chile pine, and myrtle forests; huge national parks with strikingly beautiful waterfalls, lakes, and glaciers; and rich, fertile valleys that stretch along the banks of rivers flowing down to the sea or are irrigated by the major pub- lic works that human engineering has created in an effort to tame the rushing waters. The tropical northeast and the region known as Mesopotamia are characterized by a network of fast-flowing rivers that con- verge toward the Río de la Plata (River Plate) and the largest estuary in the world. The largest of these rivers is the majes- tic Paraná (which stretches some 4,700 kilometers), which un- dulates darkly and heavily downstream like the gigantic lam- palagua of legend, a mythical snake said to drink rivers dry. This intricate web of rivers, filled with lush, extravagant vege- tation, traces fertile estuaries and marshes that are the site of cotton fields, rice paddies, and tea plantations, maté cultiva- tion, tung trees, tobacco and fruit, mills that process valuable woods, groves of palm trees, and the spectacular waterfalls of the Iguazú river, which has more than 270 cascades that drop from a height of nearly 100 meters and is one of the world’s greatest natural wonders. The dense waters are like melted stones that transform themselves into multicolored clouds of vapor so that countless birds might dance among them; green stones on red earth, which, when approached, explode into lizards like arrows shooting in all directions; the cobalt-blue sky and the clear disk of the sun that fades cloud- lessly, giving way to a dark night filled with sounds and phan- tasms that transport us to the realm of enchantment and mystery described by Horacio Quiroga.',
            'orden' => 18,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          15 => 
          array (
            'id' => 54,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000021.jpg',
            'titulo' => 'Page 21',
            'texto' => 'The central region or the Pampas is, in the words of Borges, an entity that cannot be captured in an image, but only in a series of mental processes. Its infinite nature can be understood only through the mem- ories created by continuous and repeated experience. It is a sprawling and monotonous plain of ungraspable dimensions counterposed by an immense, open sky, an overwhelming, al- most physical presence. The region includes the fertile plains that gave rise to Argentina’s rich agricultural and livestock industry, and as it approaches its boundaries, it gradually changes to salt flats, depressions, and rough grazing lands, ultimately reaching the very edge of the dry desert. As F. Bond Head wrote in notes he happened to jot down while crossing the Pampas (London, 1826): “The Pampas is so tremendously broad that in the north it is bordered by forests of palm trees, and in the south by the eternal snows.” Patagonia, Antarctica, and the southern islands constitute another extremely diverse region. Nevertheless, these ar- eas share the common characteristic of making up the far south, even for the Argentines themselves, in a sense that goes beyond mere geography. The coast that varies from wide, peaceful beaches to steep cliffs; drowned valleys and fjords that amazed the first European seafarers; the virtually unknown lands of Antarctica; quiet, windy islands; desert plateaus; petrified forests, witnesses to the passage of a thousand years; great grasslands, home to innumerable herds of sheep; and the mountain forests, glaciers, and lakes – all an expression of nature’s sheer power and, above all, its scale – imbue the landscape with a beauty that only silence can describe. It is appropriate to recall these words of Humboldt: “Just like the oceans, the steppes overwhelm the spirit with a sense of the in- finite.” The mineral deposits – petroleum, iron, and coal – and the riches of the region’s seas, plains, and forests constitute a true challenge to the initiative of humankind.',
            'orden' => 19,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          16 => 
          array (
            'id' => 55,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000022.jpg',
            'titulo' => 'Page 22',
            'texto' => 'And with humankind, we arrive at the second structural fac- tor: the inhabitants. The population density of the region is only about seven people per square kilometer. In Facundo, Sarmiento wrote: “The vast expanse of the country…is completely unpopulated, and there are navigable rivers that have yet to be plied by even small, fragile boats. This vastness is the illness from which Argentina suffers: it is surrounded by deserts on all sides, which reach into its core; solitude and the absence of a single human habitation are, by and large, the unquestionable boundaries between one province and another. “Vastness is everywhere: vast plains, vast forests, vast rivers, the ever unclear horizon, always melting into the land between clouds and in- substantial vapors, which prevent one from making out the distant point where the earth ends and the sky begins.” This vastness and solitude, which might sound rather strange to the ears of the cosmopolitan inhabitants of Greater Buenos Aires, is not intrinsically bad; on the con- trary, it is an incredible gift of nature, waiting expec- tantly for humankind to possess it, intermingle with it, and command it to produce, directing and caring for its development in the interest of mutual benefit. Mother earth and father humankind, creating the fruit of free - will and love. HUMANKIND A sharp and humorous criticism frequently heard among the employees of large international organizations: “A great busi- ness: buy Argentines for what theyʼre worth and sell them for what they think theyʼre worth.” Who are the Argentines? Is there such a thing as a proto- typical Argentine? Generally speaking, our ethnicity derives from three sources: (a) Native peoples and mestizaje, the intermingling of races and cultures; (b) The locally born offspring of Spanish immigrants, or cri- ollos; and (c) The varied and innumerable contributions of immigra- tion. (a) When the Spanish arrived in the Americas, they encoun- tered lands that were populated by a variety of different races and cultures. The land that is now the Republic of Argentina was no exception. Although it is true that an amazing civilization similar to that of the Aztecs, Mayans, or Incas did not exist here, the original inhabitants of our country formed numerous di- verse and interesting cultures. Since the Paleolithic era, according to Salvador Canals Frau, the indigenous population of Argentina has included six dif- ferent racial groups: the Huárpidos, Láguidos, Patagónidos, Brasílidos, Andinos, and Fueguinos, comprising more than 22 distinct peoples or tribes. Without entering into a consideration of other theories (Ameghino and others) concerning the origins of human- kind in the Americas, radiocarbon dating has made it pos- sible to reliably date stratigraphic layers containing the re- mains of prehistoric cultures, including layers – particularly in Patagonia – that date to at least ten thousand years be- fore Christ, as well as pictographs that match the represen- tational techniques and codes used in Europe and Australia during the late Paleolithic era. One myth that the majority of today’s Argentines need to leave behind is the idea that our roots have no indigenous elements. Although it is true that these indigenous peoples did not have a strong and enduring form of organization and that their annihilation as pure races was a gradual process (which began during the Spanish conquest, intensified during the era of independence, and continued until recent times, leav- ing only a few isolated groups subject to an unstoppable process of transculturation), it is also true that their ethnic and cultural presence, more or less pure in mestizaje, sub- sists in large segments of the Argentine population. The continued existence of indigenous cultures goes beyond communities living on reservations or anthropological studies, and it is also closer to hand. These cultures are manifested in the daily lives, personalities, and customs',
            'orden' => 20,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          17 => 
          array (
            'id' => 56,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000023.jpg',
            'titulo' => 'Page 23',
            'texto' => 'of a large number of people from various parts of the country, people whom we usu- ally refer to as criollos as a consequence of our stubborn refusal to officially recognize mestizaje. The influence of indigenous cultures is reflected in foods and handicrafts, beliefs and super- stitions, values and mores, and, above all, in that wonderful cre- ation of humankind, which, to the extent allowed, reveals all of our particular characteristics and history: language. Our everyday speech is filled with expressions and phrases that came from these cultures. The word chacarero, for ex- ample, which is virtually synonymous with gringo, derives from Quechua and refers back to the chacra, where indige- nous peoples practiced agriculture and grew fruit before the discovery of the New World and during the colonial period. Even the Argentine word che, which distinguishes us throughout Latin America, is of indigenous origin. Finally, the large numbers of Africans who were sold by English slave traders in Retiro with the authorization of the King of Spain did not just disappear either: sambos and mu- lattos, people with varying degrees of African, Amerindian, and European ancestries continue to be part of our popula- tion. (b) The second source of our ethnicity is the criollo: I am a Criollo from Mexico, which is the name The Indies give to those born in her embrace.” (Tirso de Molina) As we can see, children of European descent who were born in the colonies – and particularly, in the case of our legend- ary Americas, children of Spanish descent – began to acquire a specific name. The terrestrial influence of the New World gradually changed the distinctive characteristics of the Spaniards who were born here. Early on (sometime between 1571-1574), the geographer López de Velazco noted “differences in the color and size” of cri- ollos. In Asunción in 1579, Montalvo pointed out “an attitude of disrespect on the part of criollos and mestizos toward their parents and elders,” while Juan de Cárdenas, in comparing criollos and European Spaniards in his Wondrous Problems and Mysteries of the Indies, considered criollos to be “more po- lite, modest, and refined.” Criollo is derived from the same root as the Spanish words crear (create) and criar (raise), meaning: “to give being that which previously did not exist, or to instruct or establish new values.” These new values, which slowly began to manifest them- selves in the inhabitants of our land, continued to develop through critical examination (throughout history and con- tinuing up to the present day) and an openness to new ways of thinking. Just as the character of Spain during the era of conquest alternated between the Medieval world and the modern thinking of the Renaissance, the emerging race of criollos combined traditional principles with elements of modern thought. This had the effect of reinforcing loyalty toward traditional principles while simultaneously modify- ing them based on one’s connection to present reality and enriching them with new universal principles. Despite our narrow focus, it is worth noting the significant observations of two well-known English personalities con- cerning the Hispanic peoples of the Colonies, where the crio- llos had already developed their (second) character: “I cannot fail to praise the patient virtue of the Spanish. Rarely or never has there been a nation that has suffered so many disasters and misfortunes…nev- ertheless persisting in their enterprise with steadfast constancy….Storms, shipwrecks, hunger, defeats, uprisings, burning sun, cold, pestilence, ex- treme poverty….[These] noble discoverers toiled for many years, losing fortune and life” (Sir Walter Raleigh, Seventeenth Century). “The sole inhabitants of the vast plains of Buenos Aires are Christian savages known as ‘huachosʼ (gauchos), whose principal furniture consists of the skulls of horses, who live on raw meat and water, and whose favorite pastime is to ruin horses in forced races. Unfortunately, they prefer their national independence to our cotton and muslin” (Sir Walter Scott, Nineteenth Century). ',
            'orden' => 21,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          18 => 
          array (
            'id' => 57,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000024.jpg',
            'titulo' => 'Page 24',
            'texto' => 'In summary, leaving aside the European ancestry of our people, which has been both praised and reviled, the criollo is to be seen as the result of a biological, cultural, and social process that has taken shape within the context of Western Civilization. In the words of Silvestre Byrón: “To the exasperation of scientific understanding, which has already been the subject of ridicule in its at- tempts to explain the origin of the continentʼs indigenous population, a variety of traits and new civilizations have come together to form – through the blood and the seed of mestizaje – a new, multifaceted, and unique ethnic group. The ordering principle of this ethnic group is based on neither racial purity nor superior pedigree, but on its intrinsic freshness and its promise for the future.” (c) Finally, we arrive at the third element that has con- tributed to rounding out and diversifying the character of the Argentines: the immigrant. The oft-repeated joke “the Argentines descend from the boat” has implications be- yond humor. The exceptional Juan B. Alberdi’s statement that “to govern is to populate” is an absolute truth in our country, which has al- ways been open to people from anywhere in the world and, since 1811, has offered safety and assistance to those who wished to live here. Immigration has brought Argentina people of other faiths, arti- sans, agricultural workers, skilled businesspeople, people of cul- ture and science, and even poor, uneducated people, equipped with only their tenacity and determination, a willingness to adapt, and an unconquerable desire to engage in useful work that would enable them to rise above the misery they left behind. With the support of our tolerant laws, which allowed them not only to profess their faith but also to disseminate it, as well as the possibility of economic gain (“come be a part of America”), im- migrants to Argentina gradually put down roots and provided the country with a distinct set of features – diverse, cultured, industrious, occasionally unstable and contentious – which speeded up Argentina’s entry into the modern world. Juan Vigo said: “The most important contribution of these im- migrants…was their way of thinking, their vitality, and their hab- its, which were formed in a highly capitalistic European society and which gave them a clear and decisive advantage over criollos in terms of getting ahead, becoming rich, and accumulating land.” In order to understand what life was like in those days for most European peasants and urban laborers (who constitut “India” (Indian Woman) (detail), watercolor by C. Pellegrini, who was hired as an engineer by Rivadavia and as a portrait artist by Rosas; he painted major works of Argentine art “La porteña  en el templo” (The Woman of Buenos Aires in the Temple) (detail), oil painting by Monvoisin, portraying the mourning of Rosa Lastra upon the trial of her father and brother in 1839.',
            'orden' => 22,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          19 => 
          array (
            'id' => 58,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000025.jpg',
            'titulo' => 'Page 25',
            'texto' => 'They sold his oxen, they sold his cows, the soup pot, the blanket from the bed. They sold the cart and the vegetables he had, and he was left alone With only the clothes on his back. María, Iʼm young, I donʼt want to beg, Iʼm going out into the world to see if I can make it Galicia is poor and Iʼm going to Havana… Goodbye, goodbye, treasures of my heart. The controversial issue of immigrants and settlers cannot be viewed from an absolutist perspective; that is, immigrants cannot be considered to be responsible for all of the virtues or faults of Argentine culture. These immigrants did not always arrive in the promised land they had yearned for or been promised. The immi- gration process had two diametrically opposed sides: be- ginning in 1858 with the founding of Colonia Esperanza and continuing until approximately 1890, immigrants were usually welcomed and protected from the moment they arrived, either by provincial governments or by the nation itself; later, however, they were deceived, exploited, and treated practically like slaves by unscrupulous domestic and foreign companies through deceitful recruiting, risky business ventures, breaches of contract, and unfair leasing practices. Figures such as Barón Hirsh, Nicolás Oroño, and Bialet Massé emerged from this period in our history, and each of them, either as a spectator or a protagonist in the drama, left be- hind valuable writings on the topic. “Negra” (Black Woman) (detail) in a city store, lithograph by Isola, an Italian who arrived in 1844 and captured aspects of daily life. “Inmigrante” (Immigrant) (detail) watercolor by Pueyrredón, 1870, inscribed in the basic collection of Argentine portraits and figures.',
            'orden' => 23,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          20 => 
          array (
            'id' => 59,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000026.jpg',
            'titulo' => 'Page 26',
            'texto' => 'Beginning in 1825 under the policies of Rivadavia, Scottish and Danish immigrants began to settle in the province of Buenos Aires. Urquiza founded colonies with his own wealth in Entre Ríos and Santa Fe. In 1865, one hundred fifty-three Welsh, who had suffered under the economic and cultural oppression of the English bour- geoisie, left their country and settled in Chubut, which constituted a genuine advance in the cause of civiliza- tion prior to Roca’s battles against the Indians. (The Welsh had a peaceful relationship with the Indians and were not harmed by them). In the south, the Malvinas, and Tierra del Fuego, the evangelical work of Anglican priests gradually led to the establishment (from Ushuaia) of settlements in which English colonists, natives, and other European immigrants worked together to develop this vast and isolated region. Due to the religious, racial, political, or economic problems that they were facing in their own countries, successive waves of people of every race arrived in Argentina during the nine- teenth and the beginning of the twentieth centuries: Swiss, Germans, French, Poles, Russians, Belgians, Jews, Austro- Particular mention should be made of the Italians and the Spanish, whose presence was such that people used to joke, “Argentina is a country of Italians who speak Spanish” and “The larg- est Galician city is Buenos Aires.” Between 1880 and 1914 alone, more than two million Italians immigrated to Argentina; and at the turn of the century, 20% of the population of Buenos Aires was Spanish. In addition, particularly during the second half of this century, Argentina has become the adopted homeland of Chileans, Uruguayans, Paraguayans, Brazilians, and other Latin Americans who came in hopes of finding work and peace. In 1895, the country’s second census was taken, which indi- cated that of a total national population of 4,044,911, there were 2,950,348 Argentines and 1,094,563 foreigners; anoth- er significant result was that 56% of the population of the Federal Capital was composed of foreigners. The influence of immigrants had an impact on the formation of public opinion, leading Joaquín V. González to reflect: “To a certain extent, (immigration) can be seen as both a cause and an effect of real progress. If this process overwhelms the capacity of the original population to resist, this law of progress becomes a principle of transformation, of change, and of replacement of the old blood with the new blood; …the wisdom of the law consists not in preventing the foreign masses from becoming a part of the nation, but in requiring that they become incorporated into the national population at a mod- erate rate, thereby ensuring that the countryʼs original background is not destroyed, that this initial concept of nation…does not disappear, and that the initial seed is preserved.” In summary, the coexistence of these factors – the various natures and qualities of humankind in their multiple actions and reactions – has created the most accurate profile of Argentina and the Argentines, at least for the present. This profile is not a static and unchanging image, but rath- er a continual process of transformation that accepts and adapts to changing circumstances. The Argentine, in the abstract, seems to have selectively re- tained in the structure of his personality several of the many possible characteristics that derive from the three sources of his ethnicity: from the immigrant, the search for security, flexibility, industriousness, and initiative; from the criollo, the will to exist, idealism, and generosity; and from indig- enous peoples and mestizos, fidelity to one’s roots, res- ignation, and shrewdness. Naturally, each of these factors can tilt in both a positive and a negative direction; the search for security can become a desire to join the bourgeoisie; the will to exist can become a sham; resignation, apathy; and shrewdness, guile (viveza criolla).',
            'orden' => 24,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          21 => 
          array (
            'id' => 60,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000027.jpg',
            'titulo' => 'Page 27',
            'texto' => 'This characteristic – the fact that our country is particularly open to both domestic and foreign influences – is one of our greatest weaknesses; but, at the same time, it is a seed of hope, a seed with the realistic potential for vigorous and superior growth. On the other hand, the force that emanates from the land- scape leaves an indelible mark on the character of the in- habitant. This makes it clear that we are speaking in terms of abstract generalizations – an Argentine who lives in the charming valleys of the northwest, where the general tone of life is one of peace and seclusion, is very different from an Argentine who lives on the Pampas, a land of almost metaphysical vastness, where the inhabitant knows that the plain continues on and on, absolutely unchanged, farther than the eye can see. The first Argentine hears the message of locales where in- digenous and Hispanic traditions are kept alive most strong- ly, a land enriched with memories and legends, the birth- place of songs and verses that constitute the most valuable of cultural contributions to Argentine folklore; there, in the shelter of the valleys, his manner of speech is smooth, polite, and respectful. The second Argentine becomes introverted and quiet, taciturn, rude but noble, strong and profound, the proud and knowing master of the unbridled freedom afforded him by his paths of solitude. The inhabitants of Buenos Aires feel isolated as well, with a supra- national awareness of their unconnectedness to the rest of the world. This phenomenon of the large city, with its demands, com- petitiveness, and distractions, causes the inhabitant to develop a strong sense of individualism, along with a strange mixture of in- terior insecurity encased within exterior self-sufficiency. Frequently, the avid desire for prestige through appearances (a negative aspect of the will to exist) and a lack of ground- edness in more vital values (degradation of fidelity to one’s roots), fuel an exaggerated sentimentalism that is expressed in a valid and authentic creation of the city (the querulous tango) and in the haphazard, spurious enshrinement of transitory and baseless idols. Simply stated, Argentina is divided into two opposing coun- tries: the capital and the interior. In the political division of other countries, a natural distinc- tion is usually made between the capital and the provinces or the federal district and the states. The word interior, however, has positive and negative connotations that be- gin to point to the characteristics of a type of Argentine. Among other definitions for interior, the dictionary gives us: “said of a room or chamber that does not have a view of the street” and “of or relating to something that is felt only in the soul.” In 1580, upon founding for the second time what would become one of the most important cities of the southern hemisphere, Juan de Garay said: “It is essential to open a port to the land.” Buenos Aires did indeed become a huge entry- way on the estuary of the River Plate, but instead of gener- ously opening the way to the rest of the country, it fulfilled the egotistical destiny of great metropoli (a combination of the real concentration of economic and political power and the mythic and always attractive emanation of the idea of abstract power, which was wielded by monarchs and their courts in the old empires) – Buenos Aires absorbed the rest of the country, transforming the great entryway into a rea- son and end unto itself. This has occurred to such a degree that, in the minds of many foreigners, Buenos Aires and Argentina have apparently become synonymous. This dichotomy is clearly stated in the writings of major figures in Argentine history, such as during the process of National Confederation, when the institutional unification of the country was being undertaken: “Geography, history, and agreements link Buenos Aires to the rest of the Nation. Not even she can exist without her sisters, nor her sisters without her.” J. J. Urquiza “I (Buenos Aires) am the Republic of the River Plate…because I have the most right to represent the Argentine Nation.” B. Mitre',
            'orden' => 25,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          22 => 
          array (
            'id' => 61,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000028.jpg',
            'titulo' => 'Page 28',
            'texto' => 'Or during the process of making Buenos Aires the capital: “Buenos Aires, Capital, is a mandate of history….The capital in Buenos Aires is the decision of the nation because it is the voice of tradition itself, the legal fulfillment of the most characteristic feature of our national history.” N. Avellaneda “In attempting to make Buenos Aires the federal capital, Rivadavia has led the country into tyranny; afterwards…Buenos Aires, Capital, will be the center of all things for men of distinction from across the provinces, for all who are worthy will come here, but in so doing they will deprive their respective localities of their effective participation. “And many of those who come will be corrupted by feeding from the official trough, since not all spirits are of the same mettle. All of the splendor will be here, all of the riches, all of the talent, and then… What will be left in the rest of the Republic? Nothing but poverty, ig- norance, darkness, and irksome differences.” L. N. Alem The physical diversity of what we generally call the interior necessarily produces a variety of regionalisms (more pro- found than the political division of the provinces and char- acterized by places, names, accents, customs, and expres- sions clearly indicating differentiated characteristics) that are obviously not strictly interrelated. Every human group develops a strong sense of territoriality with respect to its environment; this sense usually outweighs the sense of “Argentineness,” which feels like something acquired. Again, as popular humor would have it: “God is everywhere, but He answers in Buenos Aires.” And, copying the criticism by Sarmiento to the effect that the Republic of Argentina ended at the Arroyo del Medio: “Argentina ends at Avenida General Paz.” Although there are still no definite plans for developing the country as a whole, these tensions are slowly beginning to diminish. Regionalisms are weakening, partly as a function of the increase in communications, tourism, and sports; ma- jor flows of internal migration are changing what were pre- viously the most insular and distinctive ethnic groups and, on the whole, Argentines are beginning to have a stronger and more genuine national consciousness. In summary, Argentina is a singularly atypical country. Although it developed within the context of Spanish culture, complemented by the contributions of indigenous peoples and mestizaje, like other nations of the continent, the ex- tent and diversity of immigration and the surprising degree of racial intermingling that ensued led to the creation of a country that is not in strict accord with the concept of Latin America as such. The mixing of nationalities and races is a common process that has occurred throughout the world at many times in history. The novelty and uniqueness of this phenomenon of ethnic fusion in this case arises from the great variety, simul- taneity, and speed that has characterized its ongoing devel- opment, resulting in individuals who have a fundamental sense of being Argentines and do not suffer any type of dis- crimination or segregation impeding the development and practice of their social lives, the exercise of their rights, or their opportunities to attain the most highly regarded posi- tions of public life and government as a result of their social class, race, religion, or political convictions.The natural desire of young people for adventure and to gain knowledge of the world, the ambitions of a large and strong middle class (another distinguishing charac- teristic of Argentina) for privileges and prestige, and the snobbery and melancholy of an economic elite have made the Argentine the classic example of the foreign travel- er intent on conquering Europe, first, in order to be able talk about it later and, second, in order to acquire novel objects and anonymously break rules of conduct reflect- ing the ones to which they officially subscribe. Of course, many young people and adults from various walks of life rise above these spurious motivations, traveling because they have a thirst for study, a desire to improve themselves, a wish to become acquainted with that which is different, and a desire for the esthetic experience of contemplating the wonders of nature or the fabulous artistic and techno- logical creations of humankind – in short, with a genuine spirit of pilgrimage.',
            'orden' => 26,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          23 => 
          array (
            'id' => 62,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000029.jpg',
            'titulo' => 'Page 29',
            'texto' => 'The first type is merely a tourist and represents an Argentine who is arrogant and conceited, enthralled with his own – false – self-sufficiency; the second is a real traveler, and his contact with that which is different contributes to mental quickness, enthusiasm for progress, and an ability to under- stand a broad range of problems and circumstances, each of which is also a characteristic of the Argentine. Interestingly, the most significant discovery usually made by these travelers to other countries, particularly by the sec- ond type (given that the first type is characterized by a pu- erile and irrational chauvinism), is that they reappraise and meditate about their own country, which inevitably leads to yearning. Exile, whether voluntary or obligatory, is very hard for an Argentine. Mass media, reading materials, escapist fantasies, and myths have easily led to the mental “Europeanization” of our people; an excessive admiration for everything foreign, everything civilized, without making a clear distinction be tween the genuine and the mediocre (“If itʼs imported, itʼs good” seems to be an irrefutable axiom). However, if some- one gains public recognition in Europe (for real or spurious reasons) for the very expression that had been ignored, the compulsion to blindly admire everything foreign enters into play, and the person in question can then return, fully con- fident that all doors will be open to him. As early as 1846, Esteban Echeverría criticized the frivolous way in which we see things: “because we rarely allow anything to make an impression on us such that it becomes ingrained in memory.” As a result, we forget the origins and values of the factors that contributed to the formation of our identity. “This is why, since the beginning of the Revolution, we have gone around and around in a monotonous vicious circle, like donkeys around a mill, and have never escaped from our predicament. This is why we have expended our energies on all sorts of endeavors, only to retry everything we have forgotten  This is why we never salimos de Cristo (*) (succeed at all), because we never store up the knowledge we have acquired.” Our insecurity or fear of ridicule often causes us to ignore or reject truly authentic native expressions.“Our educability is considerably hindered and damaged by other fac- tors as well. One is the naïve and fevered impatience with which we imagine ourselves instantly achieving proposed goals without work or difficulties; another is our new fickleness, which always leads us to seek out and delight in the new, while forgetting the familiar. “Europe often unintentionally promotes and draws out this disposition, which is very conducive to learning when it is well directed… It often prevents the good seed from taking root and leads to the confusion of ideas, for it shakes faith in acknowledged truths, injects doubt, and keeps unruly spirits in a constant state of fruitless agitation”. These thoughts, written 140 years ago, are surprisingly cur- rent today. We concentrate on intelligently absorbing what other countries have to offer – but without allowing any of it to have an impression on us – to such a degree that we forget our own identity. This identity is still a work in prog- ress, and it cannot be brought to fruition without real ef- fort. In particular, we cannot proceed unless, through a pro- cess of self-examination aimed at discovering the point at which we lost our way, we preserve our achievements and reconsider our mistakes, not to bewail them but to learn from them. In so doing, we will be integrated into our com- mon culture, not for the purpose of falling into an aberrant nationalism, but in a way that is conditioned by a feeling of love and appreciation for our own roots. Such a state of mind will make it possible to overcome con- tradictory and inexplicable situations, such as the lack of a consistent, ongoing, and comprehensive policy with respect to the development of different centers of potential wealth, which would make it possible to finance progress, resolve the cultural dichotomy, and improve the quality of life of all Argentines. he characteristics of the desert are still present. The country itself is still a world apart from the largest population cen- ters; extensive regions of passive continuity, in opposition to limited centers of gradually increasing  (*) Very much in the principles of a particular art or science.',
            'orden' => 27,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:17',
            'updated_at' => '2019-01-12 19:01:17',
          ),
          24 => 
          array (
            'id' => 63,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000030.jpg',
            'titulo' => 'Page 30',
            'texto' => 'dynamism. The discontinuity of political life is not unrelated to this state of affairs. From 1930 to 1983 (during which period there were nine constitutional presidents), the country had 28 differ- ent leaders. The inhabitants’ psyche has fallen ill: repeated hopes and frustrations have led to disbelief and skepticism, isolation, and rootlessness. Impatient, critical, and intolerant, always awaiting the sud- den appearance of a savior, the country has alternated – ex- cept for a few brief periods – between populist regimes un- der the sway of demagogues and arbitrary and repressive authoritarian governments. These observations are not pessimistic; rather, they state symptoms that encourage us to undertake an objective and impartial analysis aimed at regaining the ideals and princi- ples of generosity, humanity, and culture that have made us what we are. Looking above the storm: Sarmiento, in times of terrible crisis and anarchy, continued to work and plan for the Argentina that he sensed and hoped for. (If someone can- not see the horizon, it is because he is observing from the bottom of a well.) Any country that does not have faith in its identity is on the wrong path. Our nation is seeking, however inconsistently, that achievement that will enable it to overcome its own difficulties; that uniting fervor, that oneness that binds, in- creasing in strength and power through the energy that flows from the consciousness of acting toward common ends. The Plaza de Mayo, traditional site of popular demonstra- tions, exemplifies this situation: in the span of a week, an angry and threatening crowd gathers to protest economic conditions and later excitedly expresses its willingness to sacrifice; on each occasion it faces the same leader, who (properly or not) presses the buttons of nationality and sov- ereignty. Tension is a permanent factor in the private lives of Argentines today: this is negative in principle, but also ex- tremely positive because of the dynamism that it creates. If we know how to use this tension, we can redefine the prob- lem in two ways: by modernizing our character and behavior in order to bring them into accord with our current reality, and by understanding the past in order to avoid repeating our mistakes, but without destroying it altogether. It is vital to reinforce the concepts of place and humanity. Likewise, there is a need for a persistent, selfless, and strong effort to properly prepare the way for the new generation to shape, through their feelings and actions, the integral concept of Argentina. The future is today.',
            'orden' => 28,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          25 => 
          array (
            'id' => 64,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/01_introduccion/en/00000031.jpg',
            'titulo' => 'Page 31',
            'texto' => '“Campo Sembrado” (Sown Field), acrylic on canvas by Claudio Riquelme (1933), Zurbarán Gallery of Art.  Pampean Region “Viejo Mercado del Norte” (Old Market  of the North), gouache by Leonie Matthis (1883/1952), Zurbarán Gallery of Art.  Andean Region “El Palmar”, (The Palm Tree), Colón, Entre Ríos, oil painting on canvas by Fernan- do Romero Carranza (1935),  Zurbarán Ga- llery of Art.  Chaquean Region      “Serenidad en el Lago”, (Peacefulness on the Lake), oil pain- ting by Angel Domingo Vena (1888/1983), Zur- barán Gallery of Art.  Patagonic Region',
            'orden' => 29,
            'idioma_id' => 2,
            'capitulo_id' => 16,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
        ),
      ),
    ),
    2 => 
    array (
      'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/thumbnail.jpg',
      'imagen_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/thumbnail.jpg',
      'imagen_titulo' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000032.jpg',
      'imagen_titulo_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000032.jpg',
      'titulo_espanol' => 'Las Regiones',
      'titulo_ingles' => 'The Regions',
      'texto_espanol' => 'Las Regiones',
      'texto_ingles' => 'The Regions',
      'orden' => 3,
      'created_at' => NULL,
      'updated_at' => NULL,
      'paginas' => 
      array (
        'es' => 
        array (
          0 => 
          array (
            'id' => 65,
            'imagen' => '/storage/app/public/tomos/capitulo_title_background.jpg',
            'titulo' => 'Página 32',
            'texto' => '',
            'orden' => 3,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          1 => 
          array (
            'id' => 66,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000033.jpg',
            'titulo' => 'Página 33',
            'texto' => 'Para dar una visión más integral y organizada del país (que incluye tantas diferencias), he considerado conveniente pre- sentarlo por regiones, cada una de las cuales constituirá un volumen de esta obra, así también como el área metropoli- tana, aunque a ésta no desarrollaré aquí como Región. Será tratada exhaustivamente en el último volumen como “El Otro País”, como ciudad nacida con un destino hegemónico y cuya singular influencia ha excedido en mucho los límites de la Capital Federal. De los posibles criterios para determinar las regiones del País Interior, he preferido, por la amplitud y raigambre de sus fundamentos, seguir el enfoque elaborado por el sociólogo Dr. Juan Carlos Agulla en su Estudio sobre la Sociedad Argentina. En consecuencia, paso a sintetizar los conceptos funda- mentales de la citada obra, adaptándolos al enfoque que he desarrollado en el capítulo anterior y que permanecerá como patrón de toda esta obra; es decir, con la vista pues- ta en la permanente interrelación de los tres polos intervi- nientes: naturaleza, hombre, y su consecuente resultan- te, el producto, que (involucrando todos los factores que caracterizan una sociedad), podemos unificar con el térmi- no cultura. Las regiones argentinas son unidades estructurales que han emergido a través de complejos procesos y factores intervi- nientes (geográficos, históricos, demográficos, económicos, sociales, etc.) y que al imbricarse como un cañamazo, fueron constituyendo riquísimos tapices, con una indudable cohe- sión interna, cuyos rasgos relevantes diseñan formas de vida de una razonable homogeneidad.Podemos aproximarnos al planteo del problema de la regio- nalización a partir de una teoría o puntualizaciones histó- ricas, que puedan enriquecer la explicación del pasado, la comprensión del presente y la prolongación del futuro, po- niendo los resultados al servicio de una nueva política na- cional en base a realidades regionales. Este planteo, vincula el proceso de integración nacional con el proceso de diferenciación regional que se ha ido desarro- llando a través de caracteres integradores: la conciencia nacional; y caracteres diferenciadores: potencialidad y posibilidades de las regiones. La Constitución de 1853, después de largos y dolorosos mo- mentos vividos por el pueblo argentino, fijó por primera vez con claridad lo que aspiraba ser como sociedad nacional. A partir de ese momento, el estado implementa una políti- ca liberal que se basa en la importación de productos ma- nufacturados y exportación de productos agropecuarios. Se descubrió la pampa y se asignó a esta incipiente región una función decisiva y prevaleciente en el proyecto nacional, consolidándose así el núcleo pampa-puerto. El planteo político pampa-puerto como idea-fuerza de la polí- tica liberal, se enfrentó al planteo político continente-cordille- ra, propio de la vocación latinoamericana del viejo virreinato y del período de las luchas federales de la Confederación, iden- tificándose el primero como civilización y la segunda como barbarie sin asignar mayores diferencias entre ellas. La idea pampa-puerto se reforzó con la conquista del de- sierto, el asentamiento de la inmigración europea y el tra- zado de los ferrocarriles. El puerto fue el factor de discordia entre la ciudad y la región pampeana anexa, y la heterogé- nea región interior, al no tener la misma, asignadas funcio- nes específicas en el proyecto nacional y reclamando su par- ticipación en los beneficios del puerto. La dinámica de la región pampeana, en cambio, fue am- pliándose, absorbiendo Entre Ríos y otras regiones como resultado de una política que le asignaba manifiestamente una función prevaleciente, gracias a la continuidad de esa política durante más de 90 años- De este proceso histórico emergen dos conciencias sociales dispares y enfrentadas; en la región pampeana, una concien- cia social cargada de optimismo y con una función en el pro- yecto nacional: un nacionalismo centrista e internacional (o europeo): La Argentina Feliz, según Daus. Por el otro, el postergado interior, una conciencia social cargada de rebeldía y sin función manifiesta; un regionalismo federalista y conti- nental (o latinoamericano): La Argentina Pétrea (Daus).',
            'orden' => 4,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          2 => 
          array (
            'id' => 67,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000034.jpg',
            'titulo' => 'Página 34',
            'texto' => 'El interior en forma masificada comenzó a constituirse en una región residual, una suerte de reserva antropológica del proyecto nacional y sus economías eran sólo regionales. No obstante el proceso de regionalización fue delineando con más fuerzas otras subregiones geográficas, particularmente la Andina, en virtud de su tradición y desarrollo histórico. El territorio nacional, sin embargo, no quedó agotado con estas dos regiones, dejando otros ámbitos geográficos (por donde, desgraciadamente, apenas pasó la historia) y que al comenzar a tener vigencia inician un nuevo proceso de re- gionalización, producto tardío de la política argentina. Surgen así, la región chaqueña y la región patagónica, que- dando de esta manera constituidas cuatro regiones: la pam- peana prevaleciente, la andina residual y la chaqueña y pa- tagónica emergentes, sin que estos conceptos tengan una carga peyorativa o laudatoria. Región Pampeana Naturaleza: El centro del territorio nacional es una planicie casi sin árboles, de clima templado, sin estación seca y cu- bierta por una alfombra de gramíneas durante todo el año. Está unida al interior por rutas naturales y vías fluviales y te- rrestres, sin accidentes geográficos significativos; se recues-  ta en el Río de la Plata, de tal suerte que toda la planicie se abre hacia el mundo occidental. El dinamismo de esta región fue increíble; desde las 600.000 hectáreas cultivadas en 1872, fue creciendo en todas las di- recciones, conquistando para sí, el norte y este de La Pampa, el sudeste y centro de Córdoba, el centro sur de Santa Fe y el sur y centro de Entre Ríos, pasando, en 1900 a 10.000.000 de hectáreas y en 1946 a 22.700.000 hectáreas. Igualmente absorbió la mayor concentración de población que se asentó en los centros urbanos de Buenos Aires y su zona metropoli- tana, y en Rosario y el litoral. Hombre: Posee una franca homogeneidad demográfica, social, económica y cultural, que unida a su carácter de re- gión prevaleciente del país, llega hasta el punto en que, ac- tualmente, sus habitantes representan casi la imagen exte- rior de la Nación. Los habitantes naturales eran indios cazadores y nómades del escaso nivel cultural; su encuentro con los españoles, básicamente radicados en la zona ribereña generó largos y variados conflictos. Del fragor de tal lucha surge como respuesta antropológica, el gaucho, prototipo del hom- bre de la pampa, que al decir de Head “…vive de privaciones pero su lujo es la libertad. Orgulloso de su independencia sin lími- tes, sus sentimientos –salvajes como su forma de vida– son nobles y buenos”. La historia de esta región es la historia de la conquista del mundo salvaje, para proteger las fronteras y ganar nue- vos territorios, ampliando de esta manera, la influencia del puerto. Poco a poco, los primeros cazadores concluyen como comerciantes de productos agropecuarios. La política de inmigración condujo a la asimilación, en esta región, del mayor número de los inmigrantes que llegaban al país gracias a la política de colonización que ofreció tie- rras y facilidades; a la política de inversión, que permitió la instalación y expansión de los ferrocarriles (delineados con adecuación al puerto); y la política educativa, que ofreció la posibilidad de integrar, rápidamente las masas inmigrantes a la nacionalidad. Producto: Las reses y caballos traídos y abandonados des- de la llegada de Pedro de Mendoza, encuentran las pasturas naturales y allí comienza la riqueza ganadera de esta zona, que se diversifica y amplía con la cuidada mestización con especies finas, alambrado de campos y manejo de hacien- da, con la exportación de carnes, cueros crudos, crin, cebo, huesos, etc. y la instalación de los primeros saladeros y barracas. A mediados del siglo XIX comienza el mejoramiento de las razas: aparecen las primeras cabañas. También la agricultura se especializa, se introducen semillas seleccionadas y adelan- tos tecnológicos, con una diversificación',
            'orden' => 5,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          3 => 
          array (
            'id' => 68,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000035.jpg',
            'titulo' => 'Página 35',
            'texto' => 'del original cultivo del maíz, trigo y lino, incluyendo avena, cebada, centeno, granos oleaginosos, forrajeras, etc. Los beneficios del intercambio interno y externo recaen en la misma región, la boca de expendio fue también la boca de consumo y el destinatario del beneficio. Acontecimientos como la Primera Guerra Mundial, la cri- sis del año 29 y la Segunda Guerra Mundial, inciden en la economía, provocando un parcial estancamiento de la pro- ducción agropecuaria pero incentivando la industria de la región. Aparecen, así, los frigoríficos, fábricas textiles y de confección, las industrias metalúrgicas, químicas, petroquí- micas, farmacéuticas, siderúrgicas, así como grandes refine- rías y destilerías. Toda la línea desde Rosario, Campana, Zárate, San Nicolás, Avellaneda, Bahía Blanca, etc., se va transformando en parque Industrial, que culmina con la instalación de la in- dustria automotriz (1959) y de la planta de energía nuclear de Atucha (1974). Sólo aparecen en el país algunos polos aislados de desarrollo industrial especializado, fundamen- talmente Córdoba, con la fábrica militar de aviones (1927), fábrica de automotores, ferrocarriles, tractores, motores, molinos, etc. y Bahía Blanca, Mar del Plata, Santa Fe, así como otras ciudades menores, relacionadas con la indus- tria alimenticia. Al ser incentivado este proceso por las distintas políticas proteccionistas, la región pampeana siguió siendo la privile- giada de los bienes de la Nación. Región Andina Naturaleza: Apoyada sobre el Cordillera de los Andes, abar- ca el centro y noroeste del territorio nacional. Montañas, valles, quebradas y salares –que crean obstáculos natura- les para su intercomunicación– en una extensión de casi 700.000 kilómetros cuadrados, presenta, no obstante, una relativa homogeneidad social y económica y cultural dentro de las diferencias que, naturalmente, provocan las regiones montañosas. Clima continental y seco, aire puro y cielo azul con escasas llu- vias que llegan a la sequedad del desierto en La Puna; la ari- dez hace que toda la vida se concentre en los valles. La región tiene una especial peculiaridad que la identifica o vincula con la originalidad de la vieja América hispana e indígena. Hombre: La región fue asiento de las culturas indígenas más desarrolladas y progresistas del territorio nacio- nal. Eran Pueblos sedentarios y agricultores, con un nivel de cultura que se manifestó en sus obras de arte y de arte- sanía. Con la llegada de los españoles desde el Perú (siglos XVI y XVII), fundando casi todas las ciudades importantes en función de encontrar en estas zonas recursos naturales y población indígena como mano de obra, se dio fundamen- talmente, en esta región la mestización argentina y la su- bordinación de los indígenas en la organización de las enco- miendas y de la mita. La región fue protagonista del proceso histórico en el período de la Conquista, de la Colonia, y aún en las gue- rras de la Independencia, dentro de las luchas federales, fundada en esa vocación andina y continental que la ca- racterizó desde su origen. Con el proceso de integración na- cional comienza un proceso de residualidad, al par que un proceso de integración como región unitaria. La vida se desarrolla en algunas ciudades grandes, unas po- cas ciudades pequeñas, muchos pueblos y poblados y una población rural considerable. La misma ha emergido bási- camente de un crecimiento vegetativo, salvo pequeños con- tingentes de inmigrantes europeos, españoles e italianos, o de origen siriolibanés, a fines del siglo XIX, que fueron im- portantes para el desarrollo de la industria y el comercio de la región. No obstante, la base mestiza y criolla sigue constituyendo el núcleo étnico de la región, con una fisonomía tradicional. Producto: El carácter residual dentro del marco de la so- ciedad nacional deviene de una pérdida de la funcionalidad que tenía desde el comienzo en la vida colonial, hasta los primeros años de la Independencia Nacional, como conse- cuencia del largo proceso político conducido por el Estado Nacional.',
            'orden' => 6,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          4 => 
          array (
            'id' => 69,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000036.jpg',
            'titulo' => 'Página 36',
            'texto' => 'En su primera época fue importante el desarrollo alcanzado por la explotación del ganado (mular) y la agricultura (maíz, papa, vid, olivo, etc.); una minería incipiente, así como el desarrollo de una artesanía y pequeña industria. A partir de 1853, importancia y economía comienzan a de- caer y, no obstante sus luchas defensivas contra el poder del puerto (que lograron algunas ventajas), nunca se obtuvo una asignación de funciones en el proyecto nacional. La ex- plotación de la tierra está organizada en gran parte, toda- vía, bajo forma de estancias (quizá resabios de las viejas en- comiendas) y su productividad depende de las condiciones de los valles y principalmente de la disponibilidad del agua. Al agotarse el proyecto liberal hacia la década del 40, el Estado Nacional, intenta una política para el desarrollo de la región; Tucumán, Mendoza y San Juan, gracias a una economía específica desarrollada durante muchos años (caña de azúcar y vid), fueron centros “protegidos por la política nacional; además consiguieron industria- lizar su producción agropecuaria. Posteriormente, algo semejante ocurre también en Salta y Jujuy que comien- zan un desarrollo vinculado con la explotación del petró - leo (refinerías), del hierro (siderurgia) y en los últimos años cobran impulso industrias derivadas de la produc- ción agrícola: alcohol y papel en Tucumán, conservas en Mendoza, etc. Córdoba presenta una cierta peculiaridad. Integrada ini- cialmente a la región andina, tiende posteriormente a in- corporarse en forma parcial a la región pampeana, con el asentamiento de inmigrantes, una mayor industrialización, producción agropecuaria, etc. La región andina es básicamente productora de materias primas: tabaco, frutales, olivos, hortalizas, minerales, cobre, plata, oro, berilo, manganeso, cemento, mármoles, etc. y madera para leña, postes y vigas, especialmente de los mon- tes del árbol típico: el algarrobo. Ganado del tipo ovino Karakul, la llama, la alpaca, la vicuña y la chinchilla, originan una limitada industria de corte es- trictamente artesanal y folclórico. El aprovechamiento del agua es el tema básico de la región (que parcialmente han logrado sistematizar, en espe- cial en Córdoba y Mendoza), tanto para el aprovechamiento y el regadío como para la energía hidroeléctrica. Una polí- tica adecuada en este sentido, puede llegar a concretar las potencialidades de esta región que son inmensas. Región Chaqueña Naturaleza: Comprende la región Norte y Este del terri- torio nacional, integrada por sus características generales con los países limítrofes (de los cuales está separada por grandes ríos). Presenta vías fluviales significativas e impor- tantes que se integran en la cuenca del Paraná y que crean un paisaje homogéneo con las variaciones naturales de toda gran extensión. Clima subtropical y húmedo con planicies y bosques que dan una cierta fisonomía unitaria a más de 500.000 km2 de una relativa homogeneidad geo- gráfica, destacándose las subregiones del Chaco occiden- tal, Chaco Oriental, los esteros y lagunas correntinas y la selva misionera.  Hombre: Originalmente tuvo una población indígena bas- tante numerosa, de cierto nivel cultural, especialmente de base guaraní, asentada en las márgenes de los ríos. Hasta hace poco resultaba una zona marginal dentro del esquema de la nación, no obstante la antigua tradición his- tórica, como Corrientes durante las luchas federales, las Misiones Jesuíticas desde el comienzo colonial y todo el cauce litoraleño asentado en la ruta de los conquistadores hacia el Norte de la Colonia y como acceso del Perú. La región mantiene, en el marco de la sociedad colonial un carácter todavía incipiente, aunque en los últimos años se advierte una tendencia a una homogeneidad estructural y a una cohesión interna, no obstante, no hallarse su población suficientemente integrada. a zona de Formosa y Chaco, cuenta todavía hoy, con grupos indígenas, de otras razas, que subsisten en una si- tuación sumamente precaria. La base hispánica y mestiza, particularmente en la vieja provincia de Corrientes y la inmi- gración extranjera (especialmente alemanes, polacos, brasi- leños y paraguayos) se distribuyeron en una',
            'orden' => 7,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          5 => 
          array (
            'id' => 70,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000037.jpg',
            'titulo' => 'Página 37',
            'texto' => 'concentración urbana significativa. Corrientes-Resistencia y ciudades como Posadas y otras más pequeñas, existiendo además un gran índice de población rural. Producto: Es una región, potencialmente capacitada para cumplir una función integradora con América Latina y com- pensadora, en las posibilidades propias de las regiones sub- tropicales, en un futuro proyecto nacional. Inicialmente, fue casi una factoría de la región pampeana, de donde se extraía madera, tanino, yerbamate, etc. Con la provincialización de los territorios que la integraban, comien- za su participación en la vida política del país y una incen- tivación de la explotación económica, especialmente agro- pecuaria. El algodón fue el pionero a cargo de colonos establecidos en tierras fiscales que llegaron a cultivar hasta 200.000 hectáreas. Surge también la explotación de produc- tos tropicales, frutas, hortalizas (batata, sandía, mandioca, tártago, sorgo, bananas, té, tabaco, tung, soya, arroz, etc.) que alcanzan a tener repercusión nacional, como en el caso del té y el banano (Formosa); el arroz y el tabaco (Corrientes); el cafeto y el té (Misiones); el algodón (Chaco), etc. La industria es todavía incipiente, la más significativa es sin duda, la de la madera (Obrajes y aserraderos) –el quebracho colorado, la araucaria de Misiones, etc.– y otras industrias re- lacionadas con la madera; en el momento presente, la indus- tria del papel puede constituir una de las fuentes más impor- tantes de riqueza para una necesidad imperiosa del país. Otras fuentes de recursos significativos representan la in- dustria de la yerba mate (secaderos), del algodón (desmo- tadora), vinculada con la utilización de la semilla para acei- tes y la fibra para hilanderías y tejedurías, aparte de las ya mencionadas como el té, el tabaco, el arroz, el tung, etc. Toda la explotación está vinculada a la sustitución de impor- taciones de los productos subtropicales que pueden ser producidos en la Argentina y la incentivación del proceso va a depender de la asignación de una función específica en una futura política nacional, teniendo en cuenta, además, las incontables posibilidades que abre la utilización de las capacidades energéticas de la región (Yaciretá-Corpus) y la posibilidad de fácil comunicación con los centros consu- midores, fundamentalmente, la región pampeana, el Brasil, el Paraguay y Bolivia. Esto último reclama una apertura de fronteras sin prejuicios y con una clara vocación latinoameri- cana. El desarrollo de la inmensa potencialidad de la región puede ser factor fundamental en la realización total e inte- grada del país. Región Patagónica Naturaleza: Una región inmensa y aislada que se extiende casi como una gigantesca península que se aproxima al con- tinente Antártico. Posee netas subregiones que la califican y diversifican, si bien la inmensidad, los vientos y el clima ri- guroso parecieran un factor común. La franja costera presenta un rico litoral marino, con epi- sodios singulares como la Península de Valdés o la amplia ria y las fuertes mareas de Gallegos, y la vincula a las islas, especialemente la Isla Grande que comparte con Chile, las cuestionadas Malvinas, la de Los Estados, con su alucinante perímetro, etc. La región central constituida por secas y escalonadas me- setas esteparias, donde parece leerse todavía la prehistoria por los rastros de intensos volcanismos, residuos de mares desaparecidos y riqueza paleontológica, y la zona cordille- rana y lacustre de gran diversificación paisajista: majestuo- sos escenarios con altos picos, glaciares, frondosos bosques de variada y colorida vegetación, rincones de valles apaci- bles y fértiles. La Región se identifica a lo largo de la historia con un con- cepto de lejanía, algo así como “El salvaje Sud”. Hombre: Sus primeros pobladores ingresan a la historia occidental a través del camino de la leyenda: los gigan- tes Patagones que asombraron a Magallanes, Pigafetta, Byron y tantos otros. Si bien no densamente poblada, sus habitantes eran nómades, recolectores y cazadores, por lo cual no hay monumentos (a excepción de singulares picto- grafías) pero poseían una cultura que arqueólogos y antro- pólogos especialmente en este siglo, siguen poniendo en evidencia.',
            'orden' => 8,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          6 => 
          array (
            'id' => 71,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000038.jpg',
            'titulo' => 'Página 38',
            'texto' => 'Tehuelches, Mapuches, Onas, Alacalufes, Hausch, y los sin- gulares canoeros Yaganes, surcando las aguas de los archi- piélagos australes, eran los señores de la región. Luego el creciente aluvión de razas europeas se va intro- duciendo, conquistando y acorralando al indígena, si bien hubo humanitarias excepciones con misioneros ingleses y la Orden Salesiana. Con el correr del tiempo colonizaciones promovidas apor- tan españoles, italianos, galeses, sudafricanos, etc. y para- lelamente, el terreno virgen y rico afinca hombres de todas las nacionalidades. Finalmente se inicia una nueva corriente de migración interna con hombres de otras provincias y aún de países vecinos vinculados con la explotación de distintas riquezas. Producto: La Región, que el Estado mantuvo inerte como una potencial fuente de recursos fue creciendo básica- mente por esfuerzos privados de pioneros. Riqueza para- digmática inicial fue la lana; se mejoraron la calidad de los rebaños aunque se ignoró el manejo de la hacienda y se rea- lizaron las instalaciones para la gran explotación del “oro blanco”. Pero a comienzos del siglo es el “oro negro” que comienza a movilizar nuevos emprendimientos. Petróleo y gas en gigantescas reservas, a los que hay que sumar el car- bón, el hierro, el aluminio, etc. Los pálidos reflejos áureos de Popper se ven actualmente confirmados con importantes explotaciones. El mar y la pesca es otro importantísimo recurso casi ex- clusivo de esta región, que requiere decisiones políticas y co- merciales mas atentas para que pueda cumplir acabadamen- te con el rol vitalizador. El aprovechamiento de las fuentes hidroenergéticas dota de un potencial que nutre los requerimientos del país y el manejo del agua y regadío, ha generado verdaderos em- porios en las áreas beneficiadas del cultivo e industria- lización, que son fuentes importantes para el país y la ex- portación. Y otro de los recursos más significativos, basado en la varie- dad, esplendor y exclusividad de sus escenarios es el turis- mo, que atrae actualmente numerosas corrientes naciona- les y extranjeras, que comprenden desde el juvenil turismo- aventura hasta los más sofisticados cruceros por los canales fueguinos y la Antártida. Evidentemente podemos decir que la potencial condición de Región emergente, en el momen- to actual, se ha concretado en una tangible realidad.',
            'orden' => 9,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          7 => 
          array (
            'id' => 72,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000039.jpg',
            'titulo' => 'Página 39',
            'texto' => 'n los siguientes cuadros se ha tratado de puntualizar sin- téticamente, aquellos sucesos y acciones que, a manera de hitos en el acontecer de nuestra historia, determinaron o contribuyeron a la caracterización de nuestro país y a la con- figuración del hombre argentino. Al reescribir esta segunda edición, decidí desglosar como un volumen introductorio a la colección, toda la primera par- te, que trata de la Argentina en general y donde se incluye esta sección. Por cierto, los hitos puntualizados abarcaban hasta 1985. Quedaba la posibilidad de actualizarlos hasta 1999. Aparecerían allí numerosas “novedades” del pasado, con los hallazgos de fogones en Península de Valdés, pin- turas rupestres en el Lago Lácar, la extraña construcción piramidal de Catamarca en la que trabaja Rex González, numerosos dinosaurios y huevos y embriones petrificados; catástrofes naturales desde la erupción del Volcán Hudson hasta las inundaciones del litoral; medidas políticas y econó- micas, desde el intento del traslado de la capital, la modifica- ción de la Constitución, la autonomía de la Capital, reformas económicas y privatizaciones de bienes y servicios públicos, la estructuración del MERCOSUR, etc.; momentos de due- lo, como los atentados a la Embajada de Israel, la AMIA, la muerte del periodista Cabezas, del soldado neuquino que provocó la supresión del Servicio Militar Obligatorio, etc.; la definición de nuestra frontera con Chile a través de suce- sivos tratados; episodios tan dispares como distinciones in- ternacionales a la producción cinematográfica, la muerte de Borges, Piazzola, Bioy Casares, o la ascensión de Maradona a figura emblemática del fútbol; el acceso de Bianciotti a la Academia Francesa de Letras o las distinciones, premios y prestigios en el mundo, de figuras como Julio Bocca, Paloma Herrera, Maximiliano Guerra, Herman Cornejo, etc. en ba- llet, Cura, Lima, etc. en la lírica, maestros de la dimensión de Baremboin, Marta Argerich, etc., harían un elenco inagota- ble, y obviamente siempre incompleto. Opte entonces por una opción más interactiva y coherente con los propósitos del libro: hacer como en la estructura de las viejas novelas policiales, un Desafío al Lector, para que cada uno, en una profunda introspección seleccione e inclu- ya, con su escala de valores, aquellas circunstancias y prota- gonistas que juzgue mas valiosos para el desarrollo político, científico, cultural, etc., del pueblo argentino.',
            'orden' => 10,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          8 => 
          array (
            'id' => 73,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000040.jpg',
            'titulo' => 'Página 40',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Siglos XVI y XVII: Descubridores y fundadores Una continua búsqueda de la salida hacia el Pacífico, trae aparejados des- cubrimientos y acciones donde co- mienza a enraizarse la historia de la conquista española en nuestro terri- torio: 1516, Solís, y el ingreso al mun- do de nuestro majestuoso Mar Dulce: el Río de la Plata; 1520, Magallanes, Elcano, Pigafetta: asentamientos pre- carios, el Estrecho de Todos los Santos, la circunvalación del globo terráqueo, crónicas y cartografía; 1527, Gaboto- Ayala y la fundación de Sancti Spíritu y luego, 1536, Juan de Ayola, el fuerte Corpus Christi, origen de nuestros po- blados donde se siembra y se cosecha por primera vez trigo “argentino”. Ulrico Schmidl (que permaneció 20 años en estas tierras) nos deja este testimonio: el grabado de “El Hambre en Buenos Aires”, que acuerdan con la “Ele- gía” de Fray Luis de Miranda: “Las viandas más usadas / eran cardos que buscaban y aún éstos, no lo hallaban / todas veces. El estiérol y las heces / que algunos no digerían, muchos tristes los comían / que era espanto Allegó la cosa a tanto, / que como en Jerusalém, la carne de hombre también / la comieron; las cosas que allí se vieron / no se han visto en escritura: ¡Comer la propia asadura / del hermano...!” as tierras del Mar de Solís empiezan a cobrar nuevo nombre Portada del libro de Martín del Barco Centenera (1602) en cuyo canto primero se leen las siguientes estrofas: “Del indio chiriguano escarnecido, en carne humana origen canto solo, por descubrir el ser tan olvidado del Argentino Reino; gran Apolo envíame del monte consagrado, ayuda con que pueda aquí sin dolo, al mundo publicar en nuestra historia, de cosas admirables, la memoria. Haré con vuestra ayuda este cuaderno del Argentino Reino, recontando diversar aventuras y extrañezas, prodigios, hambres, guerras y proezas”',
            'orden' => 11,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          9 => 
          array (
            'id' => 74,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000041.jpg',
            'titulo' => 'Página 41',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1525. Capitulación de Carlos V del 4 de marzo: “Que la gente que en islase tierras descubriéredes... no se le haga ni fuerza, ni robo, ni muerte, ni otro daño ni agravio, ni desaguisado alguno... porque mi voluntad es que sean de paz e que no se les haga ninguna cosa injusta ni de mal tra- tamiento...” 1527. Diario marino de la armada de Loaysa: “... en el paraje del río de Solís, que dicen de la Plata...” primer documento donde apa- rece el nombre del río. 1536. Ulrico Schmidel, soldado bávaro y nuestro primer cronista en alemán: “Cuando los querandíes pusieron cerco a la ciudad pa- decían todos gran miseria que muchos morían de hambre... aumentaba estas angustias haber ya faltado los gatos, los ratones, las culebras y otros animalejos inmundos, con que sabían templarla y se comieron zapatos y otros cueros. Entonces fue cuando tres españoles se comieron secreta- mente un caballo que habían hurtado... y fueron ahorcados, y por la noche otros tres españoles les cortaron los muslos y otros pedazos de carne por no morir de hambre”. 1536. Don Fernando de Zamora, médico privado de Don Pedro de Mendoza es el primer médico llegado a estas latitudes.  1544-1605. Martín del Barco Centenera, escribe en 1602 su poema “La Argentina” cuyo título anticipa el nombre de nuestro país. 1586. Ingresa al país la Orden jesuita, comenzando su enorme obra misionera y cultural. 1593. Primer juicio por responsabilidad profesional a Asencio Telles de Rojo (de quien se dice que no es sino barbero y se le considera responsable por la muerte de siete esclavos). Pedro Hernández, cronista de la expedición, escribe sus “Comentarios de Alvar Núñez Cabeza de Vaca”, que representa un colorido cuadro de la vida en el siglo XVI, a lo largo de los ríos Paraná y Uruguay. 1603. Don Pedro Díaz, primer médico según anotaciones del Archivo General de la Nación. 1612. Ruy Díaz de Guzmán, criollo y mestizo de Asunción, es autor de crónicas con intenciones literarias y compone “La Argentina” (que circuló y fue conocida como Manuscrita) donde aparece relatado el episodio de Lucía Miranda. 1604-1680. Luis de Tejeda y Guzmán. Comienza el siglo con el primer autor argentino, nacido en Córdoba, donde escribe su poema “El Peregrino de Babilonia” adhiriendo a una estética barroca y gongoristañ.',
            'orden' => 12,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          10 => 
          array (
            'id' => 75,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000042.jpg',
            'titulo' => 'Página 42',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Requerimientos económicos y de soberanía comienzan a definir territorios La necesidad de afianzar y dejar cons- tancia del dominio; la búsqueda de una ruta que derivara las riquezas del Perú por el Atlántico, da origen a co- rrientes fundadoras, y los Adelantados de Carlos V establecieron fortalezas y poblados. Corrientes terrestres: fundadoras de ciudades desde el norte y el oeste: 1550, Ciudad del Barco (Juan Nuñez del Prado; 1553, Santiago del Estero (Francisco de Aguirre); 1559, Londres (Juan Pérez Zurita); 1565, San Miguel de Tucumán (Diego Villarroel); 1573, Córdoba de la Nueva Andalucía (Jerónimo Luis de Cabrera); 1582, San Felipe de Lerma de Salta (Hernando de Lerma); 1593, San Juan Bautista de Todos los Santos de la Nueva Rioja (Juan Ramírez de Velazco); 1593, San Salvador de Velazco en el Valle de Jujuy (Francisco de Argañaraz); 1561, Mendoza (Pedro del Castillo); 1572, San Juan de la Frontera (Juan Jufré); 1596, San Luis de la Punta de la Frontera (Luis Jufré); 1683, San Fernando del Valle de Catamarca (Fernando de Mendoza Mate de Luna); 1685, San Miguel de Tucumán (Fernando Mate de Luna). Corrientes marítimas: 1536, Buenos Aires (Pedro de Mendoza); 1573, Santa Fe de la Vera Cruz (Juan de Garay); 1580, Ciudad de la Santísima Trinidad y Puerto de Santa María de los Buenos Aires (Juan de Garay); 1585, Concepción de la Buena Esperanza del Bermejo; 1588, San Juan de Vera de las Siete Corrientes (Alonso de Vera y Aragón). Cúpula de la Catedral de Córdoba (Kronfuss 1920). Escudo tallado en piedra sapo de la Universidad de Córdoba, 1622.',
            'orden' => 13,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          11 => 
          array (
            'id' => 76,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000043.jpg',
            'titulo' => 'Página 43',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  La educación crece lentamente. Pedro Vega (1577) en Santa Fe, representa el primer maestro laico. A solicitud de Francisco de Victoria (1605), el Cabildo de Buenos Aires “le autoriza a dar enseñanza a los hijos de vecinos y moradores con la paga de -por cuenta de ellos- a razón de un peso los de leer, y los de escribir y contar a dos pesos.” Con el Colegio Máximo de los Jesuitas se inicia la educación secundaria: Colegio de Nuestra Señora de Monserrat, Córdoba, y en 1612 Fray Trejo y Sanabria también en Córdoba, funda de su peculio lo que sería la primera Universidad del país. Esta ciudad se transforma así en un activo centro de estudios en ciencias, filosofía e historia; paralelamente se establece en ella la primera imprenta del país. Comienzo de los primeros conjuntos de cámara (clave, flauta, violín). La Catedral Metropolitana cuenta ya con una orquesta con la excepcional cantidad de doce ejecutantes. La arquitectura entre empírica y mudéjar, se realiza (hasta aproximadamente 1810) con adobe, piedras, tiranterías, teja o paja. Mientras en los edificios corrientes aparece una configuración espontánea, en cambio, existe voluntad de diseño y exaltación en algunos edificios singulares; casas señoriales y especialmente conjuntos religiosos como los de San Ignacio en Misiones, Santa Catalina, San Isidro y Alta Gracia en la provincia de Córdoba, etc., debidos a arquitectos de órdenes religio- sas o ingenieros militares (Prímolo, Bianchi, Lemer, Krauss, Bernardez de Castro, Filcaya, Havelle, Ayroldi, Masella, etc.). Primeros sacerdotes músicos: Alonso Barzana y Francisco Solano. En las misiones jesuíticas (entre 1616-1726), trabajan maestros como los flamencos Luis Berger, Juan Vasco, Pedro Comental y especialmente el compositor y clavecinista italiano Doménico Zípoli. Se crea el primer conjunto de tres arpas, ocho violines, un fagot, tres liras, dos flautas, dos cornetas y cuatro bajos, fabricándose instrumentos musicales en las mismas misiones. 1767. Expulsión de la Orden Jesuita. 1776. Se crea el Virreinato del Río de la Plata. 1778. Juan José de Vértiz y Salcedo, criollo nacido en méxico, es el Virrey que introduce reformas liberales, haciendo llegar al país libros con los pensamientos ingleses y franceses de la época, que influirán en los forjadores de nuestra futura indepen- dencia. Funda un primer asentamiento humano en las costas patagónicas y poblaciones sobre el Río Negro, promociona la enseñan- za en Entre Ríos, Gualeguay, Gualeguaychú, Uruguay, etc. Crea el Colegio Real de San Carlos en Buenos Aires, el Tribunal de Protomedicato (que posteriormente dará origen a la primera escuela de medicina); traslada la imprenta desde Córdoba a Buenos Aires y crea numerosos y diversos institutos de beneficencia. 1780. El Virrey Vértiz nombra Protomédico al Dr. Miguel Gorman, llegado con la expedición de Pedro de Ceballos.',
            'orden' => 14,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          12 => 
          array (
            'id' => 77,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000044.jpg',
            'titulo' => 'Página 44',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Ambición e imaginación abren nuevos caminos mientras la vida urbana se hace tradicional y sin vuelo. Búsquedas de riquezas legendarias dan origen a la mitológica Ciudad del Dorado y la expedición del capi- tán Francisco César y sus hombres, a la leyenda de la Ciudad de los Césares. Carreras en pos de la supremacía de poseción entre los imperios español, portugués y los corsarios de otros im- perios (especialmente ingleses, france- ses y holandeses), enriquecieron el co- nocimiento y documentación sobre el territorio. La vida colonial: Dependencia política y económica de España y su monopo- lio; presiones por los intereses econó- micos de distintas potencias. España concede a Inglaterra, después del tra- tado de Utrecht, el asentamiento de venta de esclavos en “El Retiro”; asi- mismo, derecho para establecer resi- dencia en Buenos Aires, a súbditos in- gleses (origen de la formación de una comunidad que llegó a ser la más flo- reciente en el mundo). Igual suerte, en concesiones y restituciones derivadas de las tratativas diplomáticas en las Cortes europeas, signan el destino de los territorios de Malvinas y la Colonia del Sacramento. Un embrionario desarrollo cultural y una enseñanza pública casi exclusiva- mente concentrada en el seno de las casas patricias o en conventos religiosos, donde, para el conocimiento bastaban “las facultades de leer, contar y re- zar” (el alfabeto debía memorizarse,a partir de su primera letra Cristo, a, b, c...). A partir de Carlos III, se crea el 4° Virreinato de América hispana: el Río de la Plata, que de alguna manera, prefigu- ra la estructura geográfica de Argentina. Esta decisión, ini- cialmente transitoria, especialmente resuelta para oponer- se al avance del imperio portugués ya enclavado en Colonia del Sacramento, tiene también importantes implicancias económicas. Hasta ese momento la absurda vía legal por la que los productos europeos llegaban a Buenos Aires correspondía al siguiente itinerario: embarco exclusivo en el puerto de Cádiz; desembarco en Panamá y transporte a lomo de mula hasta el Pacífico; reembarco y transpor- te marítimo hacia El Callao - Lima; y transporte terrestre a través de la selva, cordilleras y pampas hasta Buenos Aires. Con el Virreinato y la apertura legal del puerto de Buenos Aires, la vida comienza a agilizarse y modernizarse tanto en su organización política y económica como cultural; la enseñanza, la imprenta, los libros y una creciente influen- cia y penetración de ideas y posiciones culturales, hasta en- tonces prohibidas. La misma actitud de España, que por odio a Inglaterra favo- rece la Revolución Norteamericana, alerta y prepara los espí- ritus de sus propios colonos para una futura emancipación. Difusión de la cultura del iluminismo y el enciclopedismo; ideas de Montesquieu, Voltaire, Rousseau. 25 DE MAYO DE 1810: Revolución de Mayo Cruce del Riachuelo (en las Invasiones Inglesas). Grabado anónimo de 1806, publicado en Londres en 1807.',
            'orden' => 15,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          13 => 
          array (
            'id' => 78,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000045.jpg',
            'titulo' => 'Página 45',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1781-1801. Félix de Azara traza el mapa de la América del Sur e inicia un relevamiento de la fauna en el Plata. 1799. Manuel Belgrano, secretario del Consulado, funda una Escuela de Arquitectura, Geometría y Perspectiva y jun- tamente con Cerviño, la escuela de Náutica y Matemática. 1754-1810. Manuel de Labardén crea el primer teatro por- teño “La Ranchería”, en la actual esquina de Perú y Alsina. Escribe su drama “Siripo”, en el cual retoma la historia de Lucía Miranda. 1801. Aparece “El Telégrafo Mercantil” primer periódico en nuestro país, fundado por Francisco Cabello y Mesa 1801. Se inaugura la Escuela de Medicina y Cirugía. 1802. Se edita “El Semanario de Agricultura, Industria y Comercio”, dirigido por Hipólito Vieytes. 1804. Inauguración del teatro Coliseo en Reconquista y Cangallo, donde se ejecutan tonadillas y zarzuelas. 1805. Antonio Machado es premiado por el Virrey Sobremonte por importar la vacuna antivariólica que será aplicada por el Dean Saturnino Segurola. 1806-1807. Invasiones inglesas. “The Times”, en Londres, publica una información sobre las tierras conquistadas: “... la sola naturaleza produce todo... un clima excepcionalmente favorable,... fertilísimas tierras cultivadas,... las praderas mantienen a millones de vacas, caballos, ovejas y cerdos,... hay tal abundancia de carne fresca,... que frecuentemente se la distribuye gratis entre los pobres,... no faltan lugares donde los buques puedan ingresar,... la pesca es muy productiva lo mismo que la caza,... algosón, lino y cáñamo, son cultivados en muchos dis- tritos,... no faltan algunas minas de oro, etc. Todas estas maravillas -relación anticipada del futuro granero del mundo-, concluye el estudio del diario inglés “... son parte ahora del dominio de Su Majestad”. La publicación es del 25 de septiembre de 1806, desconociendo que ya desde hacía más de un mes Buenos Aires había sido recuperada. Santiago de Liniers y Bremont es proclamado Virrey. 1810. Gobierno de la Primera Junta Patria. 1810. Aparece “El Correo de Comercio”, dirigido por Manuel Belgrano. Mariano Moreno edita “La Gaceta de Buenos Aires” cuyo acápite rezaba: “Rara felicidad de los tiempos en los que se puede sentir lo que se quiere y decir lo que se siente”. Se crea “La Filarmónica”, primera sociedad cultural dedicada a actividades musicales. Fundación de la Biblioteca Nacional.',
            'orden' => 16,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          14 => 
          array (
            'id' => 79,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000046.jpg',
            'titulo' => 'Página 46',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Guerras de Independencia Se inician las guerras de la Independencia con avance de ejér- citos simultáneos hacia los focos del poder español: corrientes al Alto Perú, al Paraguay, a la Banda Oriental y al Pacífico; Ejército de los Andes. Paralelamente, comienzan tratativas, conversaciones y estrategias tendien- tes a conciliar las voluntades de las dis- tintas provincias para la realización de un congreso en Tucumán. 9 DE JULIO DE 1816: Declaración de la Independencia. Calle de la catedral. Acuarela de C. Pellegrini. 1831.                  Facsímil del plano del Navío Negrero Bookes     El transporte de esclavos según un dibujo de Rugendas. Ante la decisión de la Asamblea del año XIII, los negros se integraron a la vida na- cional El Regimiento 8 de Infantería se integra al Ejército Sanmartiniano en Mendoza, donde recibe el nombre de Libertos. En forma onomatopéyica, las canciones popu- lares entre los negros, reflejan la alegría de su liberación: “Lingo, lingo, lingo, Linga, linga, linga, que ne tiela den balanco se cavó len dipotima. Compañelo di candombe Pita pano e bebe chiccha ya se le sijo que tienguemo no se puede se cativa. Ma luego ne solisonte lo sol melicano blilla alojado dese Oliente Len calena le mandinga”.',
            'orden' => 17,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          15 => 
          array (
            'id' => 80,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000047.jpg',
            'titulo' => 'Página 47',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1811. El Estado, en busca de símbolos e identidad, crea la Pirámide de Mayo, en la Plaza Mayor ordenando la colocación de una placa conmemorativa a las primeras víctimas de la Revolución (Comandante Pereyra de Lucena y Capitán Manuel Artigas), situación reiteradamente solicitada, que sólo se cumplió 80 años más tarde, 1891. La Asamblea del año XIII aprue- ba la bandera enarbolada por Belgrano en el río Juramento en 1812 y el diseño del escudo Nacional, que también Belgrano elaborara a partir del diseño de un sello de Juan de Dios Rivera. Se aprueba el Himno Nacional. 1811. Primer Reglamento de libertad de imprenta. 1811. El primer Triunvirato otorga la primera carta de ciudadanía a un comerciante inglés, venido de muy joven: Roberto Billinghurst, quien se incorpora inmediatamente a las luchas revolucionarias, como “ciudadano americano de las Provincias Unidas del Río de la Plata”. 1812. Se organiza la Logia Lautaro en Buenos Aires, a la cual se adhieren los más destacados civiles, militares y religiosos con un espíritu de abnegación y disciplina para obtener la consolidación de la democracia en América Latina. San Martín, figura protagónica de la misma, dotado de una profunda sagacidad política, al par que virtudes cívicas y militares, la extiende su- cesivamente a Mendoza, Chile y Perú. 1813. Censo de músicos en Buenos Aires (alrededor de 50 entre los que figuran Blas Parera, Juan Bautista Goiburu, Presbítero Juan Antonio Piscasarri, Fray Juan Moreno, etc.). 1813. La Asamblea General decreta la libertad de vientre; Argentina se constituye en uno de los primeros países en dar un paso decisivo contra la esclavitud y su abolición total. Los negros se integraron a la vida nacional al integrar el ejército sanmartiniano en Mendoza, como Regiminento 8 de Libertos. 1814. Se crea el Instituto de Medicina bajo la dirección (1815) de Cosme Argerich. Desde 1814 a 1834 se van estructurando 14 provincias que aún subsisten en el territorio argentino y se producen las segrega- ciones de las provincias de Alto Perú, Paraguay y Banda Oriental. Impulso en las distintas áreas de educación y perfeccionamiento: se crea en Buenos Aires la Facultad de Ciencias Médicas, 1813; Academia de Jurisprudencia, 1815; en el Convento de la Recolección (Recoleta), Francisco de Paula Castañeda, dos Academias de Dibujo, 1815; Academia de Matemáticas, 1816. Arriba, también en este año, el inglés Essex Vidal, dibujante y acuarelista, que documenta aspectos de Buenos Aires, editados en Londres en 1820. 1815. Residentes ingleses en Mendoza se manifiestan dispuestos a: “Tomar las armas y derramar hasta la última gota de su sangre en defensa de la Independencia”, ofreciendo al General San MArtín una compañía al mando del capitán Juan Young. Bartolomé Hidalgo (1788-1822) canta a la patria en sus “Cielitos”, con versos de contenido político a favor de la independen- cia e iniciando la literatura gauchesca.',
            'orden' => 18,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          16 => 
          array (
            'id' => 81,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000048.jpg',
            'titulo' => 'Página 48',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Afloran las guerras civiles y la anarquía Continuación de las guerras de la Independencia y comienzo de las guerras civiles (1817). Intereses encontrados entre centros y regiones del interior y el absolu- tismo y la absorción de la ciudad-puerto. La anarquía. Hordas resentidas e incul- tas acaudilladas por hombres de fuer- te personalidad. La guerra civil reina en todo el país des- trozando la unidad nacional. Las provin- cias se proclaman soberanas (Tucumán y Entre Ríos se declaran repúblicas). Las luchas interprovinciales se acentúan y ya no es exclusivamente entre porteños y provincianos, o federales y unitarios, sino, además, de cada uno de ellos entre sí. Presidencia constitucional de Rivadavia Escudos de Perú y Chile                             Las fragatas “La Argentina” y “Santa Rosa”, al mando de de Hipólito Bouchard, en la toma de Monterrey, California, 1818, óleo de Biggeri. Fragata “Heroína” al mando del coronel de marina David Jewet, toma po- sesión de Malvinas en nombre de nuestro gobierno, 1820, óleo de Biggeri.',
            'orden' => 19,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          17 => 
          array (
            'id' => 82,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000049.jpg',
            'titulo' => 'Página 49',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1817. En Tucumán, el Obispo Colombres, inicia la industria azucarera. Llega al país Amado Bonpland, ex intendente de la Emperatriz Josefina de Francia y compañero de Humboldt, radicándose en Corrientes, con plantas y semillas para adaptarlas a su cultivo. Posteriormente será Director del Museo de Historia Natural de Corrientes. 1817. Cruce de los Andes del Ejército de San Martín, liberando a Chile y Perú y otorgándole absoluta independencia sin vincularla con pactos económicos o políticos con Argentina. Como Protector de Perú, declara extinguida toda servidumbre de los indios. Juan Cruz Varela (que posteriormente será el poeta oficial de Rivadavia), con estilo neoclásico canta la acción de Maipú. Ante el páis ensangrentado en luchas civiles San Martín escribe: “... el genio del mal ha inspirado el delirio de la Federación... temo que cansados de la anarquía suspiréis al fin por la opresión y recibáis el yugo del primer aventurero feliz que se presente...” La misma estética neoclásica y romántica condicionará la arquitectura hasta, aproximadamente, la mitad del siglo que se penetra de influencias especialmente francesa e italiana, como voluntaria oposición al recuerdo de la dominación española. Los tejados se transforman en terrazas. Se construyen obras significativas como el pórtico de la Catedral de Buenos Aires y el Palacio San José en Entre Ríos; casas con planta romana, boticas, etc. Actúan arquitectos como Benoit, Catelín, Adams y posteriormente Pellegrini, Senillosa, etc. 1817 hasta 1819. El corsario Hipólito Bouchard da la vuelta al mundo con su fragata “La Argentina”, contribuyendo a prohi- bir el tráfico negrero en Madagascar, bloqueando las Islas Filipinas y hostigando a los españoles en el Pacífico al apoderarse del Fuerte de Monterrey (California U. S. A.); con él hace sus primeras armas el futuro Comandante Espora. 1820. La nave “La Heroína” arriba a la isla Soledad enviada por el gobierno de Martín Rodríguez, izándose por primera vez en Malvinas, el pabellón nacional el 6 de noviembre. Se reglamenta la caza de lobos marinos y ballenas y se autoriza la ex- plotación ganadera al hamburgués Luis Vernet, quien posteriormente se hará cargo de la Comandancia político-militar de las Islas. 1820. El Capitán Brandsen, uno de los antiguos oficiales del ejército francés (excluído del mismo después de la caída del Emperador Napoleón), presta sus servicios en las guerras de la Independencia hasta su muerte en Ituzaingó, al cargar contra un cuadro de soldados alemanes, al servicio de Brasil. Estanislao López, Facundo Quiroga, Artigas, Ibarra, Ramírez, Bustos, se afianzan fuertemente en sus respectivas regiones; disolución nacional. Cada provincia es un feudo, cada caudillo un reyezuelo que tiraniza a sus co-provincianos y hostiga al vecino. Muere Manuel Belgrano, exclamando, ante ese panorama nacional: “¡Ay, Patria mía!”. 1821. La Sociedad Literaria edita dos periódicos: “Argos” (bimensual) y “Abeja Argentina” (mensual), que reflejan los ade- lantos en la época de Martín Rodríguez y Rivadavia. 1821. Muere el General Martín de Güemes, intrépido conductor de guerrillas en los ejércitos del norte. 1821. Ley de Enfiteusis; creación de la Universidad y del Banco de Buenos Aires.',
            'orden' => 20,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          18 => 
          array (
            'id' => 83,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000050.jpg',
            'titulo' => 'Página 50',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION La vida se enriquece con el aporte de románticos y liberales. Con el regreso de Echeverría se difun- de el ideario del movimiento román- tico y del liberalismo político, que se expresa en la obra de escritores como Sarmiento, Alberdi, Gutiérrez, etc. y en pintores como Carlos Morel que introduce la rebelión romántica en la plástica argentina.       “El Regreso de la Cautiva”, óleo deMauricio Rugendas, 1838. El tema del malón interesó profundamente a Rugendas -quien admiraba enormemente a Echeverría- y realizó cuadros inspirados en “Rimas” donde el poeta románti- co incluía su famosa obra “La Cautiva”. “Peinetones en la calle”, litografía de César Bacle, satirizando la moda intro- ducida en Buenos Aires en 1823 por Don Pedro Masculino. Este pintor suizo con nacionalidad france- sa, se estableció en Buenos Aires en 1828; proyectó al- gunas obras de utilidad pú- blica, realizó una colección de Marcas de Ganado en la provincia de Buenos Aires, fundó varios periódicos dedicados a informaciones comerciales y marítimas. Ante la exigencia de hacerse ciudadano argentino, aban- dona el país y posterior- mente ofrece sus servicios a Bolivia y Chile, lo que mo- lesta a Rosas;por una confu- sa intriga de espionaje, se le mantiene preso durante casi un año, sin juicio; sólo ante las reiteradas instancias del Cónsul francés, es liberado con la salud destrozada y ya casi habiendo perdido la razón.',
            'orden' => 21,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          19 => 
          array (
            'id' => 84,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000051.jpg',
            'titulo' => 'Página 51',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1822. Instrucción primaria obligatoria; creación de la Escuela Normal y de la Bolsa Mercantil. 1822. José Antonio Piscarri iniciador de la música culta en Buenos Aires, funda una escuela de música y canto, en colabora- ción con Pedro Esnaola. Se inician los primeros conciertos de música argentina. 1822. Martín Rodríguez reglamenta la actividad médica y de farmacia con un criterio notablemente moderno. 1823-1824. Se crean la Casa de Huérfanos; Casa de Expósitos; Hospital de Mujeres; Asilo de Dementes. Se importa el primer toro “Shorton” y un plantel de ovejas “Merino”. Se crea el Museo Histórico Nacional y se declara la libertad de culto. Vacunación antivariólica obligatoria. 1825. Primera ópera en Buenos Aires: el Teatro Coliseo presenta “El Barbero de Sevilla” en estreno simultáneo con Nueva York. 1825. Un americano llamado Hallet comienza editando el semanario en inglés “Cosmopolite”; en 1826 aparece “The British Packet”; en 1861 aparece el primer diario inglés en América Latina “The Buenos Aires Standard” editado por Mulhall, quien a partir de 1863, edita un voluminoso “Handbook of The River Plate” (con sucesivas ediciones posteriores), que contiene me- ticulosa y precisa información sobre distintos aspectos de nuestro país. 1826. El “Adventure” y La “Beagle” realizan un “... relevamiento exacto de las costas meridionales de la península (sic) de Sudamérica, desde el Río de la Plata hasta Tierra del Fuego”. En 1832, La “Beagle” hace un segundo viaje trayendo a bordo al joven naturalista Darwin, quien recorre la Patagonia. 1826. El Imperio del Brasil declara la guerra, que concluye con la separación definitiva del Uruguay. El Almirante Guillermo Brown, primer jefe supremo de la Escuadra Argentina, triunfa en los combates navales de Juncal y Los Pozos. 1828. Llega a Buenos Aires, el ingeniero italo-francés Carlos Enrique Pellegrini, contratado para trabajos públicos; no obs- tante, hará fama como retratista de la sociedad porteña. Llega también el suizo César Hipólito Bacle. Alcides D´Orbigny publica la relación de sus viajes en nuestro territorio. 1830. Primer Gobierno de Rosas. Publicación de “La Cautiva” de Esteban Echeverría. 1833. Expedición al desierto dirigida por Juan Manuel de Rosas. Acampa en el Río Colorado, explorando hasta el comienzo de la cordillera y ocupa la isla de Choele-Choel, en el Río Negro.',
            'orden' => 22,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          20 => 
          array (
            'id' => 85,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000052.jpg',
            'titulo' => 'Página 52',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Hegemonía de Rosas. Se acentúan las tensiones internas y el acoso externo La confusa situación interna se ve agra- vada por guerras exteriores que encu- bren distintas ambiciones de posesión, hegemonía, problemas de límites, etc., campañas militares, combates navales, bloqueos que desangran el país y su economía. El gobierno despótico de Rosas gene- ra la reacción de opositores y exiliados culminaba con la Campaña del Ejército Grande, que conduce el Gral. Urquiza. SANTA	FE,	1853,	CONGRESO GENERAL CONSTITUYENTE “Manuela Rosas de Terrero”, óleo de Prilidiano Pueyrredón, 1850. Museo Nacional de Bellas Artes. “Ajusticiamiento de los Reynafé y de Santos Pérez”, litografía de H. Bacle, 1838. La lámi- na dice: “Vicente Guillermo Reynafé y Santos Pérez, principales autores del asesinato come- tido contra la persona del Sr. Gral. Don Juan Facundo Quiroga ysu comitiva”. Ilustra el ajusticiamiento frente al Cabildo, que se utilizó como paredón, mientras la multitud se aprieta para mirarlos y Rosas impávido, llega a caba- llo a contemplar la escena.                   Después de la victoria de Caseros, Buenos Aires recibe delirante a Urquiza, quien enca- beza la columna de guerreros; desde los balco- nes de las casas ricas le saludan los proscrip- tos como Vicente Fidel López, Eguía, Moreno, Pillado, etc. (pintura de L. Matthis).',
            'orden' => 23,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:18',
            'updated_at' => '2019-01-12 19:01:18',
          ),
          21 => 
          array (
            'id' => 86,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000053.jpg',
            'titulo' => 'Página 53',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1833. Se crea en Buenos Aires la “Sociedad Popular Restauradora” -la temible “Mazorca”- cuya violencia se ejercera impunemente en el segundo gobierno de Rosas como Restaurador de las Leyes, con la suma del poder pú- blico. 1833. Gran Bretaña se apodera de las Malvinas. Continúan las luchas de caudillos y Quiroga es asesinado en Barranca Yaco. 1835. Rosas llama nuevamente a la Orden jesuita, a la que expulsa posteriormente, por no permitir ésta, colgar su re- trato en las iglesias. Los jesuitas solo regresarán en 1862. 1836-1839. Perú y Bolivia invaden el territorio llegando hasta Tucumán. 1837. Esteban Echeverría funda “La Asociación de Mayo” con jóvenes que, dejando de lado trabajos literarios y fi- losóficos, aspiran a un movimiento destinado a superar la discordia entre unitarios y federales, sismáticos y apostóli- cos, porteños y provincianos, ciudadanos de frac y gauchos de chiripá, que escinde la sociedad argentina. Adscriben a ella figuras de la talla de Sarmiento, 1838. Se publica “El Dogma Socialista” de Esteban Echeverría. 1838. Bloqueo anglo francés hasta 1848; combate de la Vuelta de Obligado, 1845. 1839. Sarmiento, implacable opositor de Rosas, funda el periódico “El Zonda”. 1839-1843. Uruguay declara la guerra. 1843. Chile ocupa el esttrecho de Magallanes y funda Punta Arenas. 1845. Aparece “Facundo: Civilización y Barbarie” (Sarmiento). 1847. Se aplica en Buenos Aires el éter como agente anestésico (Dres. Tuksburry y Aubain), usado muy pocos meses antes, por primera vez en el mundo. 1848. Inauguración del Teatro de la Victoria, que trae continuamente artistas de Europa. 1848. Luis Piedra Buena, primer marino argentino que cruza el Círculo Polar Antártico. 1849. Inglaterra devuelve Martín García. 1852. Caseros: caída de Rosas. El general Urquiza reúne en la ciudad de San Nicolás de los Arroyos a todos los goberna- dores, a fin de formular un acuerdo como base de la organización nacional, a través de un congreso con dos diputados por cada estado, la abolición de las aduanas provinciales, la libre navegación de los ríos.',
            'orden' => 24,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          22 => 
          array (
            'id' => 87,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000054.jpg',
            'titulo' => 'Página 54',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION La constitución del país y el renacimiento de las desavenencias Redacción de nuestra Carta Magna, cuyo espíritu reconoce entre otras, la in- fluencia de la Constitución de Filadelfia, de 1778; el proyecto incluido en “Las Bases”	de	Alberdi;	la	Constitución Suiza de 1844; las resoluciones de la Asamblea de 1813; las constituciones unitarias de 1819 y 1826; los pactos fe- derales de 1822 y 1823 y las actas de la Asociación de Mayo. Su redactor fue Juan María Gutiérrez y uno de los conceptos esenciales de esta carta constitutiva es organizar una estructura que haga tan imposible la anarquía como el despotismo, males entre otros los que se había debatido la República, cuyo primer nombre his- tórico es el de Provincias Unidas del Río de la Plata. JUSTO JOSE DE URQUIZA, Primer Presidente Constitucional, 1854 Paraná capital provisoria de la Confederación Argentina. Vélez Sarsfield escribe desde “El Nacional”, Mitre desde “Los debates”, calien- tan el ambiente contra Urquiza; se rechaza el Tratado de San Nicolás. El deseo de preeminencia  porte- ño se revela y Buenos Aires se sepa- ra de la Confederación (hasta 1860). Mitre propone a Buenos Aires como “La República del Río de la Plata”. Continúan las tensiones hasta el ale- jamiento de Urquiza después de la de- rrota de Pavón. Hegemonía porteña. Plaza Victoria, el viejo teatro Colón visto desde el Cabildo. Residencia de Urquiza, Palacio San José en Entre Ríos.',
            'orden' => 25,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          23 => 
          array (
            'id' => 88,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000055.jpg',
            'titulo' => 'Página 55',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1852. Urquiza queda como Director Provisorio, encargado de las relaciones exteriores y de la convocatoria inmediata al Congreso Nacional Constituyente. 1853. Se sanciona en Santa Fe la Constitución Argentina, vigente hasta su modificación en 1994. Después de Caseros, al suprimirse la censura impuesta por Rosas, la libertad de imprenta hace aparecer casi simultáneamente numerosas publicaciones: “El Progreso”, “Los Debates”, “El Nacional”, “La Avispa”, “La Tribuna”, “La Camelia”. Se contratan científicos europeos de nombradía, naturalistas, médicos, ingenieros, economistas: Burmeister, Mantegazza, Page, Bravard, Campbell. Prilidiano Pueyrredón, ingeniero, retratista, paisajista y costumbrista, desarrolla una importante obra pictórica academicista, al par que obras de arquitectura: reconstruye sobre la vieja estructura de 1811, la Pirámide de Mayo que, en 1912, será tras- ladada sobre rieles a su emplazamiento actual. Los profesionales de esta época hasta aproximadamente la década del ´80, entre ellos Pellegrini, Taylor, Fossatti, Benoit, etc. concilian el academicismo con algunos “Revivals”, según las influencias ita- liana, inglesa, alemana, etc.; aparecen estructuras en hierro fundido, refabricación en equipamiento de ferrocarriles, impor- tación de mármoles y otros materiales los que trabajados por excelentes obreros y artesanos inmigrantes (en su mayoría ita- lianos, suizos, etc.) caracterizan la arquitectura de este período. Entre los edificios significativos se construyen el Observatorio Astronómico de Córdoba, la Aduana de Rosario, la Casa Rosada, diversas residencias importantes en Buenos Aires y grandes tiendas. Se radica en Buenos Aires Juan León Palière, pintor y dibujante brasileño, quien fija en su trabajo tipos y costumbres nacionales. 1856. Calfucurá, jefe de la confederación indígena (araucanos, pampas, etc.), establecida en Salinas Grandes, saquea el for- tín de Azul y continúa hostigando las fronteras hasta 1872, año en que es vencido y muere. 1857. Inauguración del primitivo Teatro Colón, con la representación de la ópera “La Traviata”, edificio ubicado en el actual emplazamiento del Banco de la Nación. 1857. La Sociedad del Camino de Hierro de Buenos Aires al Oeste, luego de fuertes contrariedades, logra instalar la prime- ra línea férrea en la Argentina. La ley autorizaba el arrastre del convoy por caballos, pero se adquirió una locomotora: “La Porteña”; Dalmacio Vélez Sarsfield fue el primer pasajero desde la actual Plaza Lavalle hasta la Floresta. 1859. Urquiza vence en la batalla de Cepeda. 1862-1867. Presidencia de Mitre.',
            'orden' => 26,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          24 => 
          array (
            'id' => 89,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000056.jpg',
            'titulo' => 'Página 56',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Corrientes migratorias y estructuración social. Organización	nacional.	Grandes	co- rrientes colonizadoras; europeización del país. Consolidación de una clase alta, cuyo poder se asienta, principal- mente en el latifundio y en la explota- ción ganadera. Continúan corrientes inmigratorias euro- peas y se inician planes de colonización en mayor escala, especialmente Entre Ríos y Santa Fé (Colonia Esperanza); 1861, los galeses se establecen en Rawson. El modelo, liberal. Expansión econó- mica, política de atracción de capitales e inmigrantes europeos; dependencia del mercado externo especialmente británico	(ferrocarriles,	carne,	etc.). Incorporación de nuevas tierras para el auge de la agricultura y ganadería. “Vista de Curuzú, mi- rada aguas arriba de norte a sur”. Oleo de Cándido López sobre apuntes tomados dos días antes de la batalla de Curupaití en cuya acción perdería la mano derecha.                  “Martín Fierro”, dibujo de Castagnino para la colección de EUDEBA, 1962.',
            'orden' => 27,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          25 => 
          array (
            'id' => 90,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000057.jpg',
            'titulo' => 'Página 57',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1865. Guerra del Paraguay; Cándido López, retratrista porteño se enrola para documentar los hechos de la Guerra de la Triple Alianza. En el frente sufre la amputación del brazo derecho, no obstante lo cual, educando su mano izquierda, cumplimenta su trabajo. 1866. Se publica “El Fausto” de Estanislao del Campo. 1867. Jóvenes británicos, residentes en Argentina, con dirección de Tomas Hogg fundan el “Buenos Aires Football Club”. 1867. Aparece el primer diario de venta callejera “La República”. 1868-1874. Presidencia de Sarmiento: “Democracia es educación”. Censo nacional; fundación de Escuelas Normales en Paraná y Tucumán; Colegios Nacionales en Jujuy, Santiago del Estero, San Luis, Santa Fe y Corrientes; enseñanza práctica en los colegios relacionada con la industria de las provincias, para hacer- la aplicable a las necesidades ordinarias de la vida; minería en Catamarca, San Juan, etc.; escuelas de Agricultura; escuelas Nocturnas para obreros; escuelas Ambulantes para sitios alejados, etc.; contratación de 65 maestras especializadas norteame- ricanas y de sabios alemanes para el Observatorio Astronómico que funda en Córdoba; creación de la Sociedad Científica Argentina y la Comisión de Bibliotecas Populares; del Telégrafo Trasandino y del Cable Telegráfico Transatlántico (Dirección H. Davidson), que permitirá la comunicación de nuestro país con Europa: inmigración y colonización. 1869. Código Civil (Dr. Dalmacio Vélez Sarsfield). Se funda el diario “La Prensa” y en 1870, “La Nación”. 1870. Se publica “Una Excursión a los Indios Ranqueles” de Lucio V. Mansilla. 1871. Epidemia de fiebre amarilla en Buenos Aires; desempeño abnegado del Dr. Roque Pérez. 1872. Inauguración del Teatro de la Opera; espectáculos líricos. Hilario Ascasubi publica “Santos Vega” y culmina la tradición de la poesía gauchesca con la aparición del poema básico de nuestra literatura: “Martín Fierro” de José Hernández. 1873. En la ciudad de Colon, provincia de Buenos Aires, se crea el primer Registro Civil del país. 1874. Se juega el primer partido de rugby en nuestro medio. Creación de la primera escuela de Música y Declamación di- rigida por Nicolás Bassi, dedicada a la formación de músicos e instrumentistas. Se crea, también la “Sociedad del Cuarteto” (1875), con el objeto de difundir música de cámara, clásica y romántica. Los músicos argentinos que siguen a los precursores: Amancio Alcorta (1805-1862); Juan Bautista Alberdi (1810-1884); Juan Pedro Esnaola (1808-1878); Federico Espinosa, Arturo Berutti, etc. La primera ópera argentina: “La Gata Blanca” de Francisco Hargraves, se estrena en 1877. 1877. Primera exportación de ganado en pie.',
            'orden' => 28,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          26 => 
          array (
            'id' => 91,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000058.jpg',
            'titulo' => 'Página 58',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Buenos Aires construye su grandeza. En 1885, Buenos Aires es la primera ciudad al sur del Ecuador con una den- sidad de población casi igual a la de Londres. Es la única ciudad del mun- do donde se editan diarios en cinco idiomas diferentes; la circulación del conjunto de 25 publicaciones diarias da una relación entre el número de ejemplares y habitantes que duplica a la del Reino Unido y es tres veces su- perior a la de Estados Unidos. El creci- miento de la riqueza en Buenos Aires (ciudad y provincia desde 1864 a 1883), da un promedio de acumulación por habitantes al año de £ 11 contra £ 5 en Gran Bretaña y Francia, £ 6 en U. S. A. y £ 9 en Australia. cio por habitante en Argentina está a la par con Francia y muy superior a la de Estados Unidos, Alemania, Italia o España. Si bien es cierto que la esqui- la del ganado rinde menos que la de Australia, es el país que posee el mayor número de cabezas de ganado ovino en el mundo. Ningún otro país sudamericano invier- te tanto dinero en educación. Comienzo de la industrialización, de- sarrollo del proletariado; nueva clase media en ascenso. Proceso de asimila- ción del inmigrante y su descendencia que hace del país un excepcional caso de integración y convivencia. La espi- rante burguesía media surgida con el aporte inmigratorio encuentra su lu- gar en la sociedad y en el acceso al poder. Se acentúa el centralismo porteño y el estancamiento del interior. “Banda Lisa”, óleo de Angel Della Valle. Plaza Pellegrini, pintu- ra de L. Matthis. Ha sonado la sirena de La Prensa”, dibujo de Acquarone.',
            'orden' => 29,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          27 => 
          array (
            'id' => 92,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000059.jpg',
            'titulo' => 'Página 59',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1879. Campaña del Desierto del General Roca, que concluye, según su mensa- je de 1885, después de someter a los caciques Pincén, Catriel, Namuncurá y otros, diciendo: “Saihüeque, el último de los caciques del sur, Soberano de las Manzanas, que aún andaba huyendo con su tribu por las nacientes del Chubut, acaba de so- meterse con 3.000 indios.” 1880. Carlos Tejedor encabeza la revolución que es sofocada y el presidente Nicolás Avellaneda, dando fin a un largo conflicto, remite al Congreso su proyecto de ley: “Buenos Aires capital, es una imposición de la historia”. Primera presidencia de Roca. En 1882, primer frigorífico argentino en San Nicolás. Comienza la práctica nacional de fútbol asimilado a los colegios. Se crea el Consejo Nacional de Educación; Ley 1420: educación laica, libre y obligatoria de la enseñanza primaria en las escuelas públicas. 1884. El 25 de mayo, Argentina estableció el primer faro en la Isla de los Estados. San Juan del Salvamento, actual monumento histórico, cuyo destino inmortalizara Julio Verne, en su novela “El Faro del Fin del Mundo”. Auge de la producción literaria de la generación del ´80: “La Gran Aldea”, de Vicente Fidel López; “Juvenilla”, de Miguel Cané; “La Bolsa”, de Martel; “Mis Montañas”, de González, etc. Primer teatro estable popular en el circo de los Podestá. Auge del tango-danza como expresión de una subcultura ciudadana. 1886. Fernández Davel procesa en su casa y aplica la vacuna antirrábica, segundo “Laboratorio” en el mundo en la produc- ción del descubrimiento de Pasteur (1885). Acorde con las aspiraciones de una sociedad floreciente (1890-1914), se requieren edificios públicos suntuosos, residencias, teatros, grandes tiendas, parques y paseos. Modelo liberal, historicismo, con influencias francesa o inglesa, “art noveau”. Las obras son realizadas por profesionales europeos y algunos argentinos educados en el Viejo Mundo (Christophersen, Dormal, Thays, Chamber, Walker, Tamburini, Buschiazzo, etc.), que además, importan materiales especiales: pizarras, mármoles, balaustres, yeserías, bronces, maderas. Como edificios significativos: Palacio del Congreso, Casa “Harrods”; actual Teatro Colón; Hotel “Plaza”; “La Prensa”; Catedral de La Plata, etc.; paralelamente, con las grandes residencias y parques, aparece una arquitectura industrial en bodegas, in- genios, clubes, etc. Como contrapartida: proliferan los “conventillos”. 1888. Se funda en Hurlingham el primer equipo de polo. 1889. Malatesta, dirigente italiano impulsa el movimiento anarquista. La obra del paleontólogo Florentino Ameghino, “Contribución al Conocimiento de los Mamíferos Fósiles en la República Argentina”, es premiada en la Exposición Internacional de París.',
            'orden' => 30,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          28 => 
          array (
            'id' => 93,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000060.jpg',
            'titulo' => 'Página 60',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Fines del Siglo XIX, comienzos del XX. Las tensiones políticas marcan la evolución del país. La Organización Nacional presenta distintos y constantes episodios de lucha en torno a la hegemonía polí- tica: alianzas, pactos, separaciones y esciciones de partido: liberal, en au- tonomistas y nacionalistas (crudos y cocidos). Acuerdos como la Coalición; influencias y presiones para la suce- sión de poder; insurreción (Mitre 1874, Tejedor 1880). SIGLO XX “Sin Pan y Sin Trabajo” (detalle), óleo de Ernesto de la Cárcova, 1894. ',
            'orden' => 31,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          29 => 
          array (
            'id' => 94,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000061.jpg',
            'titulo' => 'Página 61',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1889. “Meeting” en el jardín de la Florida; se reúnen jóvenes opositores al “Sistema” (Aristóbulo del Valle, Leandro N. Alem, Bernardo de Irigoyen, Vicente Fidel López, etc.) y se organiza la Unión Cívica de la Juventud con el propósito de “... ejercitar los derechos políticos de los ciudadanos... con entera independencia de las autoridades constituidas y provocar el desperta- miento de la vida cívica nacional”. Posteriormente, partido de la Unión Cívica, liderado por Alem y otros. 1890. Revolución de las Boinas Blancas. 1890. Julián Aguirre y Alberto Williams fundan el Conservatorio de Música de Buenos Aires. 1892. Cuestiones de límites con Chile; actuación del naturalista y explorador Francisco P. Moreno. 1894. Aparece “La Vanguardia”, periódico socialista, científico, defensor de la clase trabajadora con Juan B. Justo, José Ingenieros, Repetto, y otros. Pintores nacionales, en su mayoría con perfeccionamiento en el Viejo Mundo, expresan la estética del naturalismo: Sívori, Della Valle, Schiaffino, De la Cárcova (su obra “Sin Pan y Sin Trabajo” puede considerarse la culminación del Naturalismo Descriptivo). Posteriormente, con el retorno de Malharro, se introduce la estética del Impresionismo. La actividad escultórica de orientación neoclásica comienza con Lucio Correa Morales (1852-1923: “La Cautiva”, “Los Onas”, etc.), Francisco Cafferata (1861-1890: “Monumento a Brown”), Alberto Lagos, etc. Introducción en las letras del Modernismo con la llegada de Rubén Darío. 1896. A 8 meses de la presentación del cinematógrafo de los hermanos Lumiére en París, se ofrece en Buenos Aires el primer espectáculo público cinematográfico. 1897. El profesor Costa comienza a enseñar radiología y el Doctor Varzi fabrica el primer equipo que funcioníó en el país, a sólo 2 años de que Rontgen descubriera los Rayos X. 1900-1906. Se fundan los grandes clubes de fútbol nacional: Boca Juniors, River Plate, Racing, Ferrocarril Oeste. En 1904 se funda el primer equipo de rugby argentino. 1902. Fallece Alejandro Posadas, creador de la Escuela Quirúrgica Argentina, quien usa por primera vez en el país, el cine como recurso docente. 1902. Relacionado al ataque de las flotas alemanas, inglesas e italianas a Venezuela, para exigir indemnización y pago de una deuda, Luis María Drago asienta la doctrina jurídica internacional, según la cual: “No se puede exigir compulsivamente a una nación, mediante el uso de la fuerza, el pago de su deuda.” ',
            'orden' => 32,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          30 => 
          array (
            'id' => 95,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000062.jpg',
            'titulo' => 'Página 62',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Crecimiento y progreso Un segundo momento de la Organización Nacional se inicia aproxi- madamente a partir de la presiden- cia de Roca, caracterizado por luchas entre “El régimen” y la creciente e intransigente oposición de la Unión Cívica, la que a su vez también sufre la escición entre Unión Cívica Radical y la Unión Cívica Nacional (personalistas y antipersonalistas). La perforadora buscando agua en las vecinda- des de Comodoro Rivadavia. “Corbeta Uruguay en la Antártida”, óleo de Biggeri. Al mando del teniente de navío Julián Irizíbar procedieron al rescate de los miembros de la expedición NORDENSKJÖLD en 1903, en la cual, ade- más, prestaba servicios el entonces Alférez Sobral. Los nuevos subte- rráneos en Buenos Aires en 1918. El traslado de la Pirámide de Mayo a su emplazamiento actual (12-11-1912).',
            'orden' => 33,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          31 => 
          array (
            'id' => 96,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000063.jpg',
            'titulo' => 'Página 63',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1903. La corbeta argentina “Uruguay” logra salvar a la expedición sueca de Otto Nordenskjöld; entre los rescatados estaba el representante ar- gentino en la Antártida desde 1901, alférez Sobral. Argentina es el único país que mantiene desde 1904 una base antártica permanente en la Isla Laurie y en el Archipiélago de las Orcadas del Sur, donde en 1905, instala un Observatorio Astronómico. 1905. Ley Láinez sobre educación tratando de equilibrar la enseñanza im- partida en zonas urbanas y territorios alejados de esos centros. Edición de “La Guerra Gaucha” de Leopoldo Lugones. 1907. Descubrimiento de petróleo en Comodoro Rivadavia. 1908. Se inaugura el actual Teatro Colón con la representación de la ópera “Aída” de Verdi. Posteriormente se presentará la ópera argentina “Aurora” de Héctor Panizza. Se construye en Retiro la Central Ferroviaria. 1909. Unión de las galerías en el túnel trasandino con Chile. 1910. Celebración del Centenario, con la presencia de destacadas personalidades de todo el mundo: Infanta Isabel de Borbón, George Clemenceau, Guillermo Marconi, etc.; Rubén Darío publica “El Canto a la Argentina”. La ciudad se enjoya con monumentos ofrecidos por las distintas colectividades. Desde el comienzo del siglo se intensifican las construcciones y adelantos técnicos: puerto Madero en Buenos Aires, puerto Nuevo, puertos en Rosario y La Plata, agua corriente y cloacas. Se importa el primer automóvil a vapor, circulan el primer tran- vía eléctrico (1897) y el primer subterráneo a Plaza Once (1913), se instala el Observatorio Astronómico en las Orcadas, etc. El cine argentino con “El Fusilamiento de Dorrego” produce en 1911 su primer film argumental. 1912. Se promulga la Ley Roque Sáenz Peña: “Aspiro a que las minorías están representadas y ampliamente garantizadas en la integridad de sus derechos”. Voto secreto y obligatorio y formación de auténticos padrones electorales. 1913. Muere Eduardo Wilde, médico, psicólogo y político que propugnó la frenicectomía (técnica quirúrgica), varias décadas antes que en Europa. 1913. José Ingenieros publica “El Hombre Mediocre”. Fundación de la Asociación Wagneriana de Buenos Aires.Grupo Nexus (1907-1913 aprox.). Pintores como Fader, Colivadino, Ripamonte, Quirós, Alice, Cordiviola, Quinquela Martín y otros, partici- pan en los primeros salones nacionales. Estética impresionista y postimpresionista.',
            'orden' => 34,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          32 => 
          array (
            'id' => 97,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000064.jpg',
            'titulo' => 'Página 64',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Tiempo de protestas y agitación social Reclamaciones obreras, alentadas no sólo por el socialismo sino por las in- fluencias exteriores del malatestismo y el anarquismo, la Revolución Rusa y la agitación social internacional, agravan y agudizan en nuestro medio las ten- siones con levantamientos (Revolución del 90, de 1893, de 1905) culminan- do en su expresión más aguda en la Semana Trágica de 1919 y en los acon- tecimientos que suceden desde 1920 hasta 1922, con los tristes y sangrien- tos episodios patagónicos, hasta cul- minar con la Revolución de 1930. El período presenta dos caras antagó- nicas: por una parte, te, desde el gobierno hacen obras de significativo aliento y proyección para el futuro del país; por la otra, la apari- ción exaltada del caudillo, como hom- bre providencial que pone en escena una gran masa popular marginada de la vida cívica, con un sentido de reno- vación. Rusia en Buenos Aires”, el epígrafe dice: “Los de Rosendo Trepoff, interviniendo en el mitin de la plaza Lavallof”. De la revista PBT, 1905. Manifestaciones populares el 1º de mayo de 1909. Acometida policial a los manifestantes, entre ellos, la porta estandarte de la agrupación feminista. “Canto al Trabajo” grupo escultórico, bronce de Rogelio Yrurtia.',
            'orden' => 35,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          33 => 
          array (
            'id' => 98,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000065.jpg',
            'titulo' => 'Página 65',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  La escultura, especialmente en bronce y piedra, tie - ne sus representantes en Rogelio Yrurtia (1879 -1950 “Canto al Trabajo”, 1907, “Monumento a Dorrego”, etc.); José Fioravanti (1986 -1972 “Monumento a la Bandera” en Rosario); Alfredo Bigatti (Monumentos a Mitre y Roca); Lola Mora (Fuente de las Nereidas); Arturo Dresco (Monumento a España, Costanera Sur); Agustín Riganelli, etc. Continúa la corriente inmigratoria y de la colonización. La Constitución dispone el fomento de la colonización euro- pea, aunque nada impida fomentar otras corrientes. Desde la Ley Avellaneda, 1876, el flujo de colonos se incrementa constantemente. 1914. Se realiza el Censo Nacional, población aproxima- da: 8.000.000. En la capital: 1.584.106, de los cuales hay 797.969 argentinos y 786.137 extranjeros. El profesor Luis Agote realiza la primera transfusión de sangre conservada hecha incoagulable, luego, método de utilización universal. También en este año, Juan Bucetich crea el sistema de identificación dactiloscópica mediante impresiones digitales, adoptado universalmente. Muere Pedro Lagleyze, proto-oftalmólogo, creador de técnicas quirúrgicas originales que tuvieron amplia repercusión inter- nacional. 1915. Fundación de la Sociedad Nacional de Música, hoy Asociación Nacional de Compositores. 1916. Primera presidencia de Yrigoyen. 1917. Sucesión de hasta 80 huelgas obreras que continúan incrementándose hasta la huelga general de 1919, reprimida drás- ticamente: La Semana Trágica. 1918. Muere el Dr. Abel Ayerza quien describió por primera vez en la historia la enfermedad de los “cardíacos negros”, una variedad de corazón pulmonar crónico 1918. Reforma Universitaria en Córdoba. Posición de una elevada ética Argentina en política internacional: “... tratándose de una sociedad destinada a defender la paz futura entre las naciones, no cabe distingo de beligerantes y neutrales, ni de países grandes o pequeños, entre los que forman parte de ellas...” La victoria no da derechos; Argentina se retira de la Liga de las Naciones. Estreno de le ópera “Tucumán” de Felipe Boero, con primer libreto en castellano. 1920-1929. Transradio Internacional permite la comunicación directa con todo el mundo. 1920-1922. Patagonia entra en la conciencia popular nacional a través de sangrientas convulsiones sociales, donde reclamacio- nes obreras se mezclan con actividades de agitadores rusos y bandolerismo. Huelgas, saqueos, incendios, drásticas represiones y fusilamientos. 1922-1928. Presidencia de Alvear. Argentina “Granero del Mundo”. Nuestro crédito exterior es sólo comparable al de EE. UU., Inglaterra y Francia.',
            'orden' => 36,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          34 => 
          array (
            'id' => 99,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000066.jpg',
            'titulo' => 'Página 66',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Continúa la evolución incluyendo atisbos de industrialización                  Llegada del Plus Ultra a Buenos Aires volando sobre el Yatch Club Mientras una belga ingresa al país en busca de una vida mejor, un pequeño ex emigrante fran- cés, regresa a París, ya mimetizado en gaucho,   en busca de triunfo. ',
            'orden' => 37,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          35 => 
          array (
            'id' => 100,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000067.jpg',
            'titulo' => 'Página 67',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1922. El equipo argentino de polo gana la copa mundial en Inglaterra; en 1924 campeón Olímpico. Lilian Harrison cruza a nado el Río de la Plata (1923). 1923. El profesor P. Escudero emplea por primera vez en el país la insulina en el tratamiento del enfermo diabético. 1924. Fundación del “Conservatorio Nacional de Música y Arte Escénico”, actual Carlos López Buchardo. 1924. Regreso de Petorutti y Xul Solar. Irrupción en nuestro medio de la Vanguardia y los nuevos artistas. Inauguración de la Asociación Amigos del Arte; Basaldúa, Butler, Spilimbergo, Forner, etc. 1925. Se organiza la Orquesta, Coro y Cuerpo de Baile del Teatro Colón. Einstein visita la Argentina. Se construye la destilería del Plata. Se crean los Yacimientos Petrolíferos Fiscales, bajo la dirección del coronel Mosconi. 1926. Desde Palos de Moguer a Buenos Aires: el hidroplano “Plus Ultra” vence al Océano Atlántico desde el aire. En arquitectura (aproximadamente desde 1914 hasta 1943): eclecticismo, “Art Noveau”, “Art Decó”, renacimiento hispa- noamericano, racionalismo, Incorporación del uso del hormigón armado (cuyo exponente más importante es el edificio “Kavanagh”); producción de materiales nacionales. Edificios significativos: Barolo, Banco de Boston, Teatro Cervantes, Cine GRan Rex, el Obelisco. El rascacielos y las casas de departamentos; establecimientos rurales e industriales y barrios obreros; estadios, etc. (Arquitectos: Bustillo, Kronfuss, Prebisch, Villar, Dourge, Virasoro, etc.). 1927. Se inaugura la Fábrica Militar de Aviones, en Córdoba. Paro general en solidaridad con Sacco y Vanzetti. En Mendoza, Usina Hidroeléctrica de Cacheuta. 1928. Gardel es el Rey del Tango en París. 1929. Regreso de Aime Tchiffely con dos caballos criollos, Mancha y Gato, tras su exitoso raid Buenos Aires - Nueva York. Visita de Le Corbusier y el conde Keyserling. Además de su diagnóstico sobre la tristeza de los argentinos, el filósofo expresa que nuestra vida colectiva se funda en bases sentimentales, que nuestro porvenir es claro y que somos herederos de la cul- tura humanista occidental. ',
            'orden' => 38,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          36 => 
          array (
            'id' => 101,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000068.jpg',
            'titulo' => 'Página 68',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Crisis política y evolución en la primera mitad del siglo XX El país participa de la gran crisis mun- dial. Se vive la llamada “Década Infame” Buenos Aires espera la llegada del LEGH II y su navegante solitario. Revolución de 1930; el pueblo porteño en Plaza de Mayo, con motivo de la asunción del Gral. Uriburu después del derrocamiento de Irigoyen.',
            'orden' => 39,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          37 => 
          array (
            'id' => 102,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000069.jpg',
            'titulo' => 'Página 69',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1927-1929. Retorno de Guttero; taller con Bigatti, Forner y Domínguez Neira; el nuevo salón incluye a Victorica, Cúnsolo, Gómez Cornet, Del Prete, etc. 1930. Derrocamiento de Yrigoyen; Uriburu primer presidente de facto del siglo XX. Nace el primer teatro independiente de Buenos Aires. Victoria Ocampo dirige el grupo y la revista “Sur”, que traerán al país el aporte de excepcionales creadores. 1932. Vito Dumas, navegante solitario en el “Legh”, viaja alrededor del mundo en cuatro meses de navegación. Juan Carlos Zavala vence la maratón de los Juegos Olímpicos de Los Angeles. El equipo argentino de polo obtiene la “Copa de las Américas”, título que defenderá hasta 1979. 1933. Martínez Estrada publica “La Radiografía de la Pampa”. Lanzamiento de la pintura argentina al exterior en la misión del presidente Justo a Brasil. Presidencia de Justo: se establecen las Juntas: Nacional de Carnes, Reguladora de Granos, de la Industria Lechera y la Reguladora de Vinos; se crea la Dirección Nacional de Vialidad y se construye una amplia red caminera en todo el país; dis- tintas mejoras del tipo social (indemnización por despido, vacaciones pagas, “sábado inglés”, etc.). 1935. Borges publica “La Historia Universal de la Infamia”. Su producción continuará hasta el presente con obras que lo cons- tituyen en una de las figuras más importantes de la literatura mundial. Las corrientes literarias de “Florida” y “Boedo” incorporan autores de importante producción, como Mallea, Girondo, Macedonio Fernández, Horacio Quiroga, Leopoldo Marechal, Alfonsina Storni, etc. 1936. El Canciller Saavedra Lamas obtiene el Premio Nobel de la Paz por sus gestiones en relación con la Guerra del Chaco. Monseñor Copello, primer cardenal argentino. Se inaugura el Obelisco en relación al cuarto centenario de la fundación de la Ciudad de Buenos Aires. Se reúne el Congreso Constituyente de la C. G. T. 1938. Se inaugura el puente sobre el río Uruguay que une Argentina y Brasil. 1939. El primer emisor de T. V. en Argentina se instala en Rosario. 1940-1945. Los doctores E. Braun Menéndez y J. C. Fasiolo investigan la relación entre riñón e hipertensión.',
            'orden' => 40,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          38 => 
          array (
            'id' => 103,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000070.jpg',
            'titulo' => 'Página 70',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Surgimiento del peronismo Aparición del peronismo como movi- miento populista y nacional; los sec- tores obreros y las clases populares ingresan al escenario político, intentos de	industrialización;	nacionalización de empresas extranjeras. Explosivas migraciones internas: Los “cabecitas negras” constituyen cinturo- nes de villas miserias alrededor de las grandes ciudades, en un gobierno de estructura democrática, pero bajo la hegemonía personal de un líder. Destrucción de la ciudad de San Juan en el terremoto de 1944.                            Celebración del día de la lealtad, donde Perón, desde los balcones sa- luda la concentración 1953. “Los pies en la fuente”, el 17 de octubre de 1945 en Plaza de Mayo.',
            'orden' => 41,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          39 => 
          array (
            'id' => 104,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000071.jpg',
            'titulo' => 'Página 71',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1942. Se inaugura el gasoducto Buenos Aires - La Plata. 1943. El G. O. U.; derrocamiento del presidente Castillo. Asunción al poder por sucesivos golpes militares: generales Rawson, Ramírez, Farrel. 1944. Violento terremoto destruye la ciudad de San Juan. 1945. El 9 de octubre, Perón renuncia a sus tres cargos (Vicepresidente, MInistro de Guerra y Secretario de Trabajo) y es arrestado. El 17 de octubre, acto masivo obrero-sindical en adhesión a Perón. 1946-1955. Primera y segunda presidencia de Perón. En 1949 se produce la Modificación Constitucional, que posibilita su reelección presidencial. 1947. Bernardo Houssay, premio Nobel de Fisiología por sus trabajos sobre la influencia endócrina del páncreas y la hi- pófisis. 1947-1949. Nacionalización de los Ferrocarriles, Teléfonos y Compañía Telegráfica del Plata; estímulo y apoyo a la industria nacional. Se establece el I. A. P. I.; se crea la Flota Mercante Aérea del Estado. Se inicia el proceso inflacionario y acelerado aumento en el costo de vida. 1948. Fallece el Dr. Eduardo Finochietto, creador de la Escuela Quirúrgica. Se inaugura el gasoducto Comodoro Rivadavia - Buenos Aires y el aeropuerto Internacional de Ezeiza. En escultura, nombres como Curatella Manes, Libero Badií, Noemí Guerstein desarrollan su obra. Sesostris Vitullo (1899-1953), Alicia Penalba en Francia, Lucio Fontana y su espacialismo en Italia, Julio Le Parc, etc., obtienen reconocimiento en el exterior por sus méritos artísticos. Arte Abstracto, Arte Madí; Gyüla Kosice y sus hidroesculturas; Arte Concreto (Tomás Maldonado). A partir de los años 50 lanzamiento del Informalismo (Clorindo Testa, Kenneth Kemble, etc.), Pintura Generativa (Mac Entyre, etc.). 1943-1955. En Arquitectura, la acción del estado es predominante; se eligen tipologías adoptadas por gobiernos totalita- rios europeos (monumentalismo) o modelos folklóricos inauténticos y uniformizados a lo largo de todo el país, para un gran volumen de viviendas, escuelas, hoteles de turismo, colonias de recreo sindicales, etc., que se construyen. Paralelamente surgen expresiones del racionalismo (W. Acosta, Bonet, Sacriste, Alvarez,etc.). Edificios significativos: Facultad de Derecho, Municipalidad de Córdoba, Aeropuerto de Ezeiza, Fundación Eva Perón, policlínicos, colonias de vacaciones, sedes sindica- les, etc. 1951. Se sanciona el voto femenino.',
            'orden' => 42,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          40 => 
          array (
            'id' => 105,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000072.jpg',
            'titulo' => 'Página 72',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Violencias, persecuciones y sectarismos penetran y ensombrecen la vida argentina. Iglesias e instituciones sociales y políticas su- fren el incendio de turbas incontroladas.  El cortejo fúnebre dejando el Congreso, en las exequias de Eva Perón.  El pueblo de Córdoba frente a la Catedral y al Cabildo celebrando el triunfo de la Revolución Libertadora de 1955.',
            'orden' => 43,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          41 => 
          array (
            'id' => 106,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000073.jpg',
            'titulo' => 'Página 73',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1951. Juan M. Fangio gana el Campeonato Mundial de Automovilismo que revalidará en 1954, 1955, 1956 y 1957. 1952. Muerte de Eva Perón, exequias fúnebres sin precedentes. 1952. Se crea el Mozarteum Argentino. 1951-1953. Clausura de 44 diarios y revistas; desmanes y atro- pellos de masas incendian las sedes de distintos partidos polí- ticos y el Jockey Club de Buenos Aires. Entre el 54 y 55 culmina el conflicto y persecución con la Iglesia Católica, con el incendio de 11 iglesias y la Curia Me- tropolitana. 1955. Se instala la base general Belgrano, la más austral, a 1.300 Km del Polo Sur. Se firma el convenio petrolero con la California Argentina. Derrocamiento de Perón. Revolución Libertadora: Lonardi, presidente. El Dr. Eduardo de Robertis, biólogo, descubre en 1955 la sinapsis nerviosa y en 1960 la ultraestructura de los foto- rreceptores. 1956-1958. Se pone en funcionamiento el primer reactor atómico en Latinoamérica. 1958-1968. A. S. Parodi y H. A. Ruggero descubren el virus de la fiebre hemorrágica argentina y crean una vacuna para su inmunidad. 1960. Primera colada de acero argentino en San Nicolás. El médico René Favaloro inventa técnicas originales con relación a la cirujía cardiovascular, en especial el “by pass” aortoco- ronario que lleva su nombre. 1962. Aparece “Los Premios” de Cortázar. 1964. Muere el dr. Pablo Mirizzi inventor de la colangiografía intraoperatoria. 1964. Estreno mundial en el Teatro Colón de “Don Rodrigo”, primera ópera de Alberto Ginastera. 1964. La C. G. T. lanza un plan de lucha que puso en marcha la ocupación de 1.500 empresas. Comienzan las primeras mani- festaciones del movimiento gerrillero en Salta. 1965. Muere el Dr. José M. M. Fernández, creador de una reacción relacionada con la lepra que lleva su nombre y ha sido universalmente adoptada. 1965. Aparece “Sobre Héroes y Tumbas” de Ernesto Sábato.',
            'orden' => 44,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          42 => 
          array (
            'id' => 107,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000074.jpg',
            'titulo' => 'Página 74',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION Inestabilidad y subversión cada vez más presente en el desarrollo de la vida argentina. En los 53 años que van desde 1930 a 1983, Argentina sufre un descala- bro político, económico y moral que puede ser patentizado en el hecho de que en ese lapso, donde hubie - sen correspondido los períodos de 9 presidentes constitucionales, se sucedieron en la primera magistra- tura 28 gestiones, 15 de las cuales eran de facto. Inestabilidad y ambigüedad polí - tica estimulando el militarismo y crisis militares internas en procura del poder. Subversión. Terrorismo y guerrilla; represión y sus excesos: “la guerra sucia”; auge de la infla- ción. Estos acontecimientos dejan como saldo una población invadida por el escepticismo y la depresión, derivados de la alternancia de atis- bos optimistas que culminan en rei- teradas frustraciones. Luis Federico Leloir, recibiendo el Premio Nobel de Medicina y Química.            Central Nuclear de Atucha                 Estación	Terrena	de	Comunicaciones	Vía Satélite en Balcarce. Escenas de violencia callejera en el Cordobazo.La guerrilla urbana y campesina conmociona al país. El arribo de Perón culmina con sangrientos episodios en el Aeropuerto Internacional de Ezeiza',
            'orden' => 45,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          43 => 
          array (
            'id' => 108,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000075.jpg',
            'titulo' => 'Página 75',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1966. Argentina, a través del coronel Jorge Leal, llega al Polo Sur. La Fragat Escuela “Libertad”, de construcción argentina, obtiene (y aún lo posee) el récord mundial del cruce del Atlántico Norte por nave velera (6 días, 21 horas). 1966. Golpe militar. Gobierno de la Revolución Argentina. El “Che” Guevara, en Córdoba, organiza bases de acción sub- versiva. 1967. Ginastera y Mugica Láinez estrenan en Washington la ópera “Bomarzo”, cuyo posterior estreno en el país, es prohi- bido. Se inaugura el Centro Atómico de Ezeiza. 1968. M. E. Bellizi y H. A. Ruggero realizan el primer transplante de corazón. 1968. Se inicia la construcción de la Central Nuclear de Atucha. Nicolino Locche, Campeón Mundial medio mediano. El bailarín José Neglia es premiado en el 6° Festival de la Danza en París. En Taco Ralo (Tucumán), comienza acciones de guerrilla y terrorismo marxista-peronista. 1969. Se inaugura en Balcarce la Estación Receptora de Mensajes Vía Satélite. Se inaugura el túnel subfluvial Hernandarias (Santa Fe-Parana). Domingo Liotta trabaja en el corazón artificial, que se injerta por primera vez en Houston. El Cordobazo. Estallido de violencia obrero-estudiantil. Asesinatos de dirigentes sindicales: Vandor (1969), Alonso (1970), Rucci (1973). 1970. Secuestro y asesinato del general Aramburu; recrudece la actividad terrorista; copamientos de La Calera, Garín, etc. 1970. Federico Leloir recibe el Premio Nobel de Química por sus descubrimientos sobre el proceso interno por el cual el hígado recibe glucosa y devuelve glucógeno (metabolismo de hidratos de carbono). De Vicenzo, Copa Mundial en Golf; Dimiddi, Campeón Mundial de Remo; Monzón, Campeón Mundial de Box. Central Telefónica en Ushuaia. 1971. Se aprueba el contrato con “Aluar”. 1973. Complejo hidroeléctrico Chocón-Cerro Colorado (1968-1977). Se inaugura el puente “General Belgrano” entre Chaco y Corrientes. Regreso de Perón; matanza de Ezeiza. Acentuación de las acciones terrorista-guerrilleras (ERP, Montoneros, etc.) y nuevas acciones de violencia parapoliciales por la Triple A. Tercera Presidencia de Perón.',
            'orden' => 46,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          44 => 
          array (
            'id' => 109,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000076.jpg',
            'titulo' => 'Página 76',
            'texto' => 'GRANDES PERIODOS Y SU CARACTERIZACION El proceso militar y la recuperación de la democracia. Consecuencia de una inconciente medida militar, la recuperación de Malvinas, que enlutó al país, se acele- ra la caída del proceso militar y se des- emboca en elecciones populares. La euforia de la democracia. Una espe- ranza paciente, en función de develar la gran incógnita de las posibilidades de una reconciliación nacional, basada en la justicia y el trabajo. La población de Buenos Aires ce- lebra la victoria del Campeonato Mundial de Fútbol en 1978             El gral. Galtieri concentra a la po- blación porteña con motivo del con- flicto de Malvinas                    Visita del Papa a la Basílica de Luján. Concentración popu- lar en la asunción del Presidente Alfonsín',
            'orden' => 47,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          45 => 
          array (
            'id' => 110,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000077.jpg',
            'titulo' => 'Página 77',
            'texto' => 'HECHOS HISTORICOS Y CULTURALES SIGNIFICATIVOS  1974. Se pone en funcionamiento la Central Nuclear de Atucha que logra por primera vez en el hemisferio sur, la reacción de fi- sión en cadena autosostenida y autocontrolada. Se aprueba la construcción de la Central de Río III, Córdoba. Comienza la construcción de las obras civiles del Complejo Hidroeléctrico de Salto Grande. En San Nicolás, se inaugura un alto horno (el mayor de América Latina) en la Planta “General Savio” de “Somisa”. Guillermo Vilas obtiene el “Grand Prix”; Reuteman se impone en Sudáfrica, Austria y EE.UU.; Galíndez, nuevo campeón de box en medio pesado; Monzón, mantiene su categoría. 1975. Crisis económica: El Rodrigazo. El Ejército Argentino empieza en Tucumán el operativo antiguerrillero “Independencia”. 1976. Crisis política: Cambios ministeriales. Promedio: un ministro cada 23 días. Destitución de Isabel Perón por el golpe militar: Proceso de Reorganización Nacional, que perdurará hasta 1983, del cual el general Videla asume la dirección. 1976-1978. Se inaugura el Puente Internacional Fray Bentos-Puerto Unzué, sobre el río Uruguay y complejo ferro vial Zárate- Brazo Largo. Puesta en servicio del primer generador en la Central Planicie-Banderita. Se incorpora la Plataforma Submarina “General Mosconi” para detectar petróleo en los mares argentinos, operada por Bridas. Se inauguran los complejos hidroeléctricos Futaleufú y Cabra Corral. Se acentúa la represión ilegal de la subversión. Nuevo terremoto en San Juan (Caucete). Se da a conocer el fallo de la Reina de Gran Bretaña sobre el Beagle, que asigna las islas al sur del canal a la República de Chile. 1978. Argentina gana el Campeonato Mundial de Fútbol y el Campeonato Mundial de Hockey sobre patines. Se comienza la construcción del gasoducto austral. 1979. Se inaugura la primera turbina de salto Grande. Se adjudica la construcción de la Central Nuclear y la Planta de Agua Pesada Atucha II, en Río Tercero, Córdoba. Se acepta la mediación Papal para el diferendo del Beagle. 1980. Pérez Esquivel, Premio Nobel de la Paz. Comienzan las emisiones regulares de TV color con la imagen de la bandera argentina. 1982. Papel de Tucumán produce la primera bovina de papel a partir del bagazo de la caña de azúcar. 1 de abril. Se inicia la guerra por la recuperación de las Islas Malvinas. Juan Pablo II visita Buenos Aires. Serias manifestaciones contra los excesos cometidos por la represión. 1983. Se reinicia la vida democrática. Presidencia de Alfonsín. 1984. César Milstein recibe el Premio Nobel en Fisiología por sus descubrimientos sobre los mecanismos inmunológicos de la transmisión de los genes.',
            'orden' => 48,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          46 => 
          array (
            'id' => 111,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000078.jpg',
            'titulo' => 'Página 78',
            'texto' => '',
            'orden' => 49,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          47 => 
          array (
            'id' => 112,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/es/00000079.jpg',
            'titulo' => 'Página 79',
            'texto' => '',
            'orden' => 50,
            'idioma_id' => 1,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
        ),
        'en' => 
        array (
          0 => 
          array (
            'id' => 113,
            'imagen' => '/storage/app/public/tomos/capitulo_title_background.jpg',
            'titulo' => 'Page 32',
            'texto' => '',
            'orden' => 3,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          1 => 
          array (
            'id' => 114,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000033.jpg',
            'titulo' => 'Page 33',
            'texto' => 'In order to provide a more complete and well-organized view of the country in all its diversity, I have decided to pres- ent it in terms of regions, each of which will be the subject of a book in this series. Metropolitan Buenos Aires will also be the subject of a book, although I will not discuss it as a region. Buenos Aires will be thoroughly discussed in the last book as “The Other Country,” a city born with a hegemonic destiny whose extraordinary influence has, in many ways, gone beyond the boundaries of the Federal Capital. Of the various possible criteria for determining the regions of the interior, I have chosen to follow the approach devised by the sociologist Dr. Juan Charles Argulla in his Study on Argentine Society because of the breadth and depth of his analysis. Consequently, I will proceed to summarize the basic con- cepts of the series in light of the approach that I developed in the previous section, which will serve as the framework for the entire series; that is, in light of the ongoing relation- ship between three factors: nature, humankind, and their resulting product (including all of the factors that charac- terize a society), which we can bring together under the term culture. he regions of Argentina are structural units that have re- sulted from a number of complex processes and factors (such as geographical, historical, demographic, economic, and social) that have gradually become intertwined, form- ing extremely rich tapestries of unquestionable internal co- herence whose particular characteristics have created rela- tively homogeneous ways of life. We can approach the problem of regionalization through a historical theory or account that will help us to explain the past, understand the present, and make predictions about the future; the results might then be used in the service of a new national policy based on regional realities. This approach links the process of national integration to the ongoing process of regional differentiation through integrating characteristics (national consciousness) and differentiating characteristics (regional potentials and possibilities). After a long and painful period for the Argentine people, the Constitution of 1853 clearly set out, for the first time, what aspired to be a national society. From that time forward, the government implemented a lib- eral policy based on importing manufactured products and exporting agricultural products. The Pampas were discov- ered, and this incipient region was assigned a decisive and predominant role in the nation’s development, contribut- ing to the consolidation of the political concept of the Pampas and the port of Buenos Aires. The political concept of the Pampas and the port of Buenos Aires as an idea or force of liberal politics confronted the political concept of the interior and the Andes, which had been the focus of the Latin American interests of the Viceroyalty and the federalist battles for Confederation; the former concept was associated with civilization and the latter with barbarism without assigning any further dis- tinctions between them. The political concept of the Pampas-port was strength- ened by the Conquest of the Desert, the establishment of settlements by European immigrants, and the building of railroads. The port was a bone of contention between the city (and the neighboring Pampas) on the one hand, and the heterogeneous interior on the other, since the latter did not have a specifically assigned role in the nation’s development and demanded its share of the port’s benefits. In contrast to the interior, the vitality of the Pampas re- gion gradually increased as it absorbed Entre Ríos and oth- er regions; this was the result of a policy that assigned the Pampas an explicitly predominant role over a period of more than 90 years. Two entirely distinct and opposing social consciousnesses emerged from this historical process. In the Pampas re- gion, this emerging consciousness was full of optimism and awareness of the region’s role in the nation’s development; a centrist and international (or European) nationalism; “the happy Argentina,” in the words of Daus. Meanwhile, in the ne- glected interior, the outlook that developed was full of re- belliousness and cognizance of the region’s lack of purpose; a federalist',
            'orden' => 4,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          2 => 
          array (
            'id' => 115,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000034.jpg',
            'titulo' => 'Page 34',
            'texto' => 'and continental (or Latin American) regionalism; “the hard Argentina,” in the words of Daus. The interior as a whole started to become a marginalized region, a sort of anthropological reserve of the nation’s de- velopment, and its economy was strictly regional in scope. Nevertheless, the still vigorous process of regionalization continued to define other geographical subregions, particu- larly the Andean subregion because of its traditional culture and historical development. However, the nation’s territory extended well beyond these two regions. Other geographical areas remained – areas which, unfortunately, history had virtually overlooked – and when these began to attract attention, a new process of regionaliza- tion began as the belated result of Argentine policy. This led to the emergence of the Chaco and Patagonian regions. Four regions now took shape: the predominant Pampas, the marginalized Andean region, and the emerg- ing Chaco and Patagonian regions. These descriptions are neither pejorative nor complimentary. The Pampean Region Nature: The central region of the country is a virtually tree- less plain that is covered by a grassy carpet throughout the  year; it has no dry season. It is connected to the interior by natural roads, waterways, and overland routes. The region has no major geographical features. Bounded by the River Plate, the entire plain opens out toward the west. This region has been incredibly dynamic. In 1872, the region had 600,000 hectares under cultivation, and it continued to expand in all directions, taking in a large part of other provinces: northern and eastern Pampas, southern and central Córdoba, central south- ern Santa Fe, and southern and central Entre Ríos. By 1900, the re- gion had grown to 10,000,000 hectares, and by 1946, to 22,700,000 hectares. The region also absorbed the majority of the population of Greater Buenos Aires, Rosario, and the coastal region. Humankind: The demographic, social, economic, and cultural homogeneity of the Pampas, combined with its standing as the country’s predominant region, has reached the point that today, its inhabitants virtually represent Argentina to the world. The original inhabitants were primitive Indian hunters and nomads. Their encounters with the Spanish, primarily in the coastal region, resulted in numerous drawn-out conflicts. The figure that ultimately emerged from these battles was the gaucho, the prototypical man of the Pampas. According to Bond Head, the gaucho “…lives a life of privation, but his lux- ury is freedom. Proud of his unlimited independence, his sentiments – as savage as his way of life – are noble and good”. The history of this region is the story of conquering the natu- ral world in order to protect borders and gain new territory, thereby increasing the influence of the port. Little by little, the indigenous hunters became dealers of agricultural goods. Immigration policy caused this region to absorb more for- eign immigrants than any other area, particularly due to a settlement policy that offered land and credit, an invest- ment policy that allowed the construction and expansion of railroads (which were designed to serve the port), and an education policy that made it possible to quickly integrate large numbers of immigrants into the country. Product: Beginning with the arrival of Pedro de Mendoza, aban- doned cattle and horses encountered natural pastures. This is how the region’s rich livestock industry began; it diversified and grew as a result of careful breeding with fine species, the fencing in of fields, and livestock management, leading to the export of meat, raw leather, horsehair, animal feed, bones, and other goods, and the establishment of the first salteries and stockyards. Toward the middle of the nineteenth century, livestock breeds began to improve, and the first breeding',
            'orden' => 5,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:19',
            'updated_at' => '2019-01-12 19:01:19',
          ),
          3 => 
          array (
            'id' => 116,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000035.jpg',
            'titulo' => 'Page 35',
            'texto' => 'ranches ap- peared. In addition, agriculture became more specialized; selected seeds and technological innovations were intro- duced, diversifying the original crops of corn, wheat, and flax and adding oats, barley, rye, oil-producing seeds, and fodder. The benefits of foreign and domestic trade were also felt in the region; the producer, the consumer, and the recipient of these benefits were one and the same. The economic impact of events such as World War I, the financial crisis of 1929, and World War II caused a partial slowdown in agricultural production but stimulated indus- trial production. This led to the appearance of meat-pro- cessing plants, textile and garment factories, the metallurgi- cal, chemical, petrochemical, and steel industries, and large refineries and distilleries. The entire corridor running through Rosario, Campana, Zárate, San Nicolás, Avellaneda and Bahía Blanca was gradu- ally transformed into a huge industrial park, a process which culminated in the establishment of the automobile indus- try (1959) and the construction of the Atucha Nuclear Power Plant (1974). Only a few isolated areas of the country have been the site of specialized industrial development, primar- ily Córdoba – which has a military aircraft factory (1927), an automobile factory, a mill, and railroad, tractor, and engine factories, among others – and Bahía Blanca, Mar del Plata, Santa Fe, and several other minor cities associated with the food industry. Since various protectionist policies have promoted this pro- cess of industrialization, the Pampas region has continued to be the privileged beneficiary of the country’s production. The Andean Region Nature: Resting in the shadow of the Andes, this region covers the central and northeastern areas of the country. Mountains, valleys, ravines, and salt flats, which create natural obstacles to communication, occupy nearly 700,000 square kilometers. The region is relatively homogeneous, however, in terms of its social, economic, and cultural char- acteristics, despite the differences that are naturally caused by mountainous regions. It has a dry, inland climate with clean air and blue skies; the parched La Puna desert, in particular, receives very little rain. As a result of these arid conditions, life is concentrated in the valleys. One of the region’s special characteristics is that it is closely associated with the Hispanic and indigenous America of bygone days. Humankind: This region was home to the country’s most advanced and sophisticated cultures. The culture of these sedentary, agricultural peoples was reflected in their artwork and handicrafts. When the Spanish arrived from Peru during the sixteenth and seventeenth centuries, they discovered natural resources and indigenous labor and founded nearly all of the region’s major cities. This led to a great deal of racial mixing (mestizaje) and the subordina- tion of the indigenous peoples to the organization of the Spanish encomienda and forced labor systems. The region played a leading role in the history of the Spanish conquest, the colonial period, and even the wars of independence in connection with the federalist struggle, an Andean and continental cause since its earli- est days. The process of national integration simultaneously marginalized and consolidated the region. The region’s population is concentrated in several large cities, a few small cities, and many towns and villages. The region also has a fairly large rural population that is primarily engaged in agriculture, except for small groups of European, Spanish, Italian, Syrian, and Lebanese immigrants who arrived toward the end of the nineteenth century and played an important role in region’s economic and commercial development. Nevertheless, traditional mestizos and criollos continue to constitute the region’s most important ethnic group. Product: The marginalization of the region in relation to Argentine society derived from the fact that it had lost the purpose it had served from the beginning of the colonial era until the first several years of national independence',
            'orden' => 6,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          4 => 
          array (
            'id' => 117,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000036.jpg',
            'titulo' => 'Page 36',
            'texto' => 'endence as a consequence of the long-term policies of the national gov- ernment. During the region’s first era, the livestock (mule) and agri- cultural industries (corn, potatoes, grapes, olives, etc.) had reached a significant level of development; it also had an in- cipient mining industry, handicraft production, and small industry. In 1853, the region’s importance and econ- omy began to decline. Despite its defensive struggles against the power of Buenos Aires (which enabled it to gain several advantages), it was never assigned a role in the nation’s devel- opment. For the most part, agricultural production is still car- ried out on large estates (estancias) (possibly remnants of the old encomiendas) whose productivity is dependent on condi- tions in the valleys, especially on the availability of water. Following the exhaustion of the liberal agenda in the 1940s, the national government attempted to institute a development policy for the region. The government designated Tucumán, Mendoza, and San Juan as “protected areas” as a result of the specific industries that had been developed in these areas over many years (sugar cane and grapes); these areas also managed to industrialize agricul- tural production. Subsequently, something similar occurred in Salta and Jujuy, which began to develop in conjunction with the petroleum (refineries) and iron (steel) industries. In recent years, significant growth has occurred in industries derived from agricultural production, including alcohol and paper production in Tucumán and canning in Mendoza. Córdoba is unusual in a particular respect. Initially, it was part of the Andean region, but later it became partially in- corporated into the Pampas region due to factors such as immigration, increasing industrialization, and greater agri- cultural production. The Andean region mainly produces raw materials, includ- ing tobacco, fruit, olives, vegetables, minerals, copper, silver, gold, beryl, manganese, cement, and marble, as well as fire- wood and wood for posts and beams, especially from the mountains where the carob tree is found. The raising of karakul sheep, llamas, alpacas, vicuñas, and chinchillas has given rise to a small industry that is strictly limited to the production of traditional handicrafts. The utilization of water resources for human consump- tion, irrigation, and hydroelectric power is the most im- portant issue in the region. (The issue has been systemati- cally analyzed in part, particularly in Córdoba and Mendoza.) An appropriate water policy would enable the region’s vast potential to become a reality. The Chaquean Region Nature: The Chaco comprises the northern and eastern ar- eas of the country and shares many general characteristics with bordering countries (from which the region is sepa- rated by large rivers). It features significant navigable rivers that constitute part of the Paraná watershed, creating a homogeneous landscape with the natural variations com- mon to all large areas. The subtropical, humid climate, to- gether with the plains and forests, lend this area of more than 500,000 square kilometers a certain geographical reg- ularity; the most notable areas include subregions of the Western and Eastern Chaco, the estuaries and lagoons of Corrientes, and the Missionary Jungle. Humankind: The region originally had a rather large indig- enous population with a certain degree of culture, especial- ly Guaraní peoples, who lived near the banks of the rivers. Until recently, the region was marginalized in relation to the rest of the country, despite the historical significance of ar- eas such as Corrientes during the federalist struggles and the Jesuit Missions that were established along the rivers at the beginning of the colonial period, following the route of the conquistadors to the northern reaches of the colony and the entryway to Peru. The region’s character is still in the early stages of develop- ment within the context of the colonial past. Although in recent years it has given indications of structural homoge- neity and internal coherence, the population is not yet suf- ficiently integrated. ',
            'orden' => 7,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          5 => 
          array (
            'id' => 118,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000037.jpg',
            'titulo' => 'Page 37',
            'texto' => 'The area known as Formosa y Chaco is still home to several indigenous groups who are subsisting in an extremely pre- carious situation. Hispanics and mestizos – who are especially prevalent in the old province of Corrientes – and immigrants (particularly Germans, Poles, Brazilians, and Paraguayans) constitute a significant proportion of urban populations in Corrientes-Resistencia, cities such as Posadas, and smaller cities; there is also a considerable rural population. Products: The Chaco has the potential to play an impor- tant role in creating closer ties between Argentina and Latin America. In addition, the region’s inherent potential as a subtropical region will enable it to play a role in the coun- try’s future development. Early on, it was a producer of raw materials for the Pampas region, providing it with wood, tannins, maté, and other products. When the territories that comprise the Chaco became provinces, the region began to participate in the political life of the country and incentives for economic de- velopment were put in place, especially in the agricultural sector. Cotton led the way, with settlers on government land cultivating up to 200,000 hectares. Tropical fruits and vegetables were also cultivated (sweet potato, watermelon, cassava, spurge, sorghum, bananas, tea, tobacco, tung tree oil, soy, rice, and so forth). Several of these products – in- cluding tea and bananas from Formosa, rice and tobacco from Corrientes, coffee and tea from Misiones, and cotton from Chaco – had an impact on the nation as a whole. Industrial production is still in its infancy. The most impor- tant industry is undoubtedly the lumber industry (logging camps and saw mills), which harvests the quebracho colo- rado, the araucaria of Misiones, and other woods. Several related industries are also important; in view the country’s voracious appetite for paper, the paper industry is currently poised to become a major source of wealth. Other major resources that are processed on an industrial scale are mate (drying sheds) and cotton (gins) used in the production of cottonseed oil and cotton fiber for spinning mills and textile factories, as well as other resources men- tioned earlier, such as tea, tobacco, rice, and tung tree oil. All of the region’s industries are dependent on a policy of im- port substitution that favors subtropical products produced in Argentina, and further progress will depend on whether the region is assigned a specific role in the nation’s future de- velopment, taking into account the region’s enormous poten- tial for energy production (Yaciretá-Corpus) and the possi- bility of improving access to consumer markets, particularly in the Pampas, Brazil, Paraguay, and Bolivia. The latter condition would require an unequivocal opening of borders and a clear orientation toward Latin America. The development of this region’s huge potential may be of fundamental importance in the full and comprehensive development of the nation. The Patagonian Region Nature: This vast, isolated region resembles a huge penin- sula stretching toward Antarctica. It has several distinct sub- regions, although enormity, winds, and demanding climate are common to each. The coast is a rich marine littoral environment that includes features such as the Valdés Peninsula, the broad drowned valley and strong tides of the Gallegos river, and the con- necting islands, especially Isla Grande, which Argentina shares with Chile, the disputed Malvinas Islands, Isla de los Estados with its fantastic perimeter, and so on. The central region is composed of dry, steppe-like plateaus with a terraced profile, where it appears that the history of ancient events can still be deciphered in the remains of in- tense volcanic activity, the residue of ancient oceans, and a treasure trove of fossils. The mountain and lake region is remarkable for its many landscapes: scenes of majestic peaks, glaciers, luxuriant forests with plants of various spe- cies and colors, and peaceful, fertile valleys. Throughout history, the region has been associated with dis- tance, something like The Wild South.',
            'orden' => 8,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          6 => 
          array (
            'id' => 119,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000038.jpg',
            'titulo' => 'Page 38',
            'texto' => 'Humankind: The region’s first inhabitants initially made their way into Western history by way of legend: the Patagonian giants that amazed Magellan, Pigafetta, Byron, and many others. The region was sparsely populated by nomadic hunter-gatherers who left behind no monu- ments (other than remarkable pictographs). Archeologists and anthropologists, however, have continued to find evi- dence of their culture, particularly in this century. The Tehuelches, Mapuches, Onas, Alacalufes, Hausch, and the Yaganes, an extraordinary canoeing people that plied the waters around the southern islands, were the masters of the region. These indigenous peoples were later conquered and driven into isolation by the rising flood of European immigrants; an exception to this trend was the humanitarian treatment indigenous peoples received from English missionaries and the Salesian Order. Over the course of time, organized colonization efforts brought Spaniards, Italians, Welsh, South Africans, and other immigrants to the region, and people of various nationalities settled on the rich and virgin terrain. A fi- nal wave of internal migration brought people from oth- er provinces and neighboring countries to the region in connection with the exploitation of various natural re - sources. Products: Since the government left it undisturbed as a potential source of natural resources, the region has primarily grown through the efforts of individual pi- oneers. Initially, the most prevalent form of wealth was wool. Although ranch management was neglected, the quality of the flocks was improved and facilities were built in a huge effort to gather white gold. Toward the turn of the century, however, black gold started to be - come the impetus for new enterprises. The region has huge oil and gas reserves, as well as carbon, iron, and aluminum deposits. Popper’s unclear visions of gold have been confirmed, leading to significant gold mining today. The sea and fishing constitute another extremely impor- tant resource that pertains almost exclusively to this region. There is a need for better political and business decisions with regard to this resource in order to achieve its full po- tential as a source of growth. The harnessing of sources of hydroelectric power, which has the potential to satisfy all of the nation’s energy and ir- rigation needs, has resulted in the creation of agricultural and industrial empires in several areas, which in turn are important producers of goods, both for the country and for export. Another of this region’s most important resources, based on the variety, splendor, and uniqueness of its landscapes, is tourism. Currently, tourism attracts many Argentine and foreign visitors to the region, from youth-oriented adven- ture tourism to sophisticated cruises through the channels of Tierra del Fuego and along Antarctica. As we can clearly see, the potential of this emerging region is currently be- coming a tangible reality.',
            'orden' => 9,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          7 => 
          array (
            'id' => 120,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000039.jpg',
            'titulo' => 'Page 39',
            'texto' => '',
            'orden' => 10,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          8 => 
          array (
            'id' => 121,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000040.jpg',
            'titulo' => 'Page 40',
            'texto' => '',
            'orden' => 11,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          9 => 
          array (
            'id' => 122,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000041.jpg',
            'titulo' => 'Page 41',
            'texto' => 'The following section represents an attempt to briefly sum- marize the key events in Argentine history that determined or contributed to the development of our country and the character of Argentines. Upon writing this second edition, I decided to recast the entire first part – which deals with Argentina in general and which is included in this section – in the form of an introductory vol- ume to the series. Since the original list of important events had extended up to 1985, it would have been possible to in- clude the events that had occurred as of 1999. This updated list could have included much “news” of the past, such as the dis- covery of the remains of bonfires on the Valdés Peninsula; cave paintings near Lake Lácar; the strange pyramidal construction of Catamarca under investigation by Rex González; numer- ous dinosaur remains and fossilized dinosaur eggs and em- bryos; natural disasters ranging from the eruption of Hudson Volcano to the coastal floods; political and economic initiatives, including the attempt to move the Capital, the amendment of the Constitution, the autonomy of the Capital, economic re- forms, the privatization of public assets and services, and the formation of MERCOSUR; moments of grief, such as the at- tacks on the Israeli Embassy, the AMIA, the death of the jour- nalist Cabezas, and the death of the solder from the province of Neuquén, which led to the abolition of compulsory military service; the settling of border disputes with Chile through sev- eral treaties; a diverse range of events that included interna- tional recognition for Argentine films, the deaths of Borges, Piazzola, and Bioy Casares, and the rise of Maradona as a sym- bolic figure of soccer; the admission of Bianciotti to the French Academy of Letters; the international prizes, awards, and ac- knowledgements given to such figures as Julio Bocca, Paloma Herrera, Maximiliano Guerra, and Herman Cornejo for ballet, to lyrical singers Cura and Lima, and others for poetry, and to world-famous masters such as Baremboin and Marta Argerich. Including all of the above would lead to an endless list that, obviously, would never be complete. Consequently, I decided to choose a more interactive approach that would also be in better accord with the purposes of the book. That is, taking a page from traditional crime novels, I de- cided to challenge readers to decide for themselves, individually, after careful reflection and based on their own values, which events and protagonists were most significant in the political, scientific, and cultural development of the Argentine people.',
            'orden' => 12,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          10 => 
          array (
            'id' => 123,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000042.jpg',
            'titulo' => 'Page 42',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS The Sixteenth and Seventeenth Centuries: Discoverers and Founders The continual search for an outlet to the Pacific leads to a combination of discoveries and actions by virtue of which the history of the Spanish conquest begins to take root on our soil: 1516, Solís, and the entry into the world of our majestic Freshwater Sea, the River Plate; 1520, Magellan, Elcano, Pigafetta: improvised settlements, the Straight of Todos los Santos, circumnavigation of the globe, chronicles and cartography; 1527, Gaboto-Ayala and the founding of Sancti Spiritu; and 1536, Juan de Ayola, Fort Corpus Christi, the origin of villages where “Argentine” wheat is cultivated for the first time. Opposite: Ulrico Schmidl (who spent 20 years in Argentina) left us this testimony: the engraving “El Hambre en Buenos Aires” (Hunger in Buenos Aires), which is echoed in “Elegía” (Elegy) by Friar Luis de Miranda: “The most common victuals / were thistles they would forage / and even these, they could not  always find. / The dung and feces that some did not ingest, / were eaten by many a miserable soul /which was a fright. / It got to such a state that / like in Jerusalem, / even human flesh was eaten; / the things seen there / have not been seen in writing: / To eat the entrails  / of your fellow man...!” The lands of the Sea of Solís (River Plate) begin to gain new renown. Cover of a book by Martín del Barco Centenera (1602); its first song ontains the following verses “Of the Chiriguano Indian ridiculed, as a true human being I sing alone, in order to discover the long-forgotten soul of the Argentine Kingdom; great Apollo send me from the holy mountain, help me to publish, here, without deception, for the world, our history of admirable things, memory.  With your aid, I will write this book of the Argentine Kingdom, telling of various adventures and strange events, wonders, famines, wars and heroic deeds” ',
            'orden' => 13,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          11 => 
          array (
            'id' => 124,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000043.jpg',
            'titulo' => 'Page 43',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1525. Edict of Charles V of Spain, March 4: “To the people of the discovered islands and lands...let no force, thievery or death, nor any other damage or injury or wrong of any kind be done to them...because it is my will that they be peaceable and that no unjust act be done to them, nor any mistreatment ” 1527. Sea diary of Loaysa’s armada: “...at the Solís River, which is called the River Plate  ,” first document in which the name of the river appears. 1536. Ulrico Schmidel, Bavarian soldier and our first chronicler in German: “When the Querandíes surrounded the city, everyone suffered great misery as many died of hunger this anguish increased as a result of a scarcity of cats, rats, snakes and other foul creatures with which they had learned to appease their hunger, and they ate shoes and other bits of leather. It was then that three Spaniards secretly ate a horse they had pilfered...and they were hung, and that night three other Spaniards cut their thighs and other pieces of meat so they would not die of hunger.” 1536. Don Fernando de Zamora, private physician of Don Pedro de Mendoza, is the first doctor to arrive in the region. 1544-1605. In 1602, Martín del Barco Centenera writes his poem “La Argentina,” whose title anticipates the name of our country. 1586. The Jesuit Order enters the country and initiates its enormous missionary and cultural work. 1593. First trial for professional liability against Asencio Telles de Rojo (of whom it is said he is nothing more than a barber, and he is considered responsible for the death of seven slaves). Pedro Hernández, chronicler of the expedition, writes his Comentarios de Alvar Núñez Cabeza de Vaca (Commentaries of Alvar Núñez Cabeza de Vaca), which presents a colorful picture of life along the Paraná and Uruguay Rivers in the sixteenth century. 1603. Don Pedro Díaz, first physician according to notes in the General Archive of the Nation. 1612. Ruy Díaz de Guzmán, criollo and mestizo of Asunción, author of chronicles of a literary type, composes “La Argentina Manuscrita” (which was circulated and became known in manuscript), containing a recounting of the Lucía Miranda episode. 1604-1680. Luis de Tejeda y Guzmán. The century opens with the first Argentine author, born in Córdoba, where he writes his poem, “El Peregrino de Babilonia” (The Pilgrim of Babylon) in a baroque, Gongorine style.',
            'orden' => 14,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          12 => 
          array (
            'id' => 125,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000044.jpg',
            'titulo' => 'Page 44',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS Economic Needs and Matters of Sovereignty Begin to Define Territories. The need to secure and document ownership; the search for a route by which resources could be transported from Peru via the Atlantic leads to the founding expeditions; and Charles V’s captain-generals establish forts and villages. Land expeditions: cities founded in the north and west: 1550, Ciudad del Barco (Juan Nuñez del Prado); 1553, Santiago del Estero (Francisco de Aguirre); 1559, Londres (Juan Pérez Zurita); 1565, San Miguel de Tucumán (Diego Villarroel); 1573, Córdoba de la Nueva Andalucía (Jerónimo Luis de Cabrera); 1582, San Felipe de Lerma de Salta (Hernando de Lerma); 1593, San Juan Bautista de Todos los Santosde la Nueva Rioja (Juan Ramírez de Velazco); 1593, San Salvador de Velazco del Valle de Jujuy (Francisco de Argañaraz); 1561, Mendoza (Pedro del Castillo); 1572, San Juan de la Frontera (Juan Jufré); 1596, San Luis de la Punta de la Frontera (Luis Jufré); 1683, San Fernando del Valle de Catamarca (Fernando de Mendoza Mate de Luna); 1685, San Miguel de Tucumán (Fernando Mate de Luna). Coastal expeditions: 1536, Buenos Aires (Pedro de Mendoza); 1573, Santa Fe de la Vera Cruz (Juan de Garay); 1580, Ciudad de la Santísima Trinidad and Puerto de Santa María de los Buenos Aires (Juan de Garay); 1585, Concepción de la Buena Esperanza del Bermejo; 1588, San Juan de Vera de las Siete Corrientes (Alonso de Vera y Aragón). Cupola of the Córdoba Catedral (Kronfuss, 1920). Stone-sculpted insignia, the toad of the University of Córdoba, 1622.',
            'orden' => 15,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          13 => 
          array (
            'id' => 126,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000045.jpg',
            'titulo' => 'Page 45',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  Education advances slowly. Pedro Vega (1577) in Santa Fe is the first secular teacher. At the request of Francisco de Victoria (1605), the town council of Buenos Aires “authorizes him to tutor the children of residents and inhabitants with payment--at their expense- -at the rate of one peso for reading and two pesos for writing and ciphering.” Secondary education is established at the Jesuit Colegio Máximo, Nuestra Señora de Monserrat College, Córdoba; in 1612, Friar Trejo y Sanabria founds what will become the country’s first university with his own funds, also in Córdoba. The city is transformed into an active center for the study of the sciences, philosophy and history. The first printing shop in the country is opened in Córdoba at the same time. The first chamber ensembles are created (harpsichord, flute, and violin). The Metropolitan Cathedral by this time has an orchestra with an extraordinary number of 12 musicians. Architecture, a mixture of empiricist and Mudejar styles, makes use of adobe, stone, ties, tile, or straw (until approximately 1810). While the layout of most buildings is spontaneous, a few exceptional structures demonstrate purposeful design and a high level of achievement, such as aristocratic homes and, especially, religious compounds such as San Ignacio in Misiones, Santa Catalina, San Isidro, and Alta Gracia in the province of Córdoba, among others, due to the work of architects of religious orders or military engineers (Prímolo, Bianchi, Lemer, Krauss, Bernardez de Castro, Filcaya, Havelle, Ayroldi, Masella, etc.). First musician priests: Alonso Barzana and Francisco Solano. Working in the Jesuit missions (between 1616 and 1726) are teachers such as the Flemings Luis Berger, Juan Vasco, Pedro Comental, and the Italian composer and spinet player, Doménico Zípoli. The first ensemble composed of three harps, eight violins, one bassoon, three lyres, two flutes, two cornets, and four basses is founded, and the instruments are made in the missions. 1767. Expulsion of the Jesuit Order. 1776. The Viceroyalty of the River Plate is created. 1778. Viceroy Juan José de Vértiz y Salcedo, a criollo born in Mexico, introduces liberal reforms. As a result, books are brought into the country that contain the English and French thinking of the period, which will have an influence on the makers of our future independence. He establishes the first human settlement on the shores of Patagonia, as well as villages along the Negro River, and brings education to Entre Ríos, Gualeguay, Gualeguaychú, Uruguay, among other places. He creates the Royal College of San Carlos in Buenos Aires and the Board of Physicians (which would later give rise to the first medical school); he moves the printing shop from Córdoba to Buenos Aires and creates numerous and varied social service institutions. 1780. Viceroy Vértiz appoints Dr. Miguel Gorman, who arrives with Pedro de Ceballos’ expedition, as head of the Board of Physicians.',
            'orden' => 16,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          14 => 
          array (
            'id' => 127,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000046.jpg',
            'titulo' => 'Page 46',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS Ambition and Imagination Open New Roads While Urban Life Becomes Traditional and Static. The search for legendary riches gives rise to the mythical City of Gold and the expedition of Captain Francisco César and his men, as well as the legend of the City of the Caesares. The race for supremacy between the Spanish and Portuguese empires and the privateers of other empires (especially the English, French and Dutch) leads to an increase in knowledge and written information about the region. Colonial life: Political and economic dependence on Spain and its monopoly; pressure from economic interests of distant powers. After the treaty of Utrecht, Spain concedes the slave trading center of “El Retiro” to England; Spain also grants English subjects the right to establish residency in Buenos Aires (giving rise to a community that would become the most vibrant in the world). Concessions and restitution resulting from diplomatic negotiation in the European courts seal the fate of the Malvinas and the Colonia del Sacramento. Embryonic cultural development and public education almost exclusively limited to patrician homes and convents, where “the ability to read, count, and pray” (the alphabet was to be memorized from the first letter: Christ, a, b, c...) was considered sufficient. With the beginning of the reign of Charles III, the Fourth Viceroyalty of Hispanic America is created: the River Plate, which to some extent prefigures the geographical layout of Argentina. This initially temporary decision, which was intended as a means of opposing the advance of the Portuguese empire – already entrenched in Colonia del Sacramento – also has major economic implications. Up to that time, European goods imported to Buenos Aires were required to use the following absurd legal route: goods were loaded exclusively in Cádiz, unloaded in Panama and transported on muleback to the Pacific, reloaded and transported by sea to El Callao, Lima, and then transported by land through the jungles, mountains and plains to Buenos Aires. With the Viceroyalty and legal opening of the port of Buenos Aries, not only did the political and economic aspects of life become more efficient and modernized, but cultural aspects of life did, as well: education, the printing press, books, and the increasing spread and influence of ideas and opinions that had been prohibited prior to that time. Spain’s position in support the American Revolution out of hatred for England fans sentiments among its own colonists that prepare them for their own future emancipation. Spread of illuminism, encyclopedism, and the ideas of Montesquieu, Voltaire, and Rousseau. MAY 25, 1810: THE MAY REVOLUTION El Riachuelo Crossing (during the English invasions). Anonymous engraving from 1806, published in London in 1807.',
            'orden' => 17,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          15 => 
          array (
            'id' => 128,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000047.jpg',
            'titulo' => 'Page 47',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1781-1801. Félix de Azara drafts a map of South America and begins a compendium of the fauna found in the Plate region. 1799. Manuel Belgrano, Consul, founds a School of Architecture, Geometry and Perspective, and, together with Cerviño, the School of Nautical Science and Mathematics. 1754-1810. Manuel de Labardén opens the first theater in Buenos Aires, La Ranchería, at what is now the corner of Perú and Alsina. He writes his play Siripo, which recounts the story of Lucía Miranda. 1801. The country’s first newspaper, El Telégrafo Mercantil, is founded by Francisco Cabello y Mesa 1801. The School of Medicine and Surgery is opened. 1802. Agriculture, Industry and Commerce Weekly is published under the direction of Hipólito Vieytes. 1804. The Coliseo Theater opens at Reconquista and Cangallo, presenting tonadillas (songs intended for theatrical presentation) and zarzuelas. 1805. Antonio Machado receives an award from Viceroy Sobremonte for importing the smallpox vaccine, which is administered by Dean Saturnino Segurola. 1806-1807. English invasions. The Times of London publishes a report on the conquered lands: “...here nature alone created...an exceptionally favorable climate,...extremely fertile cultivated land,...the plains support millions of cows, horses, sheep and swine,...there is such an abundance of fresh meat,...that it is often distributed free to the poor,...there are plenty of places for ships to enter,...the fishing is very productive as well as the hunting,...cotton, flax and hemp are cultivated in many districts,...there are a few gold mines and other mines.” All these marvels, which anticipate the worldʼs agricultural future, “...are now part of the domain of His Majesty,” the English paper’s report concludes. The report was published September 25, 1806, although, unbeknownst to the editors, Buenos Aires had been won back more than a month earlier. Santiago de Liniers y Bremont is appointed Viceroy. 1810. First National Governing Board. 1810. The Commercial Courier is published under the direction of Manuel Belgrano. Mariano Moreno publishes The Buenos Aires Gazzette, whose subheading declares: “Happiness is rare in times when one may feel what one likes and say what one feels.” The Philharmonic is created – the first cultural association dedicated to musical activities. The National Library is founded.',
            'orden' => 18,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          16 => 
          array (
            'id' => 129,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000048.jpg',
            'titulo' => 'Page 48',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS The War for Independence. The War for Independence is launched with the simultaneous advance of armies toward the centers of Spanish power: contingents advance on Upper Peru, Paraguay, Uruguay, and the Pacific; the Army of the Andes. At the same time, negotiations, conversations, and strategies aimed at creating a consensus among the various provinces for a conference in Tucumán are put in motion. JULY 9, 1816: Declaration of Independence “The Street of the Cathedral.” Watercolor by C. Pellegrini. 1831.                  A facsimile of the layout diagram of the ship, Negrero Bookes.    Transportation of slaves, according to a drawing by Rugendas. By decision of the Year XIII Assembly, black people are integrated into national life. The 8th Infantry Regiment joins San Martin’s Army in Mendoza, where it is given the name Libertos (Freedmen). Using onomatopoeia, black folk songs reflect the joy of liberation: Lingo, lingo, lingo, Linga, linga, linga, que ne tiela den balanco se cavó len dipotima. Compañelo di candombe Pita pano e bebe chiccha ya se le sijo que tienguemo no se puede se cativa. Ma luego ne solisonte lo sol melicano blilla alojado dese Oliente Len calena le mandinga”.',
            'orden' => 19,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          17 => 
          array (
            'id' => 130,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000049.jpg',
            'titulo' => 'Page 49',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1811. In search of symbols and identity, the Government creates the Pirámide de Mayo in the Plaza Mayor and orders placement of a plaque in honor of the first victims of the Revolution (Commandant Pereyra de Lucena and Captain Manuel Artigas). In spite of numerous requests, it is not placed until 80 years later, in 1891. The XIII Assembly approves the flag raised by Belgrano at the Juramento River in 1812 and the design of the National Shield, also by Belgrano, based on a seal by Juan de Dios Rivera. The National Anthem is approved. 1811. First freedom of press law. 1811. The first Triumvirate grants the first letter of citizenship to an English merchant who arrived at a very young age: Roberto Billinghurst, who immediately joins the revolutionary forces as an “American citizen of the United Provinces of the River Plate.” 1812. The Loggia Lautaro is organized in Buenos Aires, with the participation of the most important civilian, military, and religious leaders, who come together in a spirit of self-sacrifice and commitment for the purpose of consolidating democracy in Latin America. San Martín, who was one of the main figures and a man of great political shrewdness, and equal civic and military abilities, extends the effort to Mendoza, Chile, and Peru. 1813. Musicians census in Buenos Aires (approximately 50, including Blas Parera, Juan Bautista Goiburu, Father Juan Antonio Piscasarri, Friar Juan Moreno). 1813. The General Assembly decrees freedom of progeny; Argentina is one of the first countries to take decisive steps against slavery and toward full abolition. Blacks become part of civic life upon integration of San Martín’s army as the 8th Regiment of Freedmen. 1814. The Institute of Medicine is created, under the direction of Cosme Argerich (1815). Between 1814 and 1834, 14 of Argentina’s modern-day provinces are established and Upper Peru, Paraguay, and Uruguay separate. Advances in education and the professions: the School of Medical Science is established in Buenos Aires in 1813; Academy of Jurisprudence, 1815; Francisco de Paula Castañeda founds two drawing academies in the Recoleta Convent, 1815; Academy of Mathematics, 1816. Arriving the same year, the English draftsman and watercolorist, Essex Vidal, creates images of Buenos Aires, which are published in London in 1820. 1815. English residents of Mendoza demonstrate, ready to: “Take up arms and spill their last drop of blood in defense of Independence.” They offer General San Martín a company under the command of Captain Juan Young. Bartolomé Hidalgo (1788-1822) presents his songs to the nation in Cielitos, which includes verses in favor of independence and marks the appearance of gaucho literature.',
            'orden' => 20,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          18 => 
          array (
            'id' => 131,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000050.jpg',
            'titulo' => 'Page 50',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS The Civil War Intensifies and Anarchy Abounds. The War for Independence continues and the Civil War begins (1817). Conflicts of interest between urban centers and the countryside, and between absolutism and absorption of the port-city. Anarchy. Angry, uneducated hordes are led by strong men. Civil war rages throughout the country, destroying national unity. The provinces declare themselves to be sovereign states (Tucumán and Entre Ríos declare themselves	republics).	Interprovincial fighting intensifies; now not only was there fighting between factions from Buenos Aires and the countryside, or between federalists and unionists, but also within each of these factions. Constitutional Presidency of Rivadavia. Insignias from Perú and Chile                             The frigates La Argentina and Santa Rosa, under the command of Hipólito Bouchard, taking Monterey, California, 1818; oil painting by Biggeri. The frigate Heroine under the command of marine colonel David Jewet, takes possession of the Malvinas in the name of the Argentine government, 1820; oil painting by Biggeri.',
            'orden' => 21,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          19 => 
          array (
            'id' => 132,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000051.jpg',
            'titulo' => 'Page 51',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1817. In Tucumán, Bishop Colombres begins the sugar industry. Former administrator to Empress Josephine of France and an associate of Humboldt, Amado Bonpland, arrives in the country and establishes his residence in Corrientes, bringing plants and seeds to adapt for cultivation. He will later become director of the Museum of Natural History of Corrientes. 1817. San Martín’s army crosses the Andes and liberates Chile and Peru, giving them absolute independence, with no obligation to enter into economic or political agreements with Argentina. As Protector of Peru, San Martín decrees the abolition of the servitude of the Indians. Juan Cruz Varela (who would later become Rivadavia’s official poet) recounts the Maipú action in neoclassical style. To a country sunk in bloody civil conflicts, San Martín writes: “...the genius of evil has inspired the delirium of Federation...I fear that, tired of anarchy, you will long, in the end, for oppression and you will accept the yoke of the first happy adventurer who comes along ” Neoclassical and romantic esthetics influence architecture until approximately the middle of the century, when French and Italian influences take hold as an intentional counter to the memory of Spanish domination. Terraces appear on rooftops. Important works are built, such as the portico of the Buenos Aires Cathedral and the San José Palace in Entre Ríos. Homes with Roman designs are built, apothecaries, etc. Architects such as Benoit, Catelín, and Adams are active in this period, and later Pellegrini, Senillosa, and others. 1817-1819. The privateer Hipólito Bouchard circles the globe in his frigate, La Argentina, contributing to the prohibition of smuggling in Madagascar, blockading the Philippines, and harassing the Spanish in the Pacific by taking the Fort of Monterey (California). In his company, the future Major Espora is initiated in battle. 1820. The ship Heroine, which was sent by the government of Martín Rodríguez, arrives on Soledad Island; the national flag is raised for the first time in the Malvinas on November 6. The hunting of seals and whales is regulated, and Luis Vernet of Hamburg is granted authorization for cattle-ranching. Vernet later becomes the political-military commander of the Islands. 1820. Captain Brandsen, a former officer in the French army (discharged after the fall of Napoleon), serves in the war for Independence until his death in Ituzaingó during a charge on a squad of German soldiers fighting for Brazil. Estanislao López, Facundo Quiroga, Artigas, Ibarra, Ramírez, and Bustos, become firmly entrenched in their respective regions; national dissolution. Each province is a fiefdom, each strongman a kinglet who tyrannizes the people of his province and harasses neighboring provinces. Manuel Belgrano, looking at the condition of the nation, dies with the declaration, “Woe to my Homeland!” 1821. The Literary Society publishes two periodicals: Argos (bimonthly) and Abeja Argentina (monthly), which reflected on the achievements of the era of Martín Rodríguez and Rivadavia. 1821. General Martín de Güemes, a fearless commander of guerrilla fighters in the northern armies, dies. 1821. Law of emphyteusis; founding of the University and the Bank of Buenos Aires.',
            'orden' => 22,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          20 => 
          array (
            'id' => 133,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000052.jpg',
            'titulo' => 'Page 52',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS Life Is Enhanced by the Contributions of Romantics and Liberals. With the return of Echeverría, the ideas of the romantic movement and political liberalism are spread and expressed in the works of writers such as Sarmiento, Alberdi, and Gutiérrez, and painters such as Charles Morel, who introduces the romantic rebellion into Argentine arts. “El Regreso de la Cautiva” (The Return of the Captive) oil painting by deMauricio Rugendas, 1838. The issue of the Indian raids was of great interest to Rugendas, who admired Echeverría greatly, and he did paintings inspired by “Rimas,” in which the romantic poet included his famous work, “La Cautiva” (The Captive). “Peinetones en la calle,” (Coifers in the Street) lithograph by César Bacle, satirizing the fashion brought to Buenos Aires in 1823 by Don Pedro Masculino. This Swiss painter of French nationality took up residence in Buenos Aires in 1828. He planned several public art projects, created a collection of Livestock Brands in the province of Buenos Aires, and founded several periodicals dedicated to trade and maritime information. When pressured to become an Argentine citizen, he leaves the country and offers his services to Bolivia and Chile, angering Rosas. As the result of a botched espionage plot, he is held prisoner nearly a year without trial. He is released only at the repeated insistence of the French Consul, on the verge of insanity, his health ruined.',
            'orden' => 23,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          21 => 
          array (
            'id' => 134,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000053.jpg',
            'titulo' => 'Page 53',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1822. Mandatory primary education; creation of the teacher-training school and the stock exchange. 1822. José Antonio Piscarri, first to bring society music to Buenos Aires, founds a school of music and song, together with Pedro Esnaola. The first concerts of Argentine music are begun. 1822. Martín Rodríguez regulates the medical and pharmaceutical fields using notably modern standards. 1823-1824. Founding of the Home for Orphans, the Home for Foundlings, the Women’s Hospital, and the Insane Asylum. The first “Shorton” bull and a herd of “Merino” sheep are imported. The National Historical Museum is created and freedom of religion is decreed. Mandatory smallpox vaccination. 1825. The first opera in Buenos Aires: The Coliseo Theater presents The Barber of Seville, which premieres simultaneously in New York. 1825. An American by the name of Hallet begins publishing the English-language weekly Cosmopolite. In 1826, The British Packet appears. In 1861, the first daily English newspaper in Latin America appears – The Buenos Aires Standard edited by Mulhall, who, beginning in 1863, also edits the voluminous,Handbook of the River Plate (with several later editions), containing highly detailed, accurate information about various aspects of the country. 1826. The Adventure and the Beagle carry out an “...exact rendering of the southern coast of the peninsula (sic) of South America, from the River Plate to Tierra del Fuego.” In 1832, the Beagle makes a second trip, carrying the young naturalist Darwin, who explores Patagonia. 1826. The Empire of Brazil declares war, which ends with the definitive separation of Uruguay. Admiral Guillermo Brown, the first supreme chief of the Argentine Squad, triumphs in the naval battles of Juncal and Los Pozos. 1828. The French-Italian engineer, Charles Enrique Pellegrini, arrives in Buenos Aires, having been hired for public works. He becomes famous as a portrait artist in Buenos Aires society. The Swiss César Hipólito Bacle also arrives. Alcides D’Orbigny publishes the account of his travels in the country. 1830. Rosas’s first term. Publication of “La Cautiva” by Esteban Echeverría. 1833. Expedition to the desert led by Juan Manuel de Rosas. The expedition camps at the Colorado River, explores up to the mountains, and occupies Choele-Choel Island in the Negro River.',
            'orden' => 24,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          22 => 
          array (
            'id' => 135,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000054.jpg',
            'titulo' => 'Page 54',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS Hegemony of Rosas. Rise of Internal Tensions and External Harassment. The confused domestic situation is aggravated by external wars that are covers for various territorial and hegemonic ambitions, border disputes, military campaigns, naval combat, and blockades that bleed the country and the economy. The despotic Rosas government arouses great opposition among opponents and exiles, resulting in the Grand Army Campaign led by General Urquiza. SANTA FE, 1853, GENERAL CONSTITUTIONAL ASSEMBLY “Manuela Rosas de Terrero,” oil painting by Prilidiano Pueyrredón, 1850. National Museum of Fine Arts “Execution of Reynafé and Santos Pérez,” lithograph by H. Bacle, 1838. The plate reads: “Vicente Guillermo Reynafé and Santos Pérez, main conspirators in the assassination of General Don Juan Facundo Quiroga and his entourage.” It shows the execution in front of the Town Hall, which was used as a firing wall, the crowd pushing to get a glimpse, and a stoic Rosas arriving to observe the scene.                    After the victory in Caseros, a joyful Buenos Aires greets Urquiza, who headed the column of soldiers. Some of the banned, such as Vicente Fidel López, Eguía, Moreno, and Pillado, greet him from the balconies of wealthy homes (painting by L. Matthis).',
            'orden' => 25,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          23 => 
          array (
            'id' => 136,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000055.jpg',
            'titulo' => 'Page 55',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1833. The People’s Restoration Society – the feared “Mazorca” – is created in Buenos Aires; it would use violence with impunity during Rosas’s second term as the Restorer of Law and would gain public authority. 1833. Great Britain takes over the Malvinas. The warring between strongmen continues and Quiroga is assassinated in Barranca Yaco. 1835. Rosas calls back the Jesuit Order, and then expels them again for refusing to allow his portrait to be hung in churches. The Jesuits will not return until 1862. 1836-1839. Peru and Bolivia invade the country as far as Tucumán. 1837. Esteban Echeverría founds “The May Association” with young people who abandon literary and philosophical pursuits in favor of a movement to overcome the splits between unionists and federalists, between schismatics and apostolics, between people from Buenos Aires and people from the countryside, and between well-heeled citizens and gauchos in britches that divide Argentine society. Notable figures such as Sarmiento, Alberdi, Marco Avellaneda, Vicente Fidel López, Mitre, and Aberastain join the movement. 1838. The Socialist Dogma by Esteban Echeverría is published. 1838. Anglo-French blockade until 1848. The battle of Vuelta de Obligado, 1845. 1839. Sarmiento, a staunch opponent of Rosas, founds the newspaper, El Zonda. 1839-1843. Uruguay declares war. 1843. Chile occupies the Straits of Magellan and founds Punta Arenas. 1845. Facundo: Civilization and Barbarism by Sarmiento is published. 1847. Ether is used in Buenos Aires as an anesthetic (by Drs. Tuksburry and Aubain); it was used for the first time in the world just a few months earlier. 1848. The Victoria Theater opens, attracting artists from Europe. 1848. Luis Piedra Buena is the first Argentine marine to cross the Antarctic circle. 1849. England returns Martín García. 1852. Caseros: the fall of Rosas. General Urquiza convenes all the governors in the city of San Nicolás de los Arroyos in order to forge an agreement that will serve as the basis of a national organization consisting of a congress with two congressmen from each state, the abolition of provincial customs services, and freedom of river navigation. “Payada en la Pulpería”(Improvised Musical Dialogue in the Store). Oil painting by Carlos Morel, who was born in 1813 is considered to be the first Argentine painter.',
            'orden' => 26,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:20',
            'updated_at' => '2019-01-12 19:01:20',
          ),
          24 => 
          array (
            'id' => 137,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000056.jpg',
            'titulo' => 'Page 56',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS National Constitution and the Resurgence of Divisions. Drafting of our Magna Carta, which is influenced by the Philadelphia Constitution of 1778, the ideas of The Foundations by Alberdi, the 1844 Swiss Constitution, the resolutions of the 1813 Assembly, the unionist constitutions of 1819 and 1826, the federal pacts of 1822 and 1823, and the declarations of the May Association, among others. The author of the Constitution was Juan María Gutiérrez, and one of its essential elements was to organize a structure that would make anarchy and despotism equally impossible. These and other evils were debated by the Republic, whose first name was the United Provinces of the River Plate. JUSTO JOSE DE URQUIZA, FIRST CONSTITUTIONAL PRESIDENT, 1854 Paraná is the provisional capital of the Argentine Confederation. Vélez Sarsfield writing in El Nacional and Mitre in Los Debates whip up anti-Urquiza sentiment; the Treaty of San Nicolás is rejected. Buenos Aires’ desire for preeminence is revealed, and Buenos Aires separates from the Confederation (until 1860). Mitre proposes Buenos Aires as “The Republic of the River Plate.” Tensions continue until Urquiza flees after the defeat of Pavón. Hegemony of Buenos Aires. Plaza Victoria, the old Colón Theater as seen the Cabildo (municipal building). Home of Urquiza, Palacio San José, in Entre Ríos.',
            'orden' => 27,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          25 => 
          array (
            'id' => 138,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000057.jpg',
            'titulo' => 'Page 57',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1852. Urquiza is made Provisional Director, responsible for foreign relations and for the immediate convening of a National Constitutional Convention. 1853. The Argentine Constitution is ratified in Santa Fe and remains in effect until its revision in 1994. Following Caseros, upon lifting of the censureship imposed by Rosas, freedom of the press leads to the near simultaneous appearance of numerous publications: El Progreso, Los Debates, El Nacional, La Avispa, La Tribuna, and La Camelia. Renowned European scientists, naturalists, doctors, engineers, and economists are contracted: Burmeister, Mantegazza, Page, Bravard, Campbell. Prilidiano Pueyrredón, the engineer, portrait and landscape artist, and chronicler of local customs, carries out his important academic pictorial works, as well as architectural works; in 1912, he rebuilds the Pirámides de Mayo over the existing 1811 structure. It is later transported by rail to its present location. Professionals of this period, up to the 80s, such as Pellegrini, Taylor, Fossatti, and Benoit, consolidate the academic field with various “Revivals” of Italian, English, and German influences, among others. Wrought iron structures are erected, remanufacture of railway equipment, importation of marble and other materials, which are worked by highly skilled immigrant craftsmen and artisans (mostly Italians and Swiss) and come to characterize the architecture of the period. Among the important structures erected during the period are the Astronomical Observatory of Córdoba, the Rosario Customs House, the Casa Rosada (presidential house), and various important Buenos Aires homes and large stores. Brazilian painter and draftsman, Juan León Palière, takes up residence in Buenos Aires. His work depicts local subjects and customs. 1856. Calfucurá, chief of the indigenous confederation (Araucanos, Pampas, etc.) founded in Salinas Grandes, ransacks Fort Azul and continues to harass the border areas until 1872, the year of his defeat and death. 1857. Opening of the original Colón Theater, with the production of the opera La Traviata. The building was located at what is now the site of the Banco de la Nación (National Bank). 1857. The Iron Road Society, following a series of serious setbacks, manages to build the first railway in Argentina from Buenos Aires to the west. The law authorizes a horse-pulled train, but a locomotive is obtained: La Porteña. Dalmacio Vélez Sarsfield is the first passenger to ride from what is now Lavalle Plaza to Floresta. 1859. Urquiza wins the battle of Cepeda. 1862-1867. Mitre’s presidency.',
            'orden' => 28,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          26 => 
          array (
            'id' => 139,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000058.jpg',
            'titulo' => 'Page 58',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS Migratory Waves and Social Structuring. National organization. Major colonizations. “Europeanization” of the country. Consolidation of an upper class whose power is based mainly on large estates and cattle ranching. Waves of European immigration continue, and colonization plans are stepped up, especially in Entre Ríos and Santa Fé (Colonia Esperanza); 1861, the Welsh settle in Rawson. The liberal model. Economic expansion, policy of attracting European capital and	immigrants;		dependence				on foreign		markets,			especially			the British		market		(railway,		beef,		etc.). Incorporation	of	new	lands	leads to a boom in agriculture and cattle ranching. “Vista de Curuzú, mirada aguas arriba de norte a sur”(Scene of the Curuzú, looking upriver from north to south). Oil painting by Cándido López based on notes taken two days prior to the battle of Curupaití, in which he lost his right hand. “Martín Fierro,” drawing by Castagnino for the EUDEBA collection, 1962.',
            'orden' => 29,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          27 => 
          array (
            'id' => 140,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000059.jpg',
            'titulo' => 'Page 59',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1865. Paraguayan war; Cándido López, Buenos Aires portrait artist, enlists in order to document the events of the Triple Alliance War. Despite amputation of his right arm on the battle front, he trains his left hand and finishes his work. 1866. El Fausto by Estanislao del Campo is published. 1867. Young British residents of Argentina found the Buenos Aires Football Club under the leadership of Tomas Hogg. 1867. The first newspaper to be sold on the street, La República, is launched. 1868-1874. Sarmiento’s presidency: “Democracy is education.” National census; founding of teacher-training schools in Paraná and Tucumán; national colleges in Jujuy, Santiago del Estero, San Luis, Santa Fe, and Corrientes; practical training in colleges in fields related to provincial industries in order to make it applicable to daily needs; mining in Catamarca, San Juan, etc.; schools of agriculture; night schools for workers; itinerant schools for outlying areas, etc.; 65 specialized U.S. teachers and several German specialists hired to work in the Astronomical Observatory founded in Córdoba; creation of the Argentine Science Society, the Public Library Commission, the Transandean Telegraph and the Transatlantic Telegraphic Cable (under the direction of H. Davidson), allowing communication with Europe; immigration and colonization. 1869. Civil Code (Dr. Dalmacio Vélez Sarsfield). La Prensa newspaper founded, followed by La Nación in 1870. 1870. Publication of Una Excursión a los Indios Ranqueles (An Excursion to the Ranquel Indians) by Lucio V. Mansilla. 1871. Yellow fever epidemic in Buenos Aires; Dr. Roque Pérez serves selflessly. 1872. Opening of the Opera Theater; musical shows. Hilario Ascasubi publishes Santos Vega and Gaucho poetry reaches it high point with publication of the poem “Martín Fierro” by José Hernández, the classic work of Argentine literature. 1873. In the city of Colon, province of Buenos Aires, the country’s first Civil Registry is created. 1874. The first rugby game is played. Creation of the first school of music and recitation, directed by Nicolás Bassi and dedicated to training musicians and instrumentalists. The Quartet Society is also created (1875) in order to promote chamber music and classical and romantic music. Argentine musicians who follow the precursors: Amancio Alcorta (1805-1862); Juan Bautista Alberdi (1810-1884); Juan Pedro Esnaola (1808-1878); Federico Espinosa, Arturo Berutti, etc. The first Argentine opera, La Gata Blanca (The White Cat) by Francisco Hargraves, premieres in 1877. 1877. First export of cattle on the hoof.',
            'orden' => 30,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          28 => 
          array (
            'id' => 141,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000060.jpg',
            'titulo' => 'Page 60',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS Buenos Aires Builds Its Grandeur. In 1885, Buenos Aires is the first city south of Ecuador with a population density nearly that  of  London.  It is the only city in the world where newspapers are published in five languages; the total per capita circulation of the 25 daily publications is double that of the United Kingdom and three times that of the United States. The growth of wealth in Buenos Aires (city and province from 1864 to 1883), in terms of average yearly per capita accumulation, is £ 11, compared to £ 5 in Great Britain and France, £ 6 in the U. S., and £ 9 in Australia. A comparison of the rate of trade in relation to population shows per capita trade in Argentina is equal to that of France and much higher than the rates of the United States, Germany, Italy, and Spain. While the shear yield is less than that of Australia, Argentina has more head of sheep than any country in the world. No other South American country invests as much in education. Beginning of industrial development, growth of the working class; new rising middle class. Process of assimilation of immigrants and their descendants, which makes the country a model of integration and coexistence. An aspiring	middle	class,	largely	the result of immigration, finds its place in society and gains access to power. Increasing centralization in Buenos Aires and stagnation in the countryside. “Banda Lisa,” oil painting by Angel Della Valle.             Plaza Pellegrini, painting by L. Matthis. “Ha sonado la sirena de La Prensa” (The siren of La Prensa has sung), drawing by Acquarone.',
            'orden' => 31,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          29 => 
          array (
            'id' => 142,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000061.jpg',
            'titulo' => 'Page 61',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1879. General Roca’s Desert Campaign, which concludes with the subjugation of the Pincén, Catriel, and Namuncurá chiefs, among others. According to Roca’s 1885 message: “Saihüeque, Sovereign of the Manzanas and the last of the chiefs of the south, who was still fleeing with his tribe near the headwaters of the Chubut, just surrendered with 3,000 Indians.” 1880. Carlos Tejedor heads a failed revolution, and President Nicolás Avellaneda, putting an end to a long-standing conflict, sends a bill to congress: “the Capital City of Buenos Aires is a historical necessity.” Roca’s first presidency. In 1882, first cold-storage plant in San Nicolás. The national practice of soccer is begun as it is integrated into the colleges. The National Counsel of Education is created; Law 1420: Free, mandatory, secular primary education in public schools. 1884. May 25, Argentina establishes the first lighthouse on Isla de los Estados. San Juan del Salvamento, a present-day historical monument whose destiny is immortalized by Jules Verne in his novel, The Lighthouse at the End of the World. Surge in literary production with the generation of the ‘80s: La Gran Aldea, by Vicente Fidel López; Juvenilla, by Miguel Cané; La Bolsa, by Martel; Mis Montañas,” by González, and so forth. First established folk theater at the Podestá circus. Emergence of tango as a national subculture. 1886. Fernández Davel prepares and administers a rabies vaccine in his home, the second “laboratory” in the world to produce Pasteur’s discovery (1885). The aspirations of a flourishing society (1890-1914) require lavish public buildings, homes, theaters, stores, parks, and esplanades. Liberal style, historicism, with French and English influence, Art Noveau. Works are created by European professionals and a few Argentines educated in the old world (Christophersen, Dormal, Thays, Chamber, Walker, Tamburini, Buschiazzo, etc.). These artists import special materials: slate, marble, baluster, plaster, bronze, and wood. Important buildings: the Congressional Palace, Harrods house; the current Colón Theater; Plaza Hotel; La Prensa; Cathedral of the City of La Plata, and others; simultaneous to the construction of great homes and parks, industrial architecture is seen in shops, plants, and clubs. By contrast, tenements also abound. 1888. The first polo team is established in Hurlingham. 1889. The Italian leader Malatesta propels the anarchist movement. The work of paleontologist Florentino Ameghino, Contribución al Conocimiento de los Mamíferos Fósiles en la República Argentina (A Contribution to the Study of Fossil Mammals in the Republic of Argentina), receives an award at the World’s Fair in Paris. ',
            'orden' => 32,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          30 => 
          array (
            'id' => 143,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000062.jpg',
            'titulo' => 'Page 62',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS End of the Nineteenth Century, Beginning of the Twentieth Century. Political Tensions Characterize the Country’s Evolution. National organization sparks constant battles over political hegemony alliances, pacts, party splits, and factions; liberals split into autonomists and nationalists (“raw” and “cooked”). Agreements like the Coalition; influences and pressure for transfer of power; insurrection (Mitre 1874, Tejedor 1880). The Twentieth Century “Sin Pan y Sin Trabajo” (Without Bread and Without Work), (detail), oil painting by Ernesto de la Cárcova, 1894. ',
            'orden' => 33,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          31 => 
          array (
            'id' => 144,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000063.jpg',
            'titulo' => 'Page 63',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1889. Meeting in the Florida garden; young people who oppose the “System” (Aristóbulo del Valle, Leandro N. Alem, Bernardo de Irigoyen, Vicente Fidel López, etc.) meet and organize the Civic Union of Youth in order to “…exercise citizensʼ political rights…with total independence from established authority and in order to awaken national civic life.” Subsequently, it becomes the Civic Union Party and is led by Alem and others. 1890. The White Berets Revolution. 1890. Julián Aguirre and Alberto Williams found the Buenos Aires Music Conservatory. 1892. Questions about borders with Chile; activities of naturalist and explorer, Francisco P. Moreno. 1894. La Vanguardia appears. It is a socialist and scientific newspaper that defends the working class with Juan B. Busto, José Ingenieros, Repetto, and others. Argentine painters, the majority of whom have perfected their craft in the Old World, express the naturalism aesthetic: Sívori, Della Valle, Schiaffino, De la Cárcova (his work, Without Bread, Without Work could be considered the culmination of Descriptive Naturalism). Later, with the return of Malharro, Impressionism is introduced. Neoclassical sculpture begins with Lucio Correa Morales (1852-1923: “The Female Captive”, “The Onas”, etc.), Francisco Cafferata (1861-1890: “Monument to Brown”), Alberto Lagos, and others. Introduction into Modernist literature with the arrival of Rubén Darío. 1896. Eight months after the cinematography presentation by the Lumiére brothers in Paris, the first public cinema show is presented in Buenos Aires. 1897. Professor Costa begins to teach radiology and Doctor Varzi manufactures the first equipment to work in the country only two years after Rontgen discovered x-rays. 1899-1906. The large national soccer clubs are founded: Boca Juniors, River Plate, Racing, Ferrocarril Oeste. In 1904, the first Argentine rugby team is established. 1902. Alejandro Posadas, creator of the Argentine School of Surgery, dies. He was the first in the country to use film as a teaching tool. 1902. German, English, and Italian fleets attack Venezuela in order to demand compensation and payment of a debt. As a reaction to these attacks, Luis María Drago establishes an international legal doctrine according to which: “One cannot oblige or demand, through the use of force, that a nation pay its debt.”',
            'orden' => 34,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          32 => 
          array (
            'id' => 145,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000064.jpg',
            'titulo' => 'Page 64',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS Growth and Progress. A second moment in the National Confederation begins, approximately, with the presidency of Roca; it is characterized by fights between “the Regime” and the growing and intransigent opposition of the Civic Union. The Civic Union for its part is also experiencing a split between the Radical Civic Union and the National Civic Union (individualists and anti- individualists). A drill searching for water in the area around Comodoro Rivadavia. “Uruguay Warship in the Antarctic”, oil painting by Biggeri. Under the command of naval lieutenant, Julián Irizibar, they rescued members of the Nordenskjold expedition in 1903. The then Second Lieutenant Sobral was also part of the ship’s crew. The new subway in Buenos Aires in 1918. The transfer of the Pirámides de Mayo to its current location (11-12- 1912).',
            'orden' => 35,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          33 => 
          array (
            'id' => 146,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000065.jpg',
            'titulo' => 'Page 65',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1903. The Argentine warship Uruguay manages to save Otto Nordenskjold’s Swedish expedition; among those rescued are the Argentine representative in Antarctica since 1901, Second Lieutenant Sobral. Argentina is the only country that has had a permanent base since 1904 in Antarctica on both Laurie Island and in the South Orkney Archipelago, where, in 1905, an Astronomy Observatory is established. 1905. The Lainez Law on education attempts to equalize education in urban areas and territories located far from urban centers. The War of the Gauchos by Leopoldo Lugones is published. 1907. Oil is discovered in Comodoro Rivadavia. 1908. The current Colón Theater opens with a presentation of the Verdi opera Aida. Later, the Argentine opera Aurora by Hector Panizza will be presented. The Central Railway is built in Retiro. 1909. Uniting of the “galleries” in the transandean tunnel with Chile. 1910. Centennial Celebration with the participation of distinguished personalities from around the world: Princess Isabel of Bourbon, George Clemenceau, Guillermo Marconi, etc.; Rubén Darío publishes “El Canto a la Argentina” (The Argentine Song). The city is adorned with monuments given by distinct ethnic communities. From the beginning of the century, building and technological progress intensify: Madero Port, Port Nuevo in Buenos Aires; ports in Rosario and La Plata; running water and drainage. The first steam automobile is imported; the first electric trolley (1897) and the first subway to Plaza Once (1913) begin to run; the Astronomy Observatory is established in the Orkney Islands, etc. Argentine cinema, with The Execution of Dorrego, produces it first film with a plot in 1911. 1912. The Roque Sáenz Peña Law is passed: “I aspire to have minorities represented and have the integrity of their rights fully protected.” Secret and required voting and the creation of true electoral registers. 1913. Eduardo Wilde, a physician, psychologist, and politician who advocated phrenicectomy (surgical technique) several decades before it was done in Europe, dies. 1913. José Ingenieros publishes The Mediocre Man. The Wagner Association of Buenos Aires is founded. Nexus Group (1907- 1913, approximately). Painters such as Fader, Colivadino, Ripamonte, Quirós, Alice, Cordiviola, Quinquela Martín, and others hold exhibits in the first national art galleries. Impressionism and Post-Impressionism.',
            'orden' => 36,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          34 => 
          array (
            'id' => 147,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000066.jpg',
            'titulo' => 'Page 66',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS Times of Protest and Social Upheaval. Workers demands, fueled not only by socialism but also by outside influences of malatestismo2 and anarchy, the Russian Revolution, and international social upheaval aggravate and intensify tensions in Argentina with uprisings (The Revolutions of 1890, 1893, and 1905). The most severe manifestation of this is the Tragic Week of 1919 and the events that occur from 1920 to 1922 with the sad and bloody episodes in Patagonia; all of this culminates in the Revolution of 1930. The period presents two contradictory sides: on the one hand, an oligarchy with little social sensitivity, responsible for vices that corrupt civil customs but which nevertheless, from the government’s standpoint, carries out projects of significant strength and foresight for the future of the country; and on the other hand, the exalted appearance of a strong man, as a providential man who brings the great disenfranchised masses into civil society with a feeling of renewal. “Rusia en Buenos Aires”(Russia in Buenos Aires), the caption reads (satirically): “The partisans of Rosendo Trepoff intervening in the meeting in the Lavallof Square (Plaza Lavelle).”From the magazine PBT, 1905. Public demonstrations on May 1, 1909. Police assault on the demonstrators, with the standard- bearer of the feminist group among them.',
            'orden' => 37,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          35 => 
          array (
            'id' => 148,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000067.jpg',
            'titulo' => 'Page 67',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  Sculpture, especially bronze and stone, is represented by Rogelio Yrurtia (1879-1950 “Song to Work,” 1907, “Monument to Dorrego,” etc.); José Fioravanti (1896-1972 “Monument to the Flag” in Rosario); Alfredo Bigatti (Monuments to Mitre and Roca); Lola Mora (Nereidas Fountain); Arturo Dresco (Monument to Spain, Costanera Sur); Agustín Riganelli, etc. The wave of immigrants and of colonization continues. The Constitution encourages European colonization, though nothing discourages immigration from other areas. Beginning with the passage of the Avellaneda Law in 1876, the inflow of colonists constantly increases. 1914. A National Census is taken. The population is approximately 8,000,000. In the capital, the population is 1,584,106, of whom 797,969 are Argentines and 786,137 are foreigners. Professor Luis Agote carries out the first transfusion of preserved blood that has been made incoagulable, a method that subsequently becomes universal. Pedro Lagleyze dies. He was an early ophthamologist and creator of original surgical techniques that had a broad international impact. 1915. The founding of the National Society of Music, which is known today as the National Association of Composers. 1916. Yrigoyen’s first term as president. 1917. A series of up to 80 workers’ strikes that continue until the general strike of 1919; this strike was severely repressed: The Tragic Week. 1918. Dr. Abel Ayerza, the first person in history to describe the “black heart” disease, a chronic cardiopulmonary disease, dies. 1918. University reform in Córdoba. The position of an elevated Argentine ethic in international politics: “…with regard to a society destined to defend future peace among nations, there is no distinction between belligerents and neutrals, nor between large and small countries among them….Victory does not give rights”; Argentina withdraws from the League of Nations. Premiere of the opera Tucumán by Felipe Boero, with the first libretto in Spanish. 1920-1929. Transradio International provides direct communication with the rest of the world. 1920-1922. Patagonia becomes a part of the collective national consciousness due to the bloody social upheaval in which workers’ demands mix with the actions of Russian agitators and banditry. Strikes, looting, fires, serious repression, and shootings. 1922-1928. Alvear’s presidency. Argentina, “Granary to the World.” Argentina’s foreign debt is comparable only to that of the United States, England, and France. “Canto al Trabajo” (Hymn to Work), group of sculptures in bronze by Rogelio Yrurtia.',
            'orden' => 38,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          36 => 
          array (
            'id' => 149,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000068.jpg',
            'titulo' => 'Page 68',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS Evolution Continues, Including First Indications of Industrialization.While a Belgian comes to the country in search of a better life, a small, ex-emigrant from France returns to Paris, dressed as a gaucho, in search of triumph. The arrival of Plus Ultra in Buenos Aires flying above the Yacht Club.',
            'orden' => 39,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          37 => 
          array (
            'id' => 150,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000069.jpg',
            'titulo' => 'Page 69',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1922. The Argentine polo team wins the World Cup in England; in 1924 it is Olympic champion. Lilian Harrison swims across the River Plate (1923). 1923. Professor P. Escudero is the first in Argentina to use insulin to treat diabetes. 1924. The founding of National Conservatory of Music and Scenic Art, now Carlos López Buchardo. 1924. Petorutti and Xul Solar return. La Vanguardia and new artist burst onto the scene. The opening of the Association for Friends of the Arts; Basaldúa, Butler, Spilimbergo, Forner, etc. 1925. The Colón Theater Orchestra, Chorus and Dance Troupe are created. Einstein visits Argentina. The Plata refinery is built. The Yacimientos Petrolíferos Fiscales (National Oilfields) is created under the direction of Colonel Mosconi. 1926. From Palos de Moguer to Buenos Aires: the hydroplane, Plus Ultra, conquers the Atlantic Ocean from the air. In architecture (from approximately 1914 to 1943): eclecticism, Art Nouveau, Art Deco, Hispano-American renaissance, rationalism, the incorporation of the use of reinforced concrete (the most significant example of which is the Kavanagh Building); production of domestic materials. Important buildings: Barolo, Bank of Boston, Cervantes Theater, Gran Rex Cinema, the Obelisk. The skyscraper and apartment buildings; rural and industrial establishments and working class neighborhoods; stadiums, etc. (Architects: Bustillo, Kronfuss, Prebisch, Villar, Dourge, Virasoro, and others). 1927. The Military Aircraft Factory is opened in Córdoba. General strike in solidarity with Sacco and Vanzetti. In Mendoza, the Cacheuta Hydroelectric Plant. 1928. Gardel is the King of Tango in Paris. 1929. Aime Tchiffely returns with two native horses, Mancha and Gato, after his successful ride between Buenos Aires and New York. Le Corbusier and Count Keyserling visit. Besides his diagnosis of Argentine sadness, the philosopher states that Argentine collective life is founded on sentimental bases, that Argentina’s future is clear, and that the Argentines are heirs to Western humanist culture.',
            'orden' => 40,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          38 => 
          array (
            'id' => 151,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000070.jpg',
            'titulo' => 'Page 70',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS Political Crisis and Evolution in the First Half of the Twentieth Century. Argentina endures the international Great Depression. Argentines live through the so-called “Terrible Decade.” Buenos Aires awaits the arrival of the LEGH and its solitary navigator. The Revolution of 1930; the people of Buenos Aires in the Plaza de Mayo for General Uriburu’s assumption of power after the defeat of Irigoyen.',
            'orden' => 41,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          39 => 
          array (
            'id' => 152,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000071.jpg',
            'titulo' => 'Page 71',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1927-1929. Return of Guttero; atelier with Bigatti, Forner and Domínguez Neira; the new gallery includes Victorica, Cúnsolo, Gómez Cornet, Del Prete, etc. 1930. Yrigoyen is defeated; Uriburu is the first de facto president of the twentieth century. The first independent theater is born in Buenos Aires. Victoria Ocampo directs the “Sur” group and magazine that will bring the contributions of exceptional creators to Argentina. 1932. Vito Dumas, the solitary navigator aboard the Legh, sails around the world in four months. Juan Carlos Zavala wins the marathon at the Olympic Games in Los Angeles. The Argentine polo team wins the Americas’ Cup, a title that it will defend until 1979. 1933. Martínez Estrada publishes The Pampa X-Ray. The launching of the first exhibition of Argentine painting abroad during President Justo’s trip to Brazil. Justo’s presidency: Boards are established: the National Meat Board; the Grain Regulation Board; the Dairy Industry Board and the Wine Regulatory Board; the National Highway Department is created and a broad road system is constructed throughout the country; several different social improvements such as severance pay, paid vacations, and “English Saturdays”). 1935. Borges publishes The Universal History of Infamy. It remains in publication today, along with works that make Borges one of the most important figures of world literature. The Florida and Boedo literary movements include important authors such as Mallea, Girondo, Macedonio Fernández, Horacio Quiroga, Leopoldo Marechal, Alfonsina Storni. 1936. Foreign Minister Saavedra Lamas wins the Nobel Peace Prize for his work in relation to the Chaco War. Monsignor Copello is the first Argentine Cardinal. The Obelisk is unveiled in commemoration of the 400th anniversary of the founding of the City of Buenos Aires. The Confederación General de Trabajo (CGT) Constitutional Congress meets. 1938. The bridge over the Uruguay River is opened, uniting Argentina and Brazil. 1939. The first television station in Argentina opens in Rosario. 1940-1945. Doctor E. Braun Menéndez and Doctor J.C. Fasiolo research the relationship between the kidneys and hypertension.',
            'orden' => 42,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          40 => 
          array (
            'id' => 153,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000072.jpg',
            'titulo' => 'Page 72',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS The Emergence of Peronism. The appearance of Peronism as a populist and national movement; workers and the middle classes  join the political scene; attempts at industrialization; nationalization of foreign companies. The explosion of internal migration: the “black heads” are made up of circles of poor villages around the big cities, in a democratically structured government but under the personal hegemony of a leader. Destruction of the city of San Juan in the 1944 earthquake.                            Day of Loyalty Celebration during which Perón, from the balcony, salutes the crowd in 1953. “Feet in the Fountain” – October 17, 1945 in the Plaza de Mayo.',
            'orden' => 43,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          41 => 
          array (
            'id' => 154,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000073.jpg',
            'titulo' => 'Page 73',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1942. The Buenos Aires-La Plata gas pipeline is opened. 1943. The G.O.U.; President Castillo is defeated. Successive military coups lead to turnovers in power: Generals Rawson, Ramírez, and Farrel. 1944. Violent earthquake destroys the city of San Juan. 1945. October 9, Perón renounces his three positions (Vice President, Minister of War, and Minister of Labor) and is arrested. On October 17, massive worker-union action in support of Perón. 1946-1955. Perón’s first and second presidential terms. In 1949, there is a Constitutional Amendment that makes it possible for Perón to be reelected. 1947. Bernardo Houssay wins the Nobel Prize for Physiology for his work on the endocrine influence of the pancreas and pituitary gland. 1947-1949. Nationalization of the railroads, telephone companies and the Plata Telegraph Company; stimulation and support for domestic industry. The Instituto Argentino de Proomoción industrial (Argentine Institute for the Promotion of Industry) is established; the National Air Merchant Fleet is created. Inflation and an accelerated increase in the cost of living begin. 1948. Dr. Eduardo Finochietto, founder of the Surgical School, dies. The Comodoro Rivadavia-Buenos Aires gas pipeline is opened, as is Ezeiza International Airport. In the area of sculpture, names such as Curatella Manes, Libero Badií, and Noemí Guerstein develop their work. Sesostris Vitullo (1899 – 1953), Alicia Penalba in France, Lucio Fontana and his spatialism in Italy, Julio Le Parc, and others, gain recognition abroad for their artistic talent. Abstract Art, Madí Art; Gyula Kosice and her hydro-sculpture; Concrete Art (Tomás Maldonado). Beginning in the 1950s, the launching of Informalism (Clorindo Testa, Kenneth Kemble, and others), Generative Painting (MacEntyre and others). 1943-1955. In architecture, the government’s influence dominates; typologies adopted by European totalitarian governments (monumentalism) are chosen, as are inauthentic folkloric models; these are made uniform throughout the country in the building of a great number of homes, schools, tourism hotels, union recreational resorts, etc. At the same time, there are expressions of rationalism (W. Acosta, Bonet, Sacriste, Alvarez, and others). Significant buildings: The School of Law, the Córdoba City Hall, Ezeiza Airport, the Eva Perón Foundation, polyclinics, vacation resorts, and union headquarters. 1951. Women are given the right to vote.',
            'orden' => 44,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          42 => 
          array (
            'id' => 155,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000074.jpg',
            'titulo' => 'Page 74',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS Violence, Persecution, and Sectarianism Penetrate and Cast a Shadow over Argentine Life. Churches and social and political institutions experience the fire of uncontrolled mobs.  The funeral procession leaves the Congress during the funeral rites for Eva Perón.  The people of Córdoba, in front of the Cathedral and the Town Council, celebrate the triumph of the Liberator Revolution of 1955.',
            'orden' => 45,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          43 => 
          array (
            'id' => 156,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000075.jpg',
            'titulo' => 'Page 75',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1951 Juan M. Fangio wins the World Auto Racing Championship; he will win again in 1954, 1955, 1956,and 1957. 1952. Eva Perón dies. Unprecedented funeral rites. The Argentine Mozarteum is created. 1951-1953. Forty-four newspapers and magazines are closed down; mob violence leads to the burning of the headquarters of various political parties and the Buenos Aires Jockey Club. Between 1954 and 1955, the conflict and persecution culminate in the Catholic Church with the burning of 11 churches and the Metropolitan Curia. 1955. The General Belgrano Base, the southernmost base located 1,300 kilometers from the South Pole, is established. The oil agreement with Argentine California is signed. Perón is defeated. Liberator Revolution: Lonardi is president. Dr. Eduardo de Robertis, biologist, discovers the nervous synapse in 1955 and in 1960 the basic structure of photoreceptors. 1956-1958. The first nuclear reactor in Latin America begins to operate. 1958-1968. A.S. Parodi and H.A. Ruggero discover the Argentine hemorrhagic fever virus and create a vaccine to make people immune to it. 1960. The first load of Argentine steel in San Nicolás. Dr. René Favaloro invents original techniques for cardiovascular surgery, especially the aorta-coronary bypass that carries his name. 1962. The Prizes by Cortázar appears. 1964. Dr. Pablo Mirizzi, inventor of operating room cholangiography, dies. 1964. World premiere of Don Rodrigo, Alberto Ginastera’s first opera, at the Colón Theater. 1964. The CGT launches a fight plan that leads to the occupation of 1,500 companies. The first manifestations of the guerilla movement begin in Salta. 1965. Dr. José M. M. Fernández, creator of the reaction related to leprosy that carries his name and has been universally adopted, dies. 1965. About Heroes and Tombs by Ernesto Sábato appears.',
            'orden' => 46,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          44 => 
          array (
            'id' => 157,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000076.jpg',
            'titulo' => 'Page 76',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS Instability and Subversion Are Ever More Present in Argentine Life. In the 53 years between 1930 and 1983, Argentina suffers political, economic, and moral setbacks that are illustrated by the fact that during this period, in which according to the Constitution there should have been nine presidents, there were 28 who held the highest office; in 15 of those cases, they were the leaders of de facto regimes. Political instability and ambiguity stimulate militarism and internal military crises in the quest for power. Subversion. Terrorism and guerilla warfare; repression and its excesses: “the dirty war”; increased inflation. These events leave a population plagued by skepticism and depression, the side effects of alternating moments of optimism which result in repeated frustration. Luis Federico Leloir, receiving the Nobel Prize for Medicine and Chemistry.            Atucha Nuclear Power Plant.                 The Land Station for Satellite Communications in Balcarce.                  Urban and rural guerillas stir up the country. Scenes of street violence in Cordobazo.              The arrival of Perón results in bloody episodes at Ezeiza International Airport',
            'orden' => 47,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          45 => 
          array (
            'id' => 158,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000077.jpg',
            'titulo' => 'Page 77',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1966. Argentina, through Colonel Jorge Leal, arrives at the South Pole. The training frigate Libertad, built in Argentina, achieves (and continues to hold) the world record for the crossing of the North Atlantic by a sailing ship (6 days, 21 hours). 1966. Military coup d’etat. The Argentina Revolution government. Che Guevara, in Córdoba, organizes bases of subversive activity. 1967. Ginastera and Mugica Láinez premiere their opera Bomarzo in Washington. Subsequent play in Argentina is prohibited. The Ezeiza Atomic Center is opened. 1968. M.E. Bellizi and H.A. Ruggero carry out the first heart transplant. 1968. Construction begins on the Atucha Nuclear Power Plant. Nicolino Locche, middleweight World Champion. José Neglia, the dancer, wins an award at the 6th Festival of Dance in Paris. In Taco Ralo (Tucumán province), Marxist-Peronist guerilla and terrorist movements begin. 1969. In Balcarce, the Satellite Message Receiving Station is opened. The Hernandarias underwater tunnel is opened (Santa Fe – Parana). Domingo Liotta works on the artificial heart that is implanted for the first time in Houston. Cordobazo. Worker-student outbreak of violence. Murders of union leaders: Vandor (1969), Alonso (1970), Rucci (1973). 1970. Kidnapping and murder of General Aramburu; terrorist activity worsens; awards for La Calera, Garín, etc. 1970. Federico Leloir receives the Nobel Prize for Chemistry for his discoveries about the internal process through which the liver receives glucose and produces glycogen (biosynthesis of carbohydrates). DeVicenzo wins the golf World Cup; Dimiddi is the World Rowing Champion; Monzón is the World Boxing Champion. The Ushuaia Telephone Company. 1971. The contract with “Aluar” is approved. 1973. The Chocón-Cerro Colorado Hydroelectric Complex (1968-1977). The General Belgrano Bridge between Chaco and Corrientes opens. Perón returns; Ezeiza massacre. Increased terrorist-guerilla activity (ERP, Montoneros, etc.) and new para-police violence by the Triple A. Perón’s third presidential term.',
            'orden' => 48,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          46 => 
          array (
            'id' => 159,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000078.jpg',
            'titulo' => 'Page 78',
            'texto' => 'MAJOR PERIODS AND THEIR CHARACTERISTICS Military Rule and the Return to Democracy. As a consequence of an irresponsible military effort to recover the Malvinas, the country is plunged into mourning, which hastens the fall of military rule and leads to popular elections. The euphoria of democracy. A patient hope in an attempt to answer the great question about the possibilities of a national reconciliation based on justice and work. The people of Buenos Aires celebrate the World Cup Soccer Championship victory in 1978.             General Galtieri gathers the people of Buenos Aires because of the Malvinas conflict.                    The Pope visits the Luján Basilica. Crowds of people at President Alfonsín’s inauguration.',
            'orden' => 49,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          47 => 
          array (
            'id' => 160,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000079.jpg',
            'titulo' => 'Page 79',
            'texto' => 'IMPORTANT HISTORICAL AND CULTURAL EVENTS  1974. The Atucha Nuclear Power Plant begins to operate and achieves, for the first time in the Southern Hemisphere, a sustained and controlled fission chain reaction. The construction of the Río III Plant in Córdoba is approved. Construction begins on the public works projects of the Salto Grande Hydroelectric Project. In San Nicolás, a blast furnace (the largest in Latin America) is opened in the General Savio Plant in Somisa. Guillermo Vilas wins the Grand Prix; Reuteman prevails in South Africa, Austria and the U.S.A.; Galíndez is the new middleweight boxing champion; Monzón maintains his title. 1975. Economic Crisis: The “Rodrigazo.” The Argentine army begins an anti-guerilla operation in Tucumán known as “Independencia.” 1976. Political Crisis: Ministerial changes. Average time: one Minister every 23 days. Isabel Perón is ousted by a military coup; process of national reorganization that will continue until 1983; General Videla leads the process. 1976-1978. The Fray Bentos-Puerto Unzué International Bridge is opened over the Uruguay River and Zárate-Brazo Largo railroad complex. The first generator at the Planicie-Banderita Plant is put into service. The General Mosconi Submarine Platform is used to detect oil in Argentine waters; it is operated by Bridas. The Futaleufú and Cabra Corral Hydroelectric Complexes are opened. Increased illegal repression of subversion. Another earthquake in San Juan (Caucete). The Queen of England’s decision about the Beagle Channel is made known; the islands to the south of the Channel are assigned to the Republic of Chile. 1978. Argentina wins the World Cup Soccer Championship and the World Ice Hockey Championship. Construction begins on the southern gas pipeline. 1979. The first turbine at Salto Grande is opened. Construction rights are awarded for the building of the Atucha II Nuclear Power Plant and Heavy Water Plant in Río Tercero, Córdoba. Papal mediation in the disagreement over the Beagle Channel is accepted. 1980. Pérez Esquivel wins the Nobel Peace Prize. Regular color TV broadcasts begin with the image of the Argentine flag. 1982. Tucumán Paper produces the first paper bovina using sugar cane husks. April 1. The war to recover the Malvinas begins. John Paul II visits Buenos Aires. Serious protests against the excesses committed during the repression. 1983. A return to democracy. Alfonsín is president. 1984. César Milstein receives the Nobel Prize in Physiology for his discovery of immunological mechanisms in the transmission of genes.',
            'orden' => 50,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          48 => 
          array (
            'id' => 161,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000080.jpg',
            'titulo' => 'Page 80',
            'texto' => '',
            'orden' => 51,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          49 => 
          array (
            'id' => 162,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/02_regiones/en/00000081.jpg',
            'titulo' => 'Page 81',
            'texto' => '',
            'orden' => 52,
            'idioma_id' => 2,
            'capitulo_id' => 17,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
        ),
      ),
    ),
    3 => 
    array (
      'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/thumbnail.jpg',
      'imagen_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/thumbnail.jpg',
      'imagen_titulo' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/es/00000080.jpg',
      'imagen_titulo_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000082.jpg',
      'titulo_espanol' => 'Índice general de la obra',
      'titulo_ingles' => 'Complete table of content',
      'texto_espanol' => 'Índice general de la obra',
      'texto_ingles' => 'Complete table of content',
      'orden' => 4,
      'created_at' => NULL,
      'updated_at' => NULL,
      'paginas' => 
      array (
        'es' => 
        array (
          0 => 
          array (
            'id' => 163,
            'imagen' => '/storage/app/public/tomos/capitulo_title_background.jpg',
            'titulo' => 'Página 80',
            'texto' => '',
            'orden' => 3,
            'idioma_id' => 1,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-01-12 19:01:21',
          ),
          1 => 
          array (
            'id' => 164,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/es/00000081.jpg',
            'titulo' => 'Página 81',
            'texto' => 'INTRODUCCION
(1999 - Segunda Edición)

Advertencia 6 Las Regiones 32
Prólogo 9 Región Pampeana 34
Argentina, Imagen de un País 15 Regl9“ Andma _ 35
La naturaleza 18 Region Chaquena 36
El hombre 22 Región Patagónica 37

Grandes Períodos y su Caracterización,
y los Hechos Históricos y Culturales
Significativas 39

Indice General de la Obra 78

SUMMA PATAGONICA
(1999 - Segunda Edición)

Prólogo 8 Nuevos intentos de penetración 96
Tierra Alerta 10 Los reyes de la Patagonia 98
Una tierra en blanco 1 1 Hacia la conquista más profunda 104
La fauna 3115…“ ¡8 Tierra de Hombres 108
Flora, COÍOHÍZHC¡ÓU y ganadería 25 Educar contra vientos y soledades 109
Parques de la patria patagónica 30 Armar un pueblo 1 10
El sur del SU? 34 Perfiles indígenas 1 13
El sexto continente 39 La semblanza misionera l 17
El hombre 44 El pionero 120
Tierra Incógnita 48 Francisco P. Moreno 122
Dibujando las costas 49 La nueva Gales de América 130
Nuestra Señora del Carmen 55 Sudafricanos en Patagonia 131
Nuevos viajeros 56 El nuevo Patagón 134
El mundo del interior 58 Imagen Satelitaria de la erupción del
Raíces étnicas 59 volcán Hudson 138
Rituales, el arte y la magia 63 Imagen Satelitaria de Península Valdés 139
Tierra de Fantasía 70 Imagen Satelitaria de Comodoro Rivadavia 141
Ciudades de leyenda 72 Tierra de Promisión 142
La quimera de oro 75 Una utopía posible 143
Monstruos de la fantasía 78 Crecimiento y desarrollo 147
El país de los bandoleros 85 Nuevas fuentes de energía 151
El lugar de los poetas 87 Geotermología 154
Tierra de Nadie 90 Los nuevos terratenientes 162
La juventud exploradora 93 El turismo ¡63

81',
            'orden' => 4,
            'idioma_id' => 1,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-11-28 11:56:01',
          ),
          2 => 
          array (
            'id' => 165,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/es/00000082.jpg',
            'titulo' => 'Página 82',
            'texto' => 'Dr. José Bonaparte
Historia de los dinosaurios de la Patagonia

Nestor Rubén Cúneo
Historia del Museo
Paleontológico Egidio Feruglio

Dr. Hugo Corbella
El Volcanismo y la Patagonia

Dr. Juan Carlos Agulla
Bases sociológicas de la Región Patagónica

Contralmirante Laurio H. Destéfani
Una epopeya austral

Francisco Erize
Mamíferos marinos en la Patagonia

Lic. Enrique H. Mizrahi,
y Lic. Francisco Pereira Fernández
La actividad pesquera en la Patagonia

APENDICES

175

187

191

205

211

229

237

Juan Carlos Chebez
Areas naturales: Península Mitre e Isla
de los Estados al rescate del conñn austral

Karolys Kovacs
Las aves de la región cordillerana
de la Patagonia argentina

Antonio Torrejón
La Argentina y el turismo

Dr. Alberto Rex González
Las exequias de Paine Giíorr

Dra. Anne Chapman
Sobre algunos mitos y ritos del los Selk\'nam

Dr. Pedro Lesta
La Patagonia

Bibliografía

 

249

255

265

279

308',
            'orden' => 5,
            'idioma_id' => 1,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:21',
            'updated_at' => '2019-11-28 11:56:01',
          ),
          3 => 
          array (
            'id' => 166,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/es/00000083.jpg',
            'titulo' => 'Página 83',
            'texto' => 'SUMMA ANDINA

(1989)
Dedicatoria 3 Tierra de Cantos 120
Prólogo 7 Martín Miguel de Guemes 137
Tierra Escondida … Juan Facundo Quiroga 139
La Puna y las quebradas 16 Ibarra \' Taboada 142
Los salares. desiertos o travesías 22 Juan Bautista BUSÍOS 143
Los valles 26 Alejandro Heredia 147
Las montañas 34 Nazario Benavídez 150
Imagen Satelitaria de Mendoza 49 Angel Vicente Peñaloza 151
Imagen Satelitaria de La Rioja, Felipe Varela 153
San Juan y Catamarca 51 Jºse Mana Paz 156
Tierra de Leyenda 52 Tierra en Crecimiento 164
El f01k101—e\' 54 Las ciudades 168
Los cuentos 56 Las gentes 171
Creencias, mitos y leyendas 57 La 5001edad 173
Celebraciones 59 La pol1t1ca 174
Los trabajos y la vida 60 La cultura 176
El culto popular 64 Los constructores 132
El rito o la superstición 66 Tierra de Bronces 194
La geografía del mito 68 Manuel Belgrano 196
Los pueblos sin historia 70 El Militar 198
Arte argentino precolombino 72 El Educador 200
El hombre y sus días 81 El Hombre Público 20]
Tierras de Arriba 88 Juan Bautista Alberdi 206
Las Fundaciones 90 JOSé de San Martín 212
Vida virreinal 94 Domingo F. Sarmiento 221
La rebeldia 102 Epílogo 233
La capital y las tierras de arriba 1 13
La frontera norte 1 15

83',
            'orden' => 6,
            'idioma_id' => 1,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-11-28 11:56:01',
          ),
          4 => 
          array (
            'id' => 167,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/es/00000084.jpg',
            'titulo' => 'Página 84',
            'texto' => 'APENDICES

Dr. Juan Carlos Agulla Antropóloga Ana Montes
Las elites tradicionales 9 Supervivencias de arte y artesanías en lo
Ing. Horacio Luis P0viña que fue la gran provmc1a 1nca1ca del Tucuman 45
El azúcar en la región 15 Lic. Leda Valladares
Arq. Ramón Gutiérrez Cantos de prec1p1c1o 55
Fragmentos arquitectónicos y urbanos Antonio Torrejón
del noroeste argentino 23 La región turistica andina 61
Dr. Mario Francisco Calafell Loza Dr. Alberto Rex González
El papel 33 El arte rupestre 69
Arq. Luis Horacio Casnati Dr. Pedro Lesta
El vino 37 El país montañoso y sus riquezas minerales 79

Ing. Humberto Frigerio
Origen y evolución de la vitivinicultura argentina 41

ANEXOS CORRESPONDIENTES AL TOMO 1

Anexo I 85 Anexo IV 89
Anexo II 87 Anexo V 90
Anexo III 88 Bibliografia del tomo I 93',
            'orden' => 7,
            'idioma_id' => 1,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-11-28 11:56:01',
          ),
          5 => 
          array (
            'id' => 168,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/es/00000085.jpg',
            'titulo' => 'Página 85',
            'texto' => 'SUMMA CHAQUENA

(1992)
Dedicatoria 3 Muerte y transfiguración de las razas 127
Prólogo 7 El hombre rural, santos y bandoleros 132
Tierra Embarcada 10 Santoral profano 136
Ciudades, territorios y provincias 17 Tierrra de Dios 140
La flora 20 España y la Compañía de Jesús 144
La fauna 26 Intereses, intrigas y conflictos 146
El Chacú 32 Estructura fisica del Imperio 150
Turismo 34 Estructura cultural del Imperio 154
Tierra de Caminantes 40 La Lúºida MÍSÍÓH 157
Infancia tempestuosa 44 Los jesuitas criollos 161
Adolescencia aguerrida 47 La gran colmena 163
Juan de Garay 48 De la guerra guaranítica a la caída del Imperio 167
Hernandarias 52 Andresito 169
El adelantado, su familia y Corrientes 56 Imagen Satelitaria de los rios Paraná y
Concepción del Bermejo 59 Paraguay 173
Santa Fe y los mancebos de la tierra 61 Imagen Satelitaria de Corrientes,
Hidrovía 67 Formosa y Chaco 175
Tierra Impenetrable 72 Tierra Asolada 177
Las “Entradas” al Chaco 74 Belgrano diplomático y el artiguismo 180
La paz de Lacangaye\' 76 Corrientes frente a Rosas 182
Frontera norte 80 Ferré y Paz 185
Luis Jorge Fontana 82 La Triple Alianza 188
Caciques. fortines y fortineros 85 Afianzamíento de la conquista 194
Navegantes, estudiosos y artistas 90 Explotaciones… explotadores y explotados 198
La “República” de Corrientes 92 La Forestal y Las Palmas 204
Ferré… Pujol y Vidal 94 Tierra de Embrujo 208
Aperturas del cerco 97 La colonización 215
Tierra sin Mal 100 Los pueblos nacientes 222
Teogonía Guaraní ¡04 Crecimiento de un pueblo 225
La Tierra lmperfecta 106 Desarrollo de la región 228
Babilonia 111
Las lenguas 116
Cultura, animismo y mitologías 1 17
Mundo verde. médicos y shamanes 1 18
Leyendas y narraciones 121
Arte y artesanías 123
Caciques y ejércitos 125

85',
            'orden' => 8,
            'idioma_id' => 1,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-11-28 11:56:01',
          ),
          6 => 
          array (
            'id' => 169,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/es/00000086.jpg',
            'titulo' => 'Página 86',
            'texto' => 'APENDICES

Dr. Juan Carlos Leiva
Nordeste argentino. un país de árboles y algo más

Dr. Juan Carlos Agulla
Yaciretá: un proyecto de desarrollo regional

Ing. Héctor José Bonifacio Reboratti
La actividad forestal en el norte y nordeste argentino

Guardaparque Juan Carlos Chebez
El ocaso de la selva

Lic. Roxana Patiño
Las reducciones guaraníes

Dr. Ernesto J. A. Maeder
Corrientes, de la ciudad a la provincia

15

25

29

39

Lic. Ana María Dupey
De artesanos y artesanías del nordeste

Antonio Torrejón
El turismo en la región del nordeste argentino

Arq. Ramón Gutiérrez
San Ignacio Mini

Dr. Pedro Lesta
Región Chaqueña: una esperanza en el subsuelo

Ing. José F. Speziale
Riqueza hídrica del litoral

ANEXOS CORRESPONDIENTES AL TOMO 1

Anexo 1
Anexo II
Anexo III

 

83
85
86

Anexo IV

Anexo V

Anexo VI

Anexo VII

Anexo Vlll
Bibliografia del Tomo I

45

55

61

65

71

87
89
90
91
91
92',
            'orden' => 9,
            'idioma_id' => 1,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-11-28 11:56:01',
          ),
          7 => 
          array (
            'id' => 170,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/es/00000087.jpg',
            'titulo' => 'Página 87',
            'texto' => 'SUMMA PAMPEANA

(1994)
Dedicatoria 3 Tierra de Centauros 140
Prólogo 7 Cimarrones 142
Tierra Infinita 10 El Gauºhº 144
Viajeros y cartógrafos 13 El alma del payador 147
La pampa real 19 Ave María Purísima...l 151
El Ombú y el Caldén 25 Quºf\'ºflº\'º _ _ _ lºº
Actitudes pioneras 28 El vra_¡eros1n equrpa_¡e 154
Los lunares pampeanos 32 LOS trabajos y los dias 156
Pero o canto o inando. ue es mi
Tierra del Diablº 40 modoyde C\'1nt\'1rp q 163
A 1 42 º “ . \'
lmanece en a parnpa 46 Corre una sombra doliente, sobre
E primer encuentro _ la pampa argentina 165
La conqu¡sta espnrtual 32 El cuerno de la abundancia 166
Araucanización de la pampa 54 T\' P … 174
Dinastia de los Piedras 55 ¡erras (lº _ an exar
, , _ Xenofobia 176
Dinastia de los Zorros 36
Los Catri ] 58 Para todos los hombres del mundo 180
C __ _ _ )
Cultura Indígena 59 Vida campes1na 184
, La Pampa “gr1nga" 188
Crepusculo pampeano 67 _
A los ganados y a las ¡meses 194
Tierra en Avance 72 Las pampas provinciales 199
Los blasones pampeanos 76 Imagen Satelitaria de la provincia
. \' .\' ( . _
Entradas al misterio ?) de Buenos Aires 203
Amopnando la frontera 83 Imagen Satelitaria de Entre Ríos y
Entrada de los Virreyes 85 el ¡.¡0 Uruguay 207
Entradas de la Patria ., 88 Tierra de Riquezas 208
Entradas de la Confederacron 92 “El sonoro anuncio del gran porvenir” 210
Entradas de la Orºanización Nacional 94 -
º Habrtar la pampa 216
La entrada rºllg\'ºsa 97 Bahia Blanca: la Capital del Sud 218
Tierras Separadas 100 Mar del Plata: La Perla del Atlántico 221
La hermana mayor 101 La Plata: Ciudad de las Diagonales 222
Revolución de batallas y salones 104 Rosario: La Chacra Asfaltada 223
El Robespíerre criollo ] 10 Los tres ºrandes caudillos 775
º … … .…
Buenos Aires y las provincias 1 13 Las siete vacas flacas 229
Democracias Bárbaras ] 15 Un pueblo en busca de su identidad 235
Artigas, Ramírez y López 1 … ldiosincracia del argentino 239
El restaurador de las leyes 122
Urquiza 131

87',
            'orden' => 10,
            'idioma_id' => 1,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-11-28 11:56:01',
          ),
          8 => 
          array (
            'id' => 171,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/es/00000088.jpg',
            'titulo' => 'Página 88',
            'texto' => 'APENDICES

Dr. Pedro Lesta
Historia profunda de la pampa 9

Antropólogo Carlos J. Gradín
El poblamiento temprano y
el arte rupestre de la pampa 13

Dr. Juan Carlos Agulla
La inmigración masiva europea en la región pampeana 23

Max Weber
Empresas rurales de colonos argentinos 28

Lic. Andrea Centeno y Roberto Vila Ortiz

El misterio de una ciudad que fue pampa 33
Dr. Norberto Ras
La producción agropecuaria pampeana 37

Félix Luna
Soy Roca 45

Dr. René H. Balestra
El norte cooperativo en el sur de Santa Fe\'

Lic. Rosalba Campra
Pampas de la Imaginación

Contralmirante Laurio H. Destéfani
La pampa marítima

Lic. Ana María Lasalle
Relatos de otros tiempos pampeanos

Guardaparque Raúl Angerami
El parque nacional de Lihué Calel

Guardaparque Eduardo Horacio Haene
El parque nacional El Palmar

Bibliografía Tomo I

49

53

61

75

81

85

89',
            'orden' => 11,
            'idioma_id' => 1,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-11-28 11:56:01',
          ),
          9 => 
          array (
            'id' => 172,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/es/00000089.jpg',
            'titulo' => 'Página 89',
            'texto' => 'SUMMA METROPOLITANA

Dedicatoria
Prólogo
Tierra Poblada
Un poco de teoría sobre la ciudad
y sus habitantes
Más de lo mismo
Ciudad para los hombres

Tierra Habitada
Los juegos que todos jugamos
Animal simbólico
Canales de conexión con el mundo real
Hacia una ciudad humana

Tierra Fundada
Fantasia y Ambición, madrinas del bautismo
Renacimiento de la Santísima Trinidad
La pícara cenicienta del plata
Transformación urbana y edilicia
Aires de independencia, tiempos de hegemonía
La plaza mayor
Huecos yjardines
Adelantos - Sub estructura
Utopías
Secar el río
El puerto

Tierra con Imagen
Los café. los hoteles

Los teatros
Las vías
Corrientes
Av. 9 de Julio
Alem — Colón
Florida
La costanera
Avenida de Mayo
Plazas
Plaza Lavalle
Plaza San Martín
La Recoleta
Palermo
Tierra Absorbida
Los 100 barrios porteños
De la parroquia al barrio

(1997)

10

11

20

24
29

32
35
38

42
44
48
52
57
63
69
70
72
79
80
81

86
93

95

98

98
108
112
116
120
125
130
139
142
150
155
160
161
164

89

Los trenes y el tranway
Crecimiento de la patria chica
El negocio de las tierras
La vivienda obrera

San Telmo

Constitución

Barracas

El Riachuelo

Otros distritos

La Boca

Barrios con Literatura
Flores

Chacarita

Palermo

Belgrano

Tierra de los Porteños

La impronta del hombre

Estímulos sensoriales

Cosmopolitismo

Espíritu de factoría

Discriminación

Clase media, individualismo y aspiración
La arrogancia

Vida cotidiana

El porteño

Los códigos éticos

El condimento extranjero

Adolescencia marginal

La dicotomía

Somos como somos

Imagen Satelitaria del Gran Buenos Aires
Imagen Satelitaria de la Capital Federal

Tierra Inventada

La realidad de base

De la estructura a la grandeza

Coronación de la Reina del Plata

Entre la sofisticación y la cultura

Buenos Aires, foco de cultura

De la grandeza al mito

El tango

Debilidades en el ciudadano y en la ciudad real
De la ciudad real a la ciudad inventada',
            'orden' => 12,
            'idioma_id' => 1,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-11-28 11:56:01',
          ),
          10 => 
          array (
            'id' => 173,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/es/00000090.jpg',
            'titulo' => 'Página 90',
            'texto' => 'Dr. Marcelo R. Yrigoyen
Buenos Aires por arriba y por debajo

Guardaparque Eduardo Horacio Haene
Buenos Aires a] natural

Andrés Bosso
Volando por Buenos Aires

Contralmirante Laurio Destefani
Buenos Aires marítimo

Arq. Alberto Petrina
Introducción a Buenos Aires

Arq. Federico Ugarte
La ciudad que queremos

Arq. Ramón Gutiérrez
Buenos Aires de papel. proyectos y utopías

Dr. Juan Carlos Agulla
Los habitantes de Buenos Aires

Al_/rente: Cerro Torre. al /Ondo. el
Lago Víedh¡u. (Hugo (Í\'orlw/lu)

APENDICES

17

21

23

33

39

43

49

Dra. Diana Balmori
Linajes y politica

Adolfo Bioy Casares
Recuerdos de Buenos Aires

Horacio Salas
El tango: seña de identidad de lo argentino

Lic. Beatriz Sarlo
Experiencias de ciudad

Arqta. Cocó de Larrañaga
Viaje a las estrellas

Arq. José María Peña
El carácter de Buenos Aires

Dr. Abel Posse
Buenos Aires, llamada la reina del plata

Bibliografía del Tomo I

52

67

72

77

81

87

91

97',
            'orden' => 13,
            'idioma_id' => 1,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-11-28 11:56:01',
          ),
          11 => 
          array (
            'id' => 174,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/es/00000091.jpg',
            'titulo' => 'Página 91',
            'texto' => '',
            'orden' => 14,
            'idioma_id' => 1,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-01-12 19:01:22',
          ),
          12 => 
          array (
            'id' => 175,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/es/00000092.jpg',
            'titulo' => 'Página 92',
            'texto' => '',
            'orden' => 15,
            'idioma_id' => 1,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-01-12 19:01:22',
          ),
          13 => 
          array (
            'id' => 176,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/es/00000093.jpg',
            'titulo' => 'Página 93',
            'texto' => 'l-EB

FUNDACION

Ediciones Fundación Alejandro Angel Bulgbemni Bom»
0 Raúl Bulgheruni',
            'orden' => 16,
            'idioma_id' => 1,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-11-28 11:56:01',
          ),
        ),
        'en' => 
        array (
          0 => 
          array (
            'id' => 178,
            'imagen' => '/storage/app/public/tomos/capitulo_title_background.jpg',
            'titulo' => 'Page 82',
            'texto' => '',
            'orden' => 4,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-01-12 19:01:22',
          ),
          1 => 
          array (
            'id' => 179,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000083.jpg',
            'titulo' => 'Page 83',
            'texto' => '',
            'orden' => 5,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-01-12 19:01:22',
          ),
          2 => 
          array (
            'id' => 180,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000084.jpg',
            'titulo' => 'Page 84',
            'texto' => '',
            'orden' => 6,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-01-12 19:01:22',
          ),
          3 => 
          array (
            'id' => 181,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000085.jpg',
            'titulo' => 'Page 85',
            'texto' => '',
            'orden' => 7,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-01-12 19:01:22',
          ),
          4 => 
          array (
            'id' => 182,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000086.jpg',
            'titulo' => 'Page 86',
            'texto' => '',
            'orden' => 8,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-01-12 19:01:22',
          ),
          5 => 
          array (
            'id' => 183,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000087.jpg',
            'titulo' => 'Page 87',
            'texto' => '',
            'orden' => 9,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-01-12 19:01:22',
          ),
          6 => 
          array (
            'id' => 184,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000088.jpg',
            'titulo' => 'Page 88',
            'texto' => '',
            'orden' => 10,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-01-12 19:01:22',
          ),
          7 => 
          array (
            'id' => 185,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000089.jpg',
            'titulo' => 'Page 89',
            'texto' => '',
            'orden' => 11,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-01-12 19:01:22',
          ),
          8 => 
          array (
            'id' => 186,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000090.jpg',
            'titulo' => 'Page 90',
            'texto' => '',
            'orden' => 12,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:22',
            'updated_at' => '2019-01-12 19:01:22',
          ),
          9 => 
          array (
            'id' => 187,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000091.jpg',
            'titulo' => 'Page 91',
            'texto' => '',
            'orden' => 13,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:23',
            'updated_at' => '2019-01-12 19:01:23',
          ),
          10 => 
          array (
            'id' => 188,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000092.jpg',
            'titulo' => 'Page 92',
            'texto' => '',
            'orden' => 14,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:23',
            'updated_at' => '2019-01-12 19:01:23',
          ),
          11 => 
          array (
            'id' => 189,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000093.jpg',
            'titulo' => 'Page 93',
            'texto' => '',
            'orden' => 15,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:23',
            'updated_at' => '2019-01-12 19:01:23',
          ),
          12 => 
          array (
            'id' => 190,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000094.jpg',
            'titulo' => 'Page 94',
            'texto' => '',
            'orden' => 16,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:23',
            'updated_at' => '2019-01-12 19:01:23',
          ),
          13 => 
          array (
            'id' => 191,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000095.jpg',
            'titulo' => 'Page 95',
            'texto' => '',
            'orden' => 17,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:23',
            'updated_at' => '2019-01-12 19:01:23',
          ),
          14 => 
          array (
            'id' => 192,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000096.jpg',
            'titulo' => 'Page 96',
            'texto' => '',
            'orden' => 18,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:23',
            'updated_at' => '2019-01-12 19:01:23',
          ),
          15 => 
          array (
            'id' => 193,
            'imagen' => '/storage/app/public/tomos/01_imagen_de_un_pais/03_indice_general/en/00000097.jpg',
            'titulo' => 'Page 97',
            'texto' => '',
            'orden' => 19,
            'idioma_id' => 2,
            'capitulo_id' => 18,
            'created_at' => '2019-01-12 19:01:23',
            'updated_at' => '2019-01-12 19:01:23',
          ),
        ),
      ),
    ),
  ),
);