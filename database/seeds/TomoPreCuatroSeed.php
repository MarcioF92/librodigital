<?php

use App\Pagina;
use App\Tomo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class TomoPreCuatroSeed extends CustomSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = array('id' => '4','nombre_espanol' => 'SUMMA PAMPEANA','nombre_ingles' => 'SUMMA PAMPEANA','portada' => '/storage/app/public/tomos/04_summa_pampeana/thumbnail.jpg','portada_ingles' => '/storage/app/public/tomos/04_summa_pampeana/thumbnail_ingles.jpg','created_at' => '2019-08-13 01:06:21','updated_at' => '2019-08-13 01:06:21');

        $tomo = Tomo::create($data);

        $carpetaTomo = '04_summa_pampeana';

        $capitulos = [
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/00_creditos/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/00_creditos/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/00_creditos/es/00000000.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/00_creditos/en/00000000.jpg",
                    'titulo_espanol' => "Introducción",
                    'titulo_ingles' => "Introduction",
                    'texto_espanol' => "Introducción",
                    'texto_ingles' => "Introduction",
                    'orden' => "1",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "00_creditos",
                'paginas' => [
                    'es' => [
                        'from' => 0,
                        'to' => 6
                    ],
                    'en' => [
                        'from' => 0,
                        'to' => 6
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/01_introduction/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/01_introduction/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/01_introduction/es/00000007.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/01_introduction/en/00000007.jpg",
                    'titulo_espanol' => "Introducción",
                    'titulo_ingles' => "Introduction",
                    'texto_espanol' => "Introducción",
                    'texto_ingles' => "Introduction",
                    'orden' => "2",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "01_introduction",
                'paginas' => [
                    'es' => [
                        'from' => 7,
                        'to' => 11
                    ],
                    'en' => [
                        'from' => 7,
                        'to' => 11
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/02_tierra_infinita/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/02_tierra_infinita/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/02_tierra_infinita/es/00000012.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/02_tierra_infinita/es/00000012.jpg",
                    'titulo_espanol' => "Tierra infinita",
                    'titulo_ingles' => "Infinite land",
                    'texto_espanol' => "Tierra infinita",
                    'texto_ingles' => "Infinite land",
                    'orden' => "3",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "02_tierra_infinita",
                'paginas' => [
                    'es' => [
                        'from' => 12,
                        'to' => 41
                    ],
                    'en' => [
                        'from' => 12,
                        'to' => 41
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/03_tierra_diablo/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/03_tierra_diablo/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/03_tierra_diablo/es/00000042.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/03_tierra_diablo/en/00000042.jpg",
                    'titulo_espanol' => "Tierra del diablo",
                    'titulo_ingles' => "Land of the devil",
                    'texto_espanol' => "Tierra del diablo",
                    'texto_ingles' => "Land of the devil",
                    'orden' => "4",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "03_tierra_diablo",
                'paginas' => [
                    'es' => [
                        'from' => 42,
                        'to' => 73
                    ],
                    'en' => [
                        'from' => 42,
                        'to' => 73
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/04_tierra_en_avance/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/04_tierra_en_avance/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/04_tierra_en_avance/es/00000074.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/04_tierra_en_avance/en/00000074.jpg",
                    'titulo_espanol' => "Tierra de avance",
                    'titulo_ingles' => "Land on the march",
                    'texto_espanol' => "Tierra de avance",
                    'texto_ingles' => "Land on the march",
                    'orden' => "5",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "04_tierra_en_avance",
                'paginas' => [
                    'es' => [
                        'from' => 74,
                        'to' => 101
                    ],
                    'en' => [
                        'from' => 74,
                        'to' => 101
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/05_tierras_separadas/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/05_tierras_separadas/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/05_tierras_separadas/es/00000102.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/05_tierras_separadas/en/00000102.jpg",
                    'titulo_espanol' => "Tierra separadas",
                    'titulo_ingles' => "Separate lands",
                    'texto_espanol' => "Tierra separadas",
                    'texto_ingles' => "Separate lands",
                    'orden' => "6",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "05_tierras_separadas",
                'paginas' => [
                    'es' => [
                        'from' => 102,
                        'to' => 141
                    ],
                    'en' => [
                        'from' => 102,
                        'to' => 141
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/06_tierra_centauros/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/06_tierra_centauros/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/06_tierra_centauros/es/00000142.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/06_tierra_centauros/en/00000142.jpg",
                    'titulo_espanol' => "Tierra de centauros",
                    'titulo_ingles' => "Land of centaurs",
                    'texto_espanol' => "Tierra de centauros",
                    'texto_ingles' => "Land of centaurs",
                    'orden' => "7",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "06_tierra_centauros",
                'paginas' => [
                    'es' => [
                        'from' => 142,
                        'to' => 173
                    ],
                    'en' => [
                        'from' => 142,
                        'to' => 175
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/07_tierra_de_pan_llevar/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/07_tierra_de_pan_llevar/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/07_tierra_de_pan_llevar/es/00000174.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/07_tierra_de_pan_llevar/en/00000176.jpg",
                    'titulo_espanol' => "Tierras de pan llevar",
                    'titulo_ingles' => "Wheat growing lands",
                    'texto_espanol' => "Tierras de pan llevar",
                    'texto_ingles' => "Wheat growing lands",
                    'orden' => "8",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "07_tierra_de_pan_llevar",
                'paginas' => [
                    'es' => [
                        'from' => 174,
                        'to' => 207
                    ],
                    'en' => [
                        'from' => 176,
                        'to' => 209
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/08_tierra_de_riquezas/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/08_tierra_de_riquezas/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/08_tierra_de_riquezas/es/00000208.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/08_tierra_de_riquezas/en/00000210.jpg",
                    'titulo_espanol' => "Tierra de riquezas",
                    'titulo_ingles' => "Land of abundance",
                    'texto_espanol' => "Tierra de riquezas",
                    'texto_ingles' => "Land of abundance",
                    'orden' => "9",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "08_tierra_de_riquezas",
                'paginas' => [
                    'es' => [
                        'from' => 208,
                        'to' => 244
                    ],
                    'en' => [
                        'from' => 210,
                        'to' => 244
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/09_apendices/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/09_apendices/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/09_apendices/es/00000000.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/09_apendices/en/00000245.jpg",
                    'titulo_espanol' => "Apéndices",
                    'titulo_ingles' => "Appendices",
                    'texto_espanol' => "Apéndices",
                    'texto_ingles' => "Appendices",
                    'orden' => "10",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "09_apendices",
                'paginas' => [
                    'es' => [
                        'from' => 0,
                        'to' => 0
                    ],
                    'en' => [
                        'from' => 245,
                        'to' => 341
                    ],
                ]
            ]
        ];

        foreach($capitulos as $capitulo) {
            $cap = $tomo->capitulos()->create($capitulo['capitulo']);
            for ($x = $capitulo['paginas']['es']['from']; $x <= $capitulo['paginas']['es']['to']; $x++) {
                $filename = $this->getFilename($x);
                $cap->paginas()->create([
                    'imagen' => '/storage/app/public/tomos/'.$carpetaTomo.'/'.$capitulo['carpeta'].'/es/'.$filename.'.jpg',
                    'titulo' => 'Página '.$x,
                    'texto' => '',
                    'orden' => $x,
                    'idioma_id' => '1',
                    'created_at' => '2019-01-12 19:01:15',
                    'updated_at' => '2019-01-12 19:01:15',
                ]);
            }
            for ($x = $capitulo['paginas']['en']['from']; $x <= $capitulo['paginas']['en']['to']; $x++) {
                $filename = $this->getFilename($x);
                $cap->paginas()->create([
                    'imagen' => '/storage/app/public/tomos/'.$carpetaTomo.'/'.$capitulo['carpeta'].'/en/'.$filename.'.jpg',
                    'titulo' => 'Página '.$x,
                    'texto' => '',
                    'orden' => $x,
                    'idioma_id' => '2',
                    'created_at' => '2019-01-12 19:01:15',
                    'updated_at' => '2019-01-12 19:01:15',
                ]);
            }
        }

    }
}
