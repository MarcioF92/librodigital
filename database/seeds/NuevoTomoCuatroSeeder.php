<?php

use App\Language;
use App\Tomo;
use Illuminate\Database\Seeder;

class NuevoTomoCuatroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        include 'tomo4/tomos.php';

        $imagenDeUnPais = Tomo::create([
            'nombre_espanol' => $tomos['nombre_espanol'],
            'nombre_ingles' => $tomos['nombre_ingles'],
            'portada' => $tomos['portada'],
            'portada_ingles' => $tomos['portada_ingles'],
            'created_at' => $tomos['created_at'],
            'updated_at' => $tomos['updated_at'],
        ]);

        foreach($tomos['capitulos'] as $capituloData) {
            $capitulo = $imagenDeUnPais->capitulos()->create([
                'imagen' => $capituloData['imagen'],
                'imagen_ingles' => $capituloData['imagen_ingles'],
                'imagen_titulo' => $capituloData['imagen_titulo'],
                'imagen_titulo_ingles' => $capituloData['imagen_titulo_ingles'],
                'titulo_espanol' => $capituloData['titulo_espanol'],
                'titulo_ingles' => $capituloData['titulo_ingles'],
                'texto_espanol' => $capituloData['texto_espanol'],
                'texto_ingles' => $capituloData['texto_ingles'],
                'orden' => $capituloData['orden'],
                'created_at' => $capituloData['created_at'],
                'updated_at' => $capituloData['updated_at'],
            ]);
            foreach ($capituloData['paginas']['es'] as $paginaEsData) {
                $capitulo->paginas()->create([
                    'imagen' => $paginaEsData['imagen'],
                    'titulo' => $paginaEsData['titulo'],
                    'texto' => $paginaEsData['texto'],
                    'orden' => $paginaEsData['orden'],
                    'idioma_id' => Language::where('abreviacion', 'ES')->first()->id,
                    'created_at' => $paginaEsData['created_at'],
                    'updated_at' => $paginaEsData['updated_at'],
                ]);
            }
            foreach ($capituloData['paginas']['en'] as $paginaEsData) {
                $capitulo->paginas()->create([
                    'imagen' => $paginaEsData['imagen'],
                    'titulo' => $paginaEsData['titulo'],
                    'texto' => $paginaEsData['texto'],
                    'orden' => $paginaEsData['orden'],
                    'idioma_id' => Language::where('abreviacion', 'EN')->first()->id,
                    'created_at' => $paginaEsData['created_at'],
                    'updated_at' => $paginaEsData['updated_at'],
                ]);
            }
        }
    }
}
