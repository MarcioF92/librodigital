<?php

use App\Pagina;
use App\Tomo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class TomoUnoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $tomo1 =  array('id' => '1','nombre_espanol' => 'ARGENTINA IMAGEN DE UN PAIS','nombre_ingles' => 'ARGENTINA IMAGE OF A COUNTRY','portada' => '/storage/app/public/tomos/01_imagen_de_un_pais/thumbnail.png','portada_ingles' => '/storage/app/public/tomos/01_imagen_de_un_pais/thumbnail_ingles.jpg','created_at' => '2019-08-13 01:06:05','updated_at' => '2019-08-13 01:06:05');


        $imagenDeUnPais = Tomo::create($tomo1);

        /* Capítulos de Imagen de un país */
        include 'tomo1/capitulos.php';

        if (isset($capitulos)) {
            foreach ($capitulos as &$capitulo) {
                unset($capitulo['color']);
                unset($capitulo['size']);
                unset($capitulo['position_x']);
                unset($capitulo['position_y']);
                unset($capitulo['tomo_id']);
                $imagenDeUnPais->capitulos()->create($capitulo);
            }
        }

        include 'tomo1/paginas.php';

        if (isset($paginas)) {
            foreach ($paginas as $pagina) {
                Pagina::create($pagina);
            }
        }

        /* Fin Capítulos de Imagen de un país */

        /*$tomos[] = Tomo::create([
            'nombre_espanol' => 'SUMMA ANDINA',
            'nombre_ingles' => 'SUMMA ANDINA',
            'portada' => '/storage/app/public/tomos/02_summa_andina/thumbnail.jpg',
            'portada_ingles' => '/storage/app/public/tomos/02_summa_andina/thumbnail_ingles.jpg',
        ]);

        $tomos[] = Tomo::create([
            'nombre_espanol' => 'SUMMA PATAGÓNICA',
            'nombre_ingles' => 'SUMMA PATAGÓNICA',
            'portada' => '/storage/app/public/tomos/03_summa_patagonica/thumbnail.jpg',
            'portada_ingles' => '/storage/app/public/tomos/03_summa_patagonica/thumbnail_ingles.jpg',
        ]);

        $tomos[] = Tomo::create([
            'nombre_espanol' => 'SUMMA PAMPEANA',
            'nombre_ingles' => 'SUMMA PAMPEANA',
            'portada' => '/storage/app/public/tomos/04_summa_pampeana/thumbnail.jpg',
            'portada_ingles' => '/storage/app/public/tomos/04_summa_pampeana/thumbnail_ingles.jpg',
        ]);

        $tomos[] = Tomo::create([
            'nombre_espanol' => 'SUMMA CHAQUEÑA',
            'nombre_ingles' => 'SUMMA CHAQUEÑA',
            'portada' => '/storage/app/public/tomos/05_summa_chaquena/thumbnail.png',
            'portada_ingles' => '/storage/app/public/tomos/05_summa_chaquena/thumbnail_ingles.png',
        ]);*/

    }
}
