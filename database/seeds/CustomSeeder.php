<?php

use Illuminate\Database\Seeder;

class CustomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    }

    protected function getFileName($x)
    {
        $x = (string) $x;
        while(strlen($x) < 8) {
            $x = '0'.$x;
        }
        return $x;
    }
}
