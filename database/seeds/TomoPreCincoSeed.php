<?php

use App\Pagina;
use App\Tomo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class TomoPreCincoSeed extends CustomSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = array('id' => '5','nombre_espanol' => 'SUMMA CHAQUEÑA','nombre_ingles' => 'SUMMA CHAQUEÑA','portada' => '/storage/app/public/tomos/05_summa_chaquena/thumbnail.png','portada_ingles' => '/storage/app/public/tomos/05_summa_chaquena/thumbnail_ingles.png','created_at' => '2019-08-13 01:06:21','updated_at' => '2019-08-13 01:06:21');

        $tomo = Tomo::create($data);

        $carpetaTomo = '05_summa_chaquena';

        $capitulos = [
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/00_creditos/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/00_creditos/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/00_creditos/es/00000000.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/00_creditos/en/00000000.jpg",
                    'titulo_espanol' => "Créditos",
                    'titulo_ingles' => "Credits",
                    'texto_espanol' => "Créditos",
                    'texto_ingles' => "Credits",
                    'orden' => "1",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "00_creditos",
                'paginas' => [
                    'es' => [
                        'from' => 0,
                        'to' => 4
                    ],
                    'en' => [
                        'from' => 0,
                        'to' => 6
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/01_introduccion/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/01_introduccion/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/01_introduccion/es/00000005.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/01_introduccion/en/00000007.jpg",
                    'titulo_espanol' => "Introducción",
                    'titulo_ingles' => "Introduction",
                    'texto_espanol' => "Introducción",
                    'texto_ingles' => "Introduction",
                    'orden' => "2",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "01_introduccion",
                'paginas' => [
                    'es' => [
                        'from' => 5,
                        'to' => 9
                    ],
                    'en' => [
                        'from' => 7,
                        'to' => 11
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/02_tierra_embarcada/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/02_tierra_embarcada/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/02_tierra_embarcada/es/00000010.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/02_tierra_embarcada/es/00000012.jpg",
                    'titulo_espanol' => "Tierra embarcada",
                    'titulo_ingles' => "Embarked land",
                    'texto_espanol' => "Tierra embarcada",
                    'texto_ingles' => "Embarked land",
                    'orden' => "3",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "02_tierra_embarcada",
                'paginas' => [
                    'es' => [
                        'from' => 10,
                        'to' => 39
                    ],
                    'en' => [
                        'from' => 12,
                        'to' => 41
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/03_tierra_de_caminantes/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/03_tierra_de_caminantes/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/03_tierra_de_caminantes/es/00000040.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/03_tierra_de_caminantes/en/00000042.jpg",
                    'titulo_espanol' => "Tierra de caminantes",
                    'titulo_ingles' => "Land of walkers",
                    'texto_espanol' => "Tierra de caminantes",
                    'texto_ingles' => "Land of walkers",
                    'orden' => "4",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "03_tierra_de_caminantes",
                'paginas' => [
                    'es' => [
                        'from' => 40,
                        'to' => 71
                    ],
                    'en' => [
                        'from' => 42,
                        'to' => 73
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/04_tierra_impenetrable/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/04_tierra_impenetrable/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/04_tierra_impenetrable/es/00000072.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/04_tierra_impenetrable/en/00000074.jpg",
                    'titulo_espanol' => "Tierra impenetrable",
                    'titulo_ingles' => "Impenetrable land",
                    'texto_espanol' => "Tierra impenetrable",
                    'texto_ingles' => "Impenetrable land",
                    'orden' => "5",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "04_tierra_impenetrable",
                'paginas' => [
                    'es' => [
                        'from' => 72,
                        'to' => 99
                    ],
                    'en' => [
                        'from' => 74,
                        'to' => 101
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/05_tierra_sin_mal/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/05_tierra_sin_mal/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/05_tierra_sin_mal/es/00000100.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/05_tierra_sin_mal/en/00000094.jpg",
                    'titulo_espanol' => "Tierra sin mal",
                    'titulo_ingles' => "No ones land",
                    'texto_espanol' => "Tierra sin mal",
                    'texto_ingles' => "No ones land",
                    'orden' => "6",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "05_tierra_sin_mal",
                'paginas' => [
                    'es' => [
                        'from' => 100,
                        'to' => 139
                    ],
                    'en' => [
                        'from' => 102,
                        'to' => 141
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/06_tierra_de_dios/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/06_tierra_de_dios/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/06_tierra_de_dios/es/00000140.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/06_tierra_de_dios/en/00000142.jpg",
                    'titulo_espanol' => "Tierra de Dios",
                    'titulo_ingles' => "Land of God",
                    'texto_espanol' => "Tierra de Dios",
                    'texto_ingles' => "Land of God",
                    'orden' => "7",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "06_tierra_de_dios",
                'paginas' => [
                    'es' => [
                        'from' => 140,
                        'to' => 175
                    ],
                    'en' => [
                        'from' => 142,
                        'to' => 177
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/07_tierra_asolada/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/07_tierra_asolada/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/07_tierra_asolada/es/00000176.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/07_tierra_asolada/en/00000178.jpg",
                    'titulo_espanol' => "Tierra asolada",
                    'titulo_ingles' => "Desvasted land",
                    'texto_espanol' => "Tierra asolada",
                    'texto_ingles' => "Desvasted land",
                    'orden' => "8",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "07_tierra_asolada",
                'paginas' => [
                    'es' => [
                        'from' => 176,
                        'to' => 207
                    ],
                    'en' => [
                        'from' => 178,
                        'to' => 207
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/08_tierra_de_embrujo/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/08_tierra_de_embrujo/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/08_tierra_de_embrujo/es/00000208.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/08_tierra_de_embrujo/en/00000208.jpg",
                    'titulo_espanol' => "Tierra de embrujo",
                    'titulo_ingles' => "Land of enchantment",
                    'texto_espanol' => "Tierra de embrujo",
                    'texto_ingles' => "Land of enchantment",
                    'orden' => "9",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "08_tierra_de_embrujo",
                'paginas' => [
                    'es' => [
                        'from' => 208,
                        'to' => 240
                    ],
                    'en' => [
                        'from' => 208,
                        'to' => 244
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/09_apendices/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/09_apendices/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/09_apendices/es/00000000.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/09_apendices/en/00000242.jpg",
                    'titulo_espanol' => "Apéndices",
                    'titulo_ingles' => "Appendices",
                    'texto_espanol' => "Apéndices",
                    'texto_ingles' => "Appendices",
                    'orden' => "10",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "09_apendices",
                'paginas' => [
                    'es' => [
                        'from' => 0,
                        'to' => 102
                    ],
                    'en' => [
                        'from' => 242,
                        'to' => 334
                    ],
                ]
            ]
        ];

        foreach($capitulos as $capitulo) {
            $cap = $tomo->capitulos()->create($capitulo['capitulo']);
            for ($x = $capitulo['paginas']['es']['from']; $x <= $capitulo['paginas']['es']['to']; $x++) {
                $filename = $this->getFilename($x);
                $cap->paginas()->create([
                    'imagen' => '/storage/app/public/tomos/'.$carpetaTomo.'/'.$capitulo['carpeta'].'/es/'.$filename.'.jpg',
                    'titulo' => 'Página '.($x+241),
                    'texto' => '',
                    'orden' => ($x+241),
                    'idioma_id' => '1',
                    'created_at' => '2019-01-12 19:01:15',
                    'updated_at' => '2019-01-12 19:01:15',
                ]);
            }
            for ($x = $capitulo['paginas']['en']['from']; $x <= $capitulo['paginas']['en']['to']; $x++) {
                $filename = $this->getFilename($x);
                $cap->paginas()->create([
                    'imagen' => '/storage/app/public/tomos/'.$carpetaTomo.'/'.$capitulo['carpeta'].'/en/'.$filename.'.jpg',
                    'titulo' => 'Página '.$x,
                    'texto' => '',
                    'orden' => $x,
                    'idioma_id' => '2',
                    'created_at' => '2019-01-12 19:01:15',
                    'updated_at' => '2019-01-12 19:01:15',
                ]);
            }
        }

    }
}
