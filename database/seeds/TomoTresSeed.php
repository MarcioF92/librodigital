<?php

use App\Tomo;
use Illuminate\Database\Seeder;

class TomoTresSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tomo3 =  array('id' => '3','nombre_espanol' => 'SUMMA PATAGÓNICA','nombre_ingles' => 'SUMMA PATAGÓNICA','portada' => '/storage/app/public/tomos/03_summa_patagonica/thumbnail.jpg','portada_ingles' => '/storage/app/public/tomos/03_summa_patagonica/thumbnail_ingles.jpg','created_at' => '2019-08-13 01:06:20','updated_at' => '2019-08-13 01:06:20');

        $tomos = Tomo::create($tomo3);
    }
}
