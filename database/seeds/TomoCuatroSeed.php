<?php

use App\Tomo;
use Illuminate\Database\Seeder;

class TomoCuatroSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tomo4 =   array('id' => '4','nombre_espanol' => 'SUMMA PAMPEANA','nombre_ingles' => 'SUMMA PAMPEANA','portada' => '/storage/app/public/tomos/04_summa_pampeana/thumbnail.jpg','portada_ingles' => '/storage/app/public/tomos/04_summa_pampeana/thumbnail_ingles.jpg','created_at' => '2019-08-13 01:06:21','updated_at' => '2019-08-13 01:06:21');

        $tomos = Tomo::create($tomo4);
    }
}
