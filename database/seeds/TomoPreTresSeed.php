<?php

use App\Pagina;
use App\Tomo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class TomoPreTresSeed extends CustomSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = array('id' => '3','nombre_espanol' => 'SUMMA PATAGÓNICA','nombre_ingles' => 'SUMMA PATAGÓNICA','portada' => '/storage/app/public/tomos/03_summa_patagonica/thumbnail.jpg','portada_ingles' => '/storage/app/public/tomos/03_summa_patagonica/thumbnail_ingles.jpg','created_at' => '2019-08-13 01:06:20','updated_at' => '2019-08-13 01:06:20');

        $tomo = Tomo::create($data);
        //$tomo = Tomo::find($tomo2['id']);

        $carpetaTomo = '03_summa_patagonica';

        $capitulos = [
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/00_introduction/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/00_introduction/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/00_introduction/es/00000000.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/00_introduction/en/00000000.jpg",
                    'titulo_espanol' => "Introducción",
                    'titulo_ingles' => "Introduction",
                    'texto_espanol' => "Introducción",
                    'texto_ingles' => "Introduction",
                    'orden' => "1",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "00_introduction",
                'paginas' => [
                    'es' => [
                        'from' => 2,
                        'to' => 9
                    ],
                    'en' => [
                        'from' => 0,
                        'to' => 8
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/01_summa_patagonica/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/01_summa_patagonica/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/01_summa_patagonica/es/00000007.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/01_summa_patagonica/en/00000009.jpg",
                    'titulo_espanol' => "Summa Patagónica",
                    'titulo_ingles' => "Summa Patagónica",
                    'texto_espanol' => "Summa Patagónica",
                    'texto_ingles' => "Summa Patagónica",
                    'orden' => "2",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "01_summa_patagonica",
                'paginas' => [
                    'es' => [
                        'from' => 7,
                        'to' => 11
                    ],
                    'en' => [
                        'from' => 9,
                        'to' => 13
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/02_alert_land/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/02_alert_land/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/02_alert_land/es/00000012.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/02_alert_land/es/00000014.jpg",
                    'titulo_espanol' => "Tierra de alerta",
                    'titulo_ingles' => "Alert land",
                    'texto_espanol' => "Tierra de alerta",
                    'texto_ingles' => "Alert land",
                    'orden' => "3",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "02_alert_land",
                'paginas' => [
                    'es' => [
                        'from' => 12,
                        'to' => 49
                    ],
                    'en' => [
                        'from' => 14,
                        'to' => 51
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/03_unknow_land/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/03_unknow_land/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/03_unknow_land/es/00000050.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/03_unknow_land/en/00000052.jpg",
                    'titulo_espanol' => "Tierra desconocida",
                    'titulo_ingles' => "Unknow land",
                    'texto_espanol' => "Tierra desconocida",
                    'texto_ingles' => "Unknow land",
                    'orden' => "4",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "03_unknow_land",
                'paginas' => [
                    'es' => [
                        'from' => 50,
                        'to' => 71
                    ],
                    'en' => [
                        'from' => 52,
                        'to' => 73
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/04_land_of_fantasy/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/04_land_of_fantasy/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/04_land_of_fantasy/es/00000072.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/04_land_of_fantasy/en/00000074.jpg",
                    'titulo_espanol' => "Tierra de fantasía",
                    'titulo_ingles' => "Land of fantasy",
                    'texto_espanol' => "Tierra de fantasía",
                    'texto_ingles' => "Land of fantasy",
                    'orden' => "5",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "04_land_of_fantasy",
                'paginas' => [
                    'es' => [
                        'from' => 72,
                        'to' => 91
                    ],
                    'en' => [
                        'from' => 74,
                        'to' => 93
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/05_no_ones_land/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/05_no_ones_land/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/05_no_ones_land/es/00000092.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/05_no_ones_land/en/00000094.jpg",
                    'titulo_espanol' => "Tierra de nadie",
                    'titulo_ingles' => "No ones land",
                    'texto_espanol' => "Tierra de nadie",
                    'texto_ingles' => "No ones land",
                    'orden' => "6",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "05_no_ones_land",
                'paginas' => [
                    'es' => [
                        'from' => 92,
                        'to' => 109
                    ],
                    'en' => [
                        'from' => 94,
                        'to' => 111
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/06_land_of_men/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/06_land_of_men/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/06_land_of_men/es/00000110.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/06_land_of_men/en/00000112.jpg",
                    'titulo_espanol' => "Tierra de hombres",
                    'titulo_ingles' => "Land of men",
                    'texto_espanol' => "Tierra de hombres",
                    'texto_ingles' => "Land of men",
                    'orden' => "7",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "06_land_of_men",
                'paginas' => [
                    'es' => [
                        'from' => 110,
                        'to' => 143
                    ],
                    'en' => [
                        'from' => 112,
                        'to' => 145
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/07_land_of_promise/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/07_land_of_promise/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/07_land_of_promise/es/00000144.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/07_land_of_promise/en/00000146.jpg",
                    'titulo_espanol' => "Tierra de promesas",
                    'titulo_ingles' => "Land of promise",
                    'texto_espanol' => "Tierra de promesas",
                    'texto_ingles' => "Land of promise",
                    'orden' => "8",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "07_land_of_promise",
                'paginas' => [
                    'es' => [
                        'from' => 144,
                        'to' => 172
                    ],
                    'en' => [
                        'from' => 146,
                        'to' => 174
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/08_appendices/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/08_appendices/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/08_appendices/es/00000173.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/08_appendices/en/00000175.jpg",
                    'titulo_espanol' => "Apéndices",
                    'titulo_ingles' => "Appendices",
                    'texto_espanol' => "Apéndices",
                    'texto_ingles' => "Appendices",
                    'orden' => "9",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "08_appendices",
                'paginas' => [
                    'es' => [
                        'from' => 173,
                        'to' => 316
                    ],
                    'en' => [
                        'from' => 175,
                        'to' => 321
                    ],
                ]
            ]
        ];

        foreach($capitulos as $capitulo) {
            $cap = $tomo->capitulos()->create($capitulo['capitulo']);
            for ($x = $capitulo['paginas']['es']['from']; $x <= $capitulo['paginas']['es']['to']; $x++) {
                $filename = $this->getFilename($x);
                $cap->paginas()->create([
                    'imagen' => '/storage/app/public/tomos/'.$carpetaTomo.'/'.$capitulo['carpeta'].'/es/'.$filename.'.jpg',
                    'titulo' => 'Página '.$x,
                    'texto' => '',
                    'orden' => $x,
                    'idioma_id' => '1',
                    'created_at' => '2019-01-12 19:01:15',
                    'updated_at' => '2019-01-12 19:01:15',
                ]);
            }
            for ($x = $capitulo['paginas']['en']['from']; $x <= $capitulo['paginas']['en']['to']; $x++) {
                $filename = $this->getFilename($x);
                $cap->paginas()->create([
                    'imagen' => '/storage/app/public/tomos/'.$carpetaTomo.'/'.$capitulo['carpeta'].'/en/'.$filename.'.jpg',
                    'titulo' => 'Página '.$x,
                    'texto' => '',
                    'orden' => $x,
                    'idioma_id' => '2',
                    'created_at' => '2019-01-12 19:01:15',
                    'updated_at' => '2019-01-12 19:01:15',
                ]);
            }
        }

    }
}
