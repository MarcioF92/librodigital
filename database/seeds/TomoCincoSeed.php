<?php

use App\Tomo;
use Illuminate\Database\Seeder;

class TomoCincoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tomo5 = array('id' => '5','nombre_espanol' => 'SUMMA CHAQUEÑA','nombre_ingles' => 'SUMMA CHAQUEÑA','portada' => '/storage/app/public/tomos/05_summa_chaquena/thumbnail.png','portada_ingles' => '/storage/app/public/tomos/05_summa_chaquena/thumbnail_ingles.png','created_at' => '2019-08-13 01:06:21','updated_at' => '2019-08-13 01:06:21');

        $tomos = Tomo::create($tomo5);
    }
}
