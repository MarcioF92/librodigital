<?php

use App\Pagina;
use App\Tomo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class TomoPreDosSeed extends CustomSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $tomo2 = array('id' => '2','nombre_espanol' => 'SUMMA ANDINA','nombre_ingles' => 'SUMMA ANDINA','portada' => '/storage/app/public/tomos/02_summa_andina/thumbnail.jpg','portada_ingles' => '/storage/app/public/tomos/02_summa_andina/thumbnail_ingles.jpg','created_at' => '2019-08-13 01:06:20','updated_at' => '2019-08-13 01:06:20');

        $tomo = Tomo::create($tomo2);
        //$tomo = Tomo::find($tomo2['id']);

        $capitulos = [
            [
                'capitulo' => [
                    'imagen' => '/storage/app/public/tomos/02_summa_andina/00_comienzo/thumbnail.jpg',
                    'imagen_ingles' => '/storage/app/public/tomos/02_summa_andina/00_comienzo/thumbnail.jpg',
                    'imagen_titulo' => '/storage/app/public/tomos/02_summa_andina/00_comienzo/es/00000001.jpg',
                    'imagen_titulo_ingles' => '/storage/app/public/tomos/02_summa_andina/00_comienzo/en/00000000.jpg',
                    'titulo_espanol' => 'Créditos',
                    'titulo_ingles' => 'Credits',
                    'texto_espanol' => 'Créditos',
                    'texto_ingles' => 'Credits',
                    'orden' => '1',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => '00_comienzo',
                'paginas' => [
                    'es' => [
                        'from' => 1,
                        'to' => 6
                    ],
                    'en' => [
                        'from' => 0,
                        'to' => 6
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => '/storage/app/public/tomos/02_summa_andina/01_introduccion/thumbnail.jpg',
                    'imagen_ingles' => '/storage/app/public/tomos/02_summa_andina/01_introduccion/thumbnail.jpg',
                    'imagen_titulo' => '/storage/app/public/tomos/02_summa_andina/01_introduccion/es/00000007.jpg',
                    'imagen_titulo_ingles' => '/storage/app/public/tomos/02_summa_andina/01_introduccion/en/00000007.jpg',
                    'titulo_espanol' => 'Introducción',
                    'titulo_ingles' => 'Introduction',
                    'texto_espanol' => 'Introducción',
                    'texto_ingles' => 'Introduction',
                    'orden' => '2',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => '01_introduccion',
                'paginas' => [
                    'es' => [
                        'from' => 7,
                        'to' => 11
                    ],
                    'en' => [
                        'from' => 7,
                        'to' => 13
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => '/storage/app/public/tomos/02_summa_andina/02_tierra_escondida/thumbnail.jpg',
                    'imagen_ingles' => '/storage/app/public/tomos/02_summa_andina/02_tierra_escondida/thumbnail.jpg',
                    'imagen_titulo' => '/storage/app/public/tomos/02_summa_andina/02_tierra_escondida/es/00000012.jpg',
                    'imagen_titulo_ingles' => '/storage/app/public/tomos/02_summa_andina/02_tierra_escondida/es/00000012.jpg',
                    'titulo_espanol' => 'Tierra escondida',
                    'titulo_ingles' => 'Hidden land',
                    'texto_espanol' => 'Tierra escondida',
                    'texto_ingles' => 'Hidden land',
                    'orden' => '3',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => '02_tierra_escondida',
                'paginas' => [
                    'es' => [
                        'from' => 12,
                        'to' => 53
                    ],
                    'en' => [
                        'from' => 14,
                        'to' => 61
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => '/storage/app/public/tomos/02_summa_andina/03_tierra_de_leyenda/thumbnail.jpg',
                    'imagen_ingles' => '/storage/app/public/tomos/02_summa_andina/03_tierra_de_leyenda/thumbnail.jpg',
                    'imagen_titulo' => '/storage/app/public/tomos/02_summa_andina/03_tierra_de_leyenda/es/00000054.jpg',
                    'imagen_titulo_ingles' => '/storage/app/public/tomos/02_summa_andina/03_tierra_de_leyenda/en/00000062.jpg',
                    'titulo_espanol' => 'Tierra de leyenda',
                    'titulo_ingles' => 'Land of leyend',
                    'texto_espanol' => 'Tierra de leyenda',
                    'texto_ingles' => 'Land of leyend',
                    'orden' => '4',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => '03_tierra_de_leyenda',
                'paginas' => [
                    'es' => [
                        'from' => 54,
                        'to' => 89
                    ],
                    'en' => [
                        'from' => 62,
                        'to' => 107
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => '/storage/app/public/tomos/02_summa_andina/04_tierra_de_arriba/thumbnail.jpg',
                    'imagen_ingles' => '/storage/app/public/tomos/02_summa_andina/04_tierra_de_arriba/thumbnail.jpg',
                    'imagen_titulo' => '/storage/app/public/tomos/02_summa_andina/04_tierra_de_arriba/es/00000090.jpg',
                    'imagen_titulo_ingles' => '/storage/app/public/tomos/02_summa_andina/04_tierra_de_arriba/en/00000108.jpg',
                    'titulo_espanol' => 'Tierra de arriba',
                    'titulo_ingles' => 'Highlands',
                    'texto_espanol' => 'Tierra de arriba',
                    'texto_ingles' => 'Highlands',
                    'orden' => '5',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => '04_tierra_de_arriba',
                'paginas' => [
                    'es' => [
                        'from' => 90,
                        'to' => 121
                    ],
                    'en' => [
                        'from' => 108,
                        'to' => 147
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => '/storage/app/public/tomos/02_summa_andina/05_tierra_de_cantos/thumbnail.jpg',
                    'imagen_ingles' => '/storage/app/public/tomos/02_summa_andina/05_tierra_de_cantos/thumbnail.jpg',
                    'imagen_titulo' => '/storage/app/public/tomos/02_summa_andina/05_tierra_de_cantos/es/00000122.jpg',
                    'imagen_titulo_ingles' => '/storage/app/public/tomos/02_summa_andina/05_tierra_de_cantos/en/00000148.jpg',
                    'titulo_espanol' => 'Tierra de cantos',
                    'titulo_ingles' => 'Land of song',
                    'texto_espanol' => 'Tierra de cantos',
                    'texto_ingles' => 'Land of song',
                    'orden' => '6',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => '05_tierra_de_cantos',
                'paginas' => [
                    'es' => [
                        'from' => 122,
                        'to' => 165
                    ],
                    'en' => [
                        'from' => 148,
                        'to' => 201
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => '/storage/app/public/tomos/02_summa_andina/06_tierra_de_crecimientos/thumbnail.jpg',
                    'imagen_ingles' => '/storage/app/public/tomos/02_summa_andina/06_tierra_de_crecimientos/thumbnail.jpg',
                    'imagen_titulo' => '/storage/app/public/tomos/02_summa_andina/06_tierra_de_crecimientos/es/00000166.jpg',
                    'imagen_titulo_ingles' => '/storage/app/public/tomos/02_summa_andina/06_tierra_de_crecimientos/en/00000202.jpg',
                    'titulo_espanol' => 'Tierra de crecimientos',
                    'titulo_ingles' => 'Growing land',
                    'texto_espanol' => 'Tierra de crecimientos',
                    'texto_ingles' => 'Growing land',
                    'orden' => '7',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => '06_tierra_de_crecimientos',
                'paginas' => [
                    'es' => [
                        'from' => 166,
                        'to' => 195
                    ],
                    'en' => [
                        'from' => 202,
                        'to' => 239
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => '/storage/app/public/tomos/02_summa_andina/07_tierra_de_bronces/thumbnail.jpg',
                    'imagen_ingles' => '/storage/app/public/tomos/02_summa_andina/07_tierra_de_bronces/thumbnail.jpg',
                    'imagen_titulo' => '/storage/app/public/tomos/02_summa_andina/07_tierra_de_bronces/es/00000196.jpg',
                    'imagen_titulo_ingles' => '/storage/app/public/tomos/02_summa_andina/07_tierra_de_bronces/en/00000240.jpg',
                    'titulo_espanol' => 'Tierra de bronces',
                    'titulo_ingles' => 'Land of heroes',
                    'texto_espanol' => 'Tierra de bronces',
                    'texto_ingles' => 'Land of heroes',
                    'orden' => '7',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => '07_tierra_de_bronces',
                'paginas' => [
                    'es' => [
                        'from' => 196,
                        'to' => 246
                    ],
                    'en' => [
                        'from' => 240,
                        'to' => 294
                    ],
                ]
            ],
        ];

        foreach($capitulos as $capitulo) {
            $cap = $tomo->capitulos()->create($capitulo['capitulo']);
            for ($x = $capitulo['paginas']['es']['from']; $x <= $capitulo['paginas']['es']['to']; $x++) {
                $filename = $this->getFilename($x);
                $cap->paginas()->create([
                    'imagen' => '/storage/app/public/tomos/02_summa_andina/'.$capitulo['carpeta'].'/es/'.$filename.'.jpg',
                    'titulo' => 'Página '.$x,
                    'texto' => '',
                    'orden' => $x,
                    'idioma_id' => '1',
                    'created_at' => '2019-01-12 19:01:15',
                    'updated_at' => '2019-01-12 19:01:15',
                ]);
            }
            for ($x = $capitulo['paginas']['en']['from']; $x <= $capitulo['paginas']['en']['to']; $x++) {
                $filename = $this->getFilename($x);
                $cap->paginas()->create([
                    'imagen' => '/storage/app/public/tomos/02_summa_andina/'.$capitulo['carpeta'].'/en/'.$filename.'.jpg',
                    'titulo' => 'Página '.$x,
                    'texto' => '',
                    'orden' => $x,
                    'idioma_id' => '2',
                    'created_at' => '2019-01-12 19:01:15',
                    'updated_at' => '2019-01-12 19:01:15',
                ]);
            }
        }

    }
}
