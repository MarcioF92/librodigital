<?php

use Illuminate\Database\Seeder;
use App\Language;

class IdiomasSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Language::create([
                'nombre' => 'Español',
                'abreviacion' => 'ES',
                'defecto' => 1
        ]);
        Language::create([
                'nombre' => 'Inglés',
                'abreviacion' => 'EN',
                'defecto' => 0
        ]);
    }
}
