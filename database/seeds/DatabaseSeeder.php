<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AdminSeeder::class,
            IdiomasSeed::class,
            NuevoTomoUnoSeeder::class,
            NuevoTomoDosSeeder::class,
            NuevoTomoTresSeeder::class,
            NuevoTomoCuatroSeeder::class,
            NuevoTomoCincoSeeder::class,
            NuevoTomoSeisSeeder::class,
        ]);
    }
}
