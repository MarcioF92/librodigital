<?php

use Illuminate\Database\Seeder;

class PortadasTomoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $tomos = [
            [
                'nombre_espanol' => 'SUMMA ANDINA',
                'nombre_ingles' => 'SUMMA ANDINA',
                'nombre_carpeta' => '02_summa_andina'
            ],
            [
                'nombre_espanol' => 'SUMMA PATAGÓNICA',
                'nombre_ingles' => 'SUMMA PATAGÓNICA',
                'nombre_carpeta' => '03_summa_patagonica'
            ],
            [
                'nombre_espanol' => 'SUMMA PAMPEANA',
                'nombre_ingles' => 'SUMMA PAMPEANA',
                'nombre_carpeta' => '04_summa_pampeana'
            ],
            [
                'nombre_espanol' => 'SUMMA CHAQUEÑA',
                'nombre_ingles' => 'SUMMA CHAQUEÑA',
                'nombre_carpeta' => '05_summa_chaquena'
            ],
        ];

        foreach ($tomos as $tomo){
            \App\Tomo::create([
                'nombre_espanol' => $tomo['nombre_espanol'],
                'nombre_ingles' => $tomo['nombre_espanol'],
                'portada' => '/storage/app/public/tomos/'.$tomo['nombre_carpeta'].'/thumbnail.jpg',
                'portada_ingles' => '/storage/app/public/tomos/'.$tomo['nombre_carpeta'].'/thumbnail_ingles.jpg',
            ]);
        }

    }
}
