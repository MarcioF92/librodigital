<?php

use App\Pagina;
use App\Tomo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class TomoPreSeisSeed extends CustomSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = array('id' => '6',
            'nombre_espanol' => 'SUMMA METROPOLITANA',
            'nombre_ingles' => 'SUMMA METROPOLITANA',
            'portada' => '/storage/app/public/tomos/06_summa_metropolitana/thumbnail.png',
            'portada_ingles' => '/storage/app/public/tomos/06_summa_metropolitana/thumbnail_ingles.png',
            'created_at' => '2019-08-13 01:06:21',
            'updated_at' => '2019-08-13 01:06:21'
        );

        $tomo = Tomo::create($data);

        $carpetaTomo = '06_summa_metropolitana';

        $capitulos = [
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/00_introduccion/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/00_introduccion/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/00_introduccion/es/00000000.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/00_introduccion/en/00000000.jpg",
                    'titulo_espanol' => "Créditos",
                    'titulo_ingles' => "Credits",
                    'texto_espanol' => "Créditos",
                    'texto_ingles' => "Credits",
                    'orden' => "1",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "00_introduccion",
                'paginas' => [
                    'es' => [
                        'from' => 0,
                        'to' => 6
                    ],
                    'en' => [
                        'from' => 0,
                        'to' => 6
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/01_summa_metropolitana/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/01_summa_metropolitana/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/01_summa_metropolitana/es/00000007.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/01_summa_metropolitana/en/00000007.jpg",
                    'titulo_espanol' => "Introducción",
                    'titulo_ingles' => "Introduction",
                    'texto_espanol' => "Introducción",
                    'texto_ingles' => "Introduction",
                    'orden' => "2",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "01_summa_metropolitana",
                'paginas' => [
                    'es' => [
                        'from' => 7,
                        'to' => 11
                    ],
                    'en' => [
                        'from' => 7,
                        'to' => 11
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/02_tierra_poblada/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/02_tierra_poblada/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/02_tierra_poblada/es/00000012.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/02_tierra_poblada/es/00000012.jpg",
                    'titulo_espanol' => "Tierra poblada",
                    'titulo_ingles' => "Populated land",
                    'texto_espanol' => "Tierra poblada",
                    'texto_ingles' => "Populated land",
                    'orden' => "3",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "02_tierra_poblada",
                'paginas' => [
                    'es' => [
                        'from' => 12,
                        'to' => 27
                    ],
                    'en' => [
                        'from' => 12,
                        'to' => 27
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/03_tierra_habitada/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/03_tierra_habitada/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/03_tierra_habitada/es/00000028.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/03_tierra_habitada/en/00000028.jpg",
                    'titulo_espanol' => "Tierra habitada",
                    'titulo_ingles' => "Inhabited land",
                    'texto_espanol' => "Tierra habitada",
                    'texto_ingles' => "Inhabited land",
                    'orden' => "4",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "03_tierra_habitada",
                'paginas' => [
                    'es' => [
                        'from' => 28,
                        'to' => 43
                    ],
                    'en' => [
                        'from' => 28,
                        'to' => 43
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/04_tierra_fundada/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/04_tierra_fundada/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/04_tierra_fundada/es/00000044.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/04_tierra_fundada/en/00000044.jpg",
                    'titulo_espanol' => "Tierra fundada",
                    'titulo_ingles' => "Founded land",
                    'texto_espanol' => "Tierra fundada",
                    'texto_ingles' => "Founded land",
                    'orden' => "5",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "04_tierra_fundada",
                'paginas' => [
                    'es' => [
                        'from' => 44,
                        'to' => 87
                    ],
                    'en' => [
                        'from' => 44,
                        'to' => 87
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/05_tierra_con_imagen/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/05_tierra_con_imagen/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/05_tierra_con_imagen/es/00000088.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/05_tierra_con_imagen/en/00000088.jpg",
                    'titulo_espanol' => "Tierra con imagen",
                    'titulo_ingles' => "Memorable land",
                    'texto_espanol' => "Tierra con imagen",
                    'texto_ingles' => "Memorable land",
                    'orden' => "6",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "05_tierra_con_imagen",
                'paginas' => [
                    'es' => [
                        'from' => 88,
                        'to' => 161
                    ],
                    'en' => [
                        'from' => 88,
                        'to' => 161
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/06_tierra_absorbida/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/06_tierra_absorbida/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/06_tierra_absorbida/es/00000162.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/06_tierra_absorbida/en/00000162.jpg",
                    'titulo_espanol' => "Tierra absorbida",
                    'titulo_ingles' => "Absorbed land",
                    'texto_espanol' => "Tierra absorbida",
                    'texto_ingles' => "Absorbed land",
                    'orden' => "7",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "06_tierra_absorbida",
                'paginas' => [
                    'es' => [
                        'from' => 162,
                        'to' => 215
                    ],
                    'en' => [
                        'from' => 162,
                        'to' => 215
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/07_tierra_de_los_portenos/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/07_tierra_de_los_portenos/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/07_tierra_de_los_portenos/es/00000216.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/07_tierra_de_los_portenos/en/00000216.jpg",
                    'titulo_espanol' => "Tierra de los porteños",
                    'titulo_ingles' => "Land of the porteños",
                    'texto_espanol' => "Tierra de los porteños",
                    'texto_ingles' => "Land of the porteños",
                    'orden' => "8",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "07_tierra_de_los_portenos",
                'paginas' => [
                    'es' => [
                        'from' => 216,
                        'to' => 245
                    ],
                    'en' => [
                        'from' => 216,
                        'to' => 245
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/08_tierra_inventada/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/08_tierra_inventada/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/08_tierra_inventada/es/00000246.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/08_tierra_inventada/en/00000246.jpg",
                    'titulo_espanol' => "Tierra inventada",
                    'titulo_ingles' => "Invented land",
                    'texto_espanol' => "Tierra inventada",
                    'texto_ingles' => "Invented land",
                    'orden' => "9",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "08_tierra_inventada",
                'paginas' => [
                    'es' => [
                        'from' => 246,
                        'to' => 302
                    ],
                    'en' => [
                        'from' => 246,
                        'to' => 300
                    ],
                ]
            ],
            [
                'capitulo' => [
                    'imagen' => "/storage/app/public/tomos/{$carpetaTomo}/09_apendices/thumbnail.jpg",
                    'imagen_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/09_apendices/thumbnail.jpg",
                    'imagen_titulo' => "/storage/app/public/tomos/{$carpetaTomo}/09_apendices/es/00000000.jpg",
                    'imagen_titulo_ingles' => "/storage/app/public/tomos/{$carpetaTomo}/09_apendices/en/00000242.jpg",
                    'titulo_espanol' => "Apéndices",
                    'titulo_ingles' => "Appendices",
                    'texto_espanol' => "Apéndices",
                    'texto_ingles' => "Appendices",
                    'orden' => "10",
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ],
                'carpeta' => "09_apendices",
                'paginas' => [
                    'es' => [
                        'from' => 0,
                        'to' => 0
                    ],
                    'en' => [
                        'from' => 301,
                        'to' => 403
                    ],
                ]
            ]
        ];

        foreach($capitulos as $capitulo) {
            $cap = $tomo->capitulos()->create($capitulo['capitulo']);
            for ($x = $capitulo['paginas']['es']['from']; $x <= $capitulo['paginas']['es']['to']; $x++) {
                $filename = $this->getFilename($x);
                $cap->paginas()->create([
                    'imagen' => '/storage/app/public/tomos/'.$carpetaTomo.'/'.$capitulo['carpeta'].'/es/'.$filename.'.jpg',
                    'titulo' => 'Página '.($x+303),
                    'texto' => '',
                    'orden' => ($x+303),
                    'idioma_id' => '1',
                    'created_at' => '2019-01-12 19:01:15',
                    'updated_at' => '2019-01-12 19:01:15',
                ]);
            }
            for ($x = $capitulo['paginas']['en']['from']; $x <= $capitulo['paginas']['en']['to']; $x++) {
                $filename = $this->getFilename($x);
                $cap->paginas()->create([
                    'imagen' => '/storage/app/public/tomos/'.$carpetaTomo.'/'.$capitulo['carpeta'].'/en/'.$filename.'.jpg',
                    'titulo' => 'Página '.$x,
                    'texto' => '',
                    'orden' => $x,
                    'idioma_id' => '2',
                    'created_at' => '2019-01-12 19:01:15',
                    'updated_at' => '2019-01-12 19:01:15',
                ]);
            }
        }

    }
}
